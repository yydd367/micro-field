/*
 * file_operation.c
 *
 *  Created on: 2016年2月29日
 *      Author: Kang
 */
#include "file_operation.h"
#include "Collection.h"
#include "updata.h"
#include "network_config.h"
char filebuf[MAX_FILE_BUF_SIZE]={0};
//unsigned char gaucCmpBuf[2048];
//*****************************************************************************
//
//!  写日志函数
//!
//!  /param[IN] filename 日志文件名
//!  /param[IN] buffer 需要保存的缓存
//!
//!  /return  0:Success, -ve: failure
//
//*****************************************************************************
long writekeepfile(char *filename,char* buffer)
{
	long pFileHandle;
	SlFsFileInfo_t pFsFileInfo;
	uint16_t savesize=0;
	long lRetVal = -1;
	savesize=100;
	//UART_PRINT("writer_size=%d\r\n",savesize);
	lRetVal = sl_FsGetInfo((unsigned char *) filename, NULL, &pFsFileInfo);						//获取文件信息
	// UART_PRINT("nb\r\n");
	if (lRetVal < 0)																			//lretval《0证明文件不存在，需要创建文件
	{
		lRetVal = sl_FsOpen((unsigned char *) filename,FS_MODE_OPEN_CREATE(MAX_USER_FILE_SIZE,_FS_FILE_OPEN_FLAG_COMMIT |\
								_FS_FILE_PUBLIC_WRITE), NULL,&pFileHandle);						//创建文件
		if(lRetVal < 0)
		{
		        //
		        // File may already be created
		        //
			lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
			ASSERT_ON_ERROR(lRetVal);
		}
	}
	else																						//如果文件存在
	{
		lRetVal = sl_FsOpen((unsigned char *) filename,FS_MODE_OPEN_WRITE,NULL, &pFileHandle);	//打开文件
		if (lRetVal < 0)																		//判断返回值
		{
			lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
			ASSERT_ON_ERROR(lRetVal);
		}

	}
	lRetVal = sl_FsWrite(pFileHandle, 0, (unsigned char *)buffer, savesize);					//把缓存中的数据filebuffer写入到文件中
	if (lRetVal < 0)																			//判断返回值
	{
		lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
		ASSERT_ON_ERROR(lRetVal);
	}
	lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);													//关闭文件
	//UART_PRINT("Write file success!\r\n");														//写入成功
	return SUCCESS;
}

long readkeepfile(char *filename,char* buffer)
{
	long pFileHandle;			// Pointer to file handle
	SlFsFileInfo_t pFsFileInfo;
	long lRetVal = -1;
	uint32_t savesize=0;
//	unsigned char *pucFileBuffer=NULL;


	/* open same file which has been written with the server's file content */
	lRetVal = sl_FsOpen((unsigned char *) filename, FS_MODE_OPEN_READ,
								NULL, &pFileHandle);		 //打开要发送的文件
	if (lRetVal < 0)	//判断返回值，返回错误信息
	{
		lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
//		pucFileBuffer=NULL;
		ERR_PRINT(lRetVal);
		return lRetVal;
	}

	lRetVal = sl_FsGetInfo((unsigned char *) filename, NULL, &pFsFileInfo);	//获取文件信息
	if (lRetVal < 0)	//判断返回值
	{
		lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
//		pucFileBuffer=NULL;
		ERR_PRINT(lRetVal);
		return lRetVal;
	}
	savesize=(&pFsFileInfo)->FileLen;//获取文件长度
	UART_PRINT("readsize=%d\r\n",savesize);
//	pucFileBuffer = malloc(savesize+1);					//分配内存
//	if (NULL == pucFileBuffer)							//为空返回
//	{
//	    UART_PRINT("Can't Allocate Resources\r\n");
//	    return lRetVal;
//	}
//	memset(pucFileBuffer,'\0',savesize+1);
//	memset(buffer,0,savesize);
	UART_PRINT("menset ok");
	lRetVal = sl_FsRead(pFileHandle, 0, (unsigned char *)buffer, savesize);//读取文件的内容到缓存中
	if(lRetVal < 0)
	{
		lRetVal = sl_FsClose(pFileHandle,0,0,0);
//	    free(pucFileBuffer);
//	    pucFileBuffer=NULL;
	    ERR_PRINT(lRetVal);
	    return lRetVal;
	}
	lRetVal = sl_FsClose(pFileHandle,0,0,0);//关闭文件
//    UART_PRINT("%s\r\n",pucFileBuffer);
//	    free(pucFileBuffer);
//	    pucFileBuffer=NULL;
	UART_PRINT("Read file Successful \r\n");
	return lRetVal;
}
//*****************************************************************************
//
//!  This funtion includes the following steps:
//!  -open a user file for writing
//!  -write "Old MacDonalds" child song 37 times to get just below a 64KB file
//!  -close the user file
//!
//!  /param[out] ulToken : file token
//!  /param[out] lFileHandle : file handle
//!
//!  /return  0:Success, -ve: failure
//
//*****************************************************************************
long WriteFile(char *filename,char* buffer)
{
	long pFileHandle;			// Pointer to file handle
	SlFsFileInfo_t pFsFileInfo;
	long lRetVal = -1;
	char *oldbuffer=NULL;
	uint16_t savesize=0,offset=0;
	savesize= strlen(buffer);
	UART_PRINT("savesize=%d\r\n",savesize);
	lRetVal = sl_FsGetInfo((unsigned char *) filename, NULL, &pFsFileInfo);//获取文件信息
	UART_PRINT("回打瞌睡贷记卡sd");
	if (lRetVal < 0)									//lret-val《0证明文件不存在，需要创建文件
	{
		lRetVal = sl_FsOpen((unsigned char *) filename,FS_MODE_OPEN_CREATE(MAX_USER_FILE_SIZE,_FS_FILE_OPEN_FLAG_COMMIT |\
							_FS_FILE_PUBLIC_WRITE), NULL,&pFileHandle);
		 if(lRetVal < 0)
		 {
		        //
		        // File may already be created
		        //
			 lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
		        ASSERT_ON_ERROR(lRetVal);
		  }
		offset=0;
	}
	else
	{
		//否则证明文件存在，以读取的方式打开文件
//		lRetVal = sl_FsOpen((unsigned char *) filename, FS_MODE_OPEN_WRITE,NULL, &pFileHandle);
		lRetVal = sl_FsOpen((unsigned char *) filename,FS_MODE_OPEN_READ,NULL, &pFileHandle);
		offset=pFsFileInfo.FileLen;
		oldbuffer=malloc(offset+savesize);
		memset(oldbuffer,'\0',offset+savesize);
		if(oldbuffer==NULL)
		{
			UART_PRINT("Memory isn't malloc!\r\n");
			return lRetVal;
		}
		lRetVal = sl_FsRead(pFileHandle, 0, (unsigned char *) oldbuffer, offset);//读取文件的内容到缓存中
		if (lRetVal < 0)									//判断返回值
		{
			lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
			ERR_PRINT(lRetVal);
			free(oldbuffer);
			return lRetVal;
		}
		UART_PRINT("***********************\r\n");
		UART_PRINT("%s\r\n",oldbuffer);
		UART_PRINT("***********************\r\n");
		lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);//关闭文件

		lRetVal = sl_FsOpen((unsigned char *) filename,FS_MODE_OPEN_WRITE,NULL, &pFileHandle);
		if (lRetVal < 0)									//判断返回值
		{
			lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
			ERR_PRINT(lRetVal);
			free(oldbuffer);
			return lRetVal;
		}
	}
	UART_PRINT("FileLen=%d\r\n",offset);
	if(offset>0)
	{
		strcat(oldbuffer,buffer);
		lRetVal = sl_FsWrite(pFileHandle, 0, (unsigned char *)oldbuffer, savesize+offset);//把缓存中的数据filebuffer写入到文件中
		if (lRetVal < 0)//判断返回值返回错误信息
		{
			lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
			free(oldbuffer);
			ERR_PRINT(lRetVal);
			return lRetVal;
		}

	}
	else
	{
		lRetVal = sl_FsWrite(pFileHandle, 0, (unsigned char *)buffer, savesize);//把缓存中的数据filebuffer写入到文件中
		if (lRetVal < 0)//判断返回值返回错误信息
		{
			lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
			free(oldbuffer);
			ERR_PRINT(lRetVal);
			return lRetVal;
		}

	}
	UART_PRINT("file save Successful \r\n");
	free(oldbuffer);
	lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);//关闭文件
	return lRetVal;
}
//*****************************************************************************
//
//! 读取指定文件名的文件，并根木模式做相应的处理
//!
//!/param[in]filename  需要读取的文件的名字
//!/param[in] operation : 处理的方式
//!/param[in] lFileHandle : 相应处理方式的模式
//!			当 operation=DISPOSE_CONTURLFILE   mod 为 DATE_MOD 说明规则处理的模式为只获取规则改变的天数，和第一天的规则数据
//!										      mod为DATA_MOD，说明更新指定天数的规则
//!
//!  /return  0:Success, -ve: failure
//
//*****************************************************************************
long ReadFile(char *filename,char operation,char mod)
{
	long pFileHandle;			// Pointer to file handle
	SlFsFileInfo_t pFsFileInfo;
	long lRetVal = -1;
	uint32_t savesize=0;
	unsigned char *pucFileBuffer=NULL;


	/* open same file which has been written with the server's file content */
	lRetVal = sl_FsOpen((unsigned char *) filename, FS_MODE_OPEN_READ,
							NULL, &pFileHandle);		 //打开要发送的文件
	if (lRetVal < 0)	//判断返回值，返回错误信息
	{
		lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
		free(pucFileBuffer);
		pucFileBuffer=NULL;
		ERR_PRINT(lRetVal);
		return lRetVal;
	}

	lRetVal = sl_FsGetInfo((unsigned char *) filename, NULL, &pFsFileInfo);	//获取文件信息
	if (lRetVal < 0)	//判断返回值
	{
		lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
		free(pucFileBuffer);
		pucFileBuffer=NULL;
		ERR_PRINT(lRetVal);
		return lRetVal;
	}
	savesize=(&pFsFileInfo)->FileLen;//获取文件长度
	UART_PRINT("readsize=%d\r\n",savesize);
    pucFileBuffer = malloc(savesize+1);					//分配内存
    if (NULL == pucFileBuffer)							//为空返回
    {
    	UART_PRINT("Can't Allocate Resources\r\n");
    	return lRetVal;
    }
    memset(pucFileBuffer,'\0',savesize+1);
    lRetVal = sl_FsRead(pFileHandle, 0,  pucFileBuffer, savesize);//读取文件的内容到缓存中
    if(lRetVal < 0)
    {
        lRetVal = sl_FsClose(pFileHandle,0,0,0);
        free(pucFileBuffer);
        pucFileBuffer=NULL;
        ERR_PRINT(lRetVal);
        return lRetVal;
    }
    lRetVal = sl_FsClose(pFileHandle,0,0,0);//关闭文件
//    UART_PRINT("%s\r\n",pucFileBuffer);
    switch (operation)						//处理获取的文件内容
    {
    	case DISPOSE_PSSWD:					//处理获取的SSID和密码文件
    		UART_PRINT("%s\r\n",pucFileBuffer);
    		break;
    	case DISPOSE_CONTURLFILE:			//处理获取到的规则文件
    		dispose_control_file((char *)pucFileBuffer,mod);
    		break;
    	case DISPOSE_LOG:
    	memset(filebuf,'\0',MAX_FILE_BUF_SIZE);
    	memcpy((unsigned char *)filebuf,pucFileBuffer,savesize);
		break;
    	case DISPOSE_NAME:
    		memset(pcSsidName,'\0',33);
    		memcpy((unsigned char *)pcSsidName,pucFileBuffer,savesize);
    	default:
    		UART_PRINT("****%s*******\r\n",pucFileBuffer);
    }
//    UART_PRINT("%s\r\n",pucFileBuffer);
    free(pucFileBuffer);
    pucFileBuffer=NULL;
    UART_PRINT("Read file Successful \r\n");
    return lRetVal;
}
//*****************************************************************************
//
//!  更新断电保存日志文件
//!
//!  /param 	None
//!
//!
//*****************************************************************************
void updata_restore_file()
{
	long lRetVal=-1;
	DATA file_data=DATA_init_default;												//创建发送数据结构体
	char time[7]={0};																//时间字符串
	char statue[2]={0};																//状态字符串
	char date[5]={0};																//日期字符串
	char flow[5]={0};																//水流字符串
	sprintf(time,"%02d%02d%02d",time_axis.hour,time_axis.minute,time_axis.second);	//格式化时间
	sprintf(statue,"%d",plan_start);												//格式化状态
	sprintf(date,"%02d%02d",plan_date,last_updata_plant_day);						//格式化日期
	sprintf(flow,"%d",flow_count);													//格式化水流
	file_data.DATA1=statue;
	file_data.DATA2=date;
	file_data.DATA3=plant_name;
	file_data.DATA4=time;
	file_data.DATA5=flow;
	memset(filebuf,'\0',MAX_FILE_BUF_SIZE);											//初始化filebuf格式化数组
	lRetVal= sent_tcp_command(filebuf,sizeof(filebuf),"LOG",MOD_LOG_MOD,1,file_data);//序列化要保存的信息
	if(lRetVal<=0)
		 ERR_PRINT(lRetVal);
	writekeepfile((char *)RESTORE_FILE,filebuf);									//写入日志文件
//	read_restore_file();
//	writekeepfile((char *)RESTORE_FILE,p);
//	WriteFile((char *)RESTORE_FILE,p);
//	ReadFile((char *)RESTORE_FILE,DISPOSE_LOG,0);
}
//*****************************************************************************
//
//!  开机上电读取文件信息
//!
//!  /param[out] None
//
//*****************************************************************************
void read_restore_file(void)
{
	long lRetVal=-1;
	unsigned long token1=0;
	lRetVal=ReadFile((char *)RESTORE_FILE,DISPOSE_LOG,0);							//读取日志文件
	if (lRetVal < 0)																//判断是否获取成功
	{
		ERR_PRINT(lRetVal);
//		LOOP_FOREVER();
	}
	if(lRetVal>=0)																	//如果读取失败不解析
	{
		lRetVal=gettcpcommand_dispose((char *)filebuf,sizeof(filebuf));				//反序列化获取到的指令，解析指令，发送命令
		if(lRetVal<=0)
			ERR_PRINT(lRetVal);
		lRetVal=sl_FsDel((unsigned char *)RESTORE_FILE,token1);						//删除文件中的日志文件 不删除会造成如果本来没有断电，结果残留日志会影响程序正常运行
	}
}


