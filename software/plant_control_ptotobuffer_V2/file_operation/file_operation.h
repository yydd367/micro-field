/*
 * file_operation.h
 *
 *  Created on: 2016��2��29��
 *      Author: Kang
 */

#ifndef PLANT_CONTROL_PTOTOBUFFER_FILE_OPERATION_FILE_OPERATION_H_
#define PLANT_CONTROL_PTOTOBUFFER_FILE_OPERATION_FILE_OPERATION_H_
#include "systerm_init.h"

typedef enum
{
	DISPOSE_PSSWD=0,
	DISPOSE_CONTURLFILE,
	DISPOSE_LOG,
	DISPOSE_NAME
}FILE_DISPOSE_type;

#define SL_MAX_FILE_SIZE        64L*1024L       /* 64KB file */
#define RESTORE_FILE          "restore.txt"
#define NAME_FILE			   "MYNAME.txt"
#define MAX_FILE_BUF_SIZE		100
#define MAX_USER_FILE_SIZE		(4*1024)
extern char filebuf[MAX_FILE_BUF_SIZE];
void updata_restore_file(void);
void read_restore_file(void);
//long ReadFileFromDevice(unsigned long ulToken, long lFileHandle);
//long WriteFileToDevice(unsigned long *ulToken, long *lFileHandle);
long WriteFile(char *filename,char* buffer);
long ReadFile(char *filename,char operation,char mod);
long writekeepfile(char *filename,char* buffer);
long readkeepfile(char *filename,char* buffer);
#endif /* PLANT_CONTROL_PTOTOBUFFER_FILE_OPERATION_FILE_OPERATION_H_ */
