/*
 * user.h
 *
 *  Created on: 2016年2月27日
 *      Author: Kang
 */

#ifndef PLANT_CONTROL_PTOTOBUFFER_USER_H_
#define PLANT_CONTROL_PTOTOBUFFER_USER_H_
#include "network_config.h"
#include "TCP_Config.h"
#include "file_operation.h"
#include "tftp_file_transport.h"
#include "MDNS_server.h"
#include "updata.h"
#include "Collection.h"
#include "Send_Command.h"
#include "timer_if.h"
typedef enum
{
     receive_command_error                           =(-1),
	 send_command_error                              =(-2),
	 skip_error                                      =(-3),
	 read_id_error                                   =(-4),
	 statue_ok                                       =(0)
} error;
//#define DEBUG 1
#define one_hour_time_set           60*60*1000 /*5*60*1000*/             //中间停顿时间
#define watering_UI_delay_set       2*1000              //正在浇水界面延迟时间
#define fifty_minute_set            50*60*1000         //在浸泡50分钟后进行领一次浇水
//#define handle_non_water_yield      800                  //今天未浇水时的浇水量
#define automode_water_yeild        1500
#define handle_water_yield          800                //今天浇水时 的浇水量
#define min_15                      15*60*1000/*15*60*1000  */            //主界面操作      延时15分钟
#define screen_protect_await_time   3*60*1000            //屏幕保护等待时间
#define soak_mode_yeild             1500/*2000*/                 //浸泡时  浇水量
#define adjust_time_delay_set       3*1000              //时间调整延时时间
#define BGO_delay_set               4*1000              //后台界面等待时间
#define unlock_key_delay_set        2*1000              //解锁按键延时
#endif /* PLANT_CONTROL_PTOTOBUFFER_USER_H_ */
