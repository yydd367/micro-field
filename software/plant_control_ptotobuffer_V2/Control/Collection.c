/*
 * Collection.c
 *
 *  Created on: 2016年3月19日
 *      Author: Kang
 */
#include "Collection.h"
#include "user.h"
#include "task.h"
DHT_GET DHT11_OUT,DHT11_IN;
uint16_t num=0;
uint8_t dht_t1=0,dht_t2=0;
uint8_t dht_d1=0,dht_d2=0;
uint8_t dht_chk=0;
uint8_t rulse_date[40]={0};
uint8_t plan_start=0;
uint8_t last_updata_plant_day=0;
UART_VALUE get_uart_sebsor={0,0,1};				//存储前面板获取的温度，湿度，水为值
RULES_DATA rule_data;
Control_data controlrule={0};		//存储当前应该控制的信息
uint8_t flag_first_water=1;				//种植开始第一次浇水标志位
uint8_t control_time=5;
uint8_t control_lamp_mod=0;
uint8_t FAN_state=0;
uint8_t flag_watering_stop=0;
char plant_name[20]="CommandMethod.txt";
error error_statue=statue_ok;
//*****************************************************************************
//
//! Application startup display on UART
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void task_flow_get(void *pvParameters)
{
//	GPIO_IF_Toggle(6);
}
//*****************************************************************************
//
//! Application startup display on UART
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
uint8_t dht_read(uint8_t DhtPin)
{
	uint8_t i=0,dat=0;
//	MAP_GPIODirModeSet(GPIOA1_BASE, 0x40, GPIO_DIR_MODE_IN);
	for(i=0;i<8;i++)
	{
		num=2;
		while((GPIO_IF_Get(DhtPin)==0)&&((num++)<=10000));
		MAP_UtilsDelay(550);
		if(GPIO_IF_Get(DhtPin)==1)
		{
			num=2;
			dat=dat<<1;
			dat|=0x01;
			//UART_PRINT("1");
			while((GPIO_IF_Get(DhtPin)==1)&&((num++)<=10000));
		}
		else
		{
			dat=dat<<1;
		    //UART_PRINT("0");
		}
//		UART_PRINT("dat is %d",dat);
	}
	return dat;
}
//*****************************************************************************
//
//! Application startup display on UART
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
DHT_GET dht_getdat(uint8_t DhtPin)
{
	DHT_GET collect_dht={0,0,0,0,0},old_collect;
	//UART_PRINT("begin\r\n");
	if(DhtPin==OUTSIDE_DHT11)
		old_collect=DHT11_OUT;
	else
		old_collect=DHT11_IN;
	if(DhtPin==OUTSIDE_DHT11)
		MAP_GPIODirModeSet(GPIOA3_BASE, 0x40, GPIO_DIR_MODE_OUT);
	else
		MAP_GPIODirModeSet(GPIOA3_BASE, 0x80, GPIO_DIR_MODE_OUT);
	GPIO_IF_Set(DhtPin,0);
//	MAP_UtilsDelay(510000);
	if(DhtPin==OUTSIDE_DHT11)
		osi_Sleep(5);
	else
	    osi_Sleep(40);
	GPIO_IF_Set(DhtPin,1);
	MAP_UtilsDelay(510);//等待40us 等待DHT11相应
	//GPIO_IF_Set(DhtPin,1);
	if(DhtPin==OUTSIDE_DHT11)
		MAP_GPIODirModeSet(GPIOA3_BASE, 0x40, GPIO_DIR_MODE_IN);
	else
		MAP_GPIODirModeSet(GPIOA3_BASE, 0x80, GPIO_DIR_MODE_IN);
	if(GPIO_IF_Get(DhtPin)==0)
	{
		//UART_PRINT("yingda\r\n");
		num=2;while((GPIO_IF_Get(DhtPin)==0)&&((num++)<=10000));
		num=2;while((GPIO_IF_Get(DhtPin)==1)&&((num++)<=10000));
		collect_dht.humi1=dht_read(DhtPin);
		collect_dht.humi2=dht_read(DhtPin);
		collect_dht.temp1=dht_read(DhtPin);
		collect_dht.temp2=dht_read(DhtPin);
		collect_dht.check=dht_read(DhtPin);
	}
	if(DhtPin==OUTSIDE_DHT11)
		MAP_GPIODirModeSet(GPIOA3_BASE, 0x40, GPIO_DIR_MODE_OUT);
	else
		MAP_GPIODirModeSet(GPIOA3_BASE, 0x80, GPIO_DIR_MODE_OUT);
	GPIO_IF_Set(DhtPin,1);
	//UART_PRINT("collect_check is         %d\r\n",collect_dht.check);
	if(((collect_dht.humi1+collect_dht.humi2+collect_dht.temp1+collect_dht.temp2)&0xff)!=collect_dht.check)
	{
	    return old_collect;
	}
	else
	    return collect_dht;
//	MAP_UtilsDelay(100000);
}

//*****************************************************************************
//
//! Application startup display on UART
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void dht_init(void)
{
	 MAP_UtilsDelay(8000000);
	 GPIO_IF_Set(30, 1);
	 GPIO_IF_Set(31, 1);
	 UART_PRINT("DHT11 Init complete!\r\n");
}
//*****************************************************************************
//
//! 把文件规则解析成一天天的股则，并取出各个段的信息
//!
//! \param  bufferin 传入要解析的规则文件
//! \param  mod 传入要解析的模式
//!			DATE_MOD 只解析日期和第一天的种植规则
//!		    DATA_MOD 解析相应种植天数的规则
//!
//! \return none
//!
//*****************************************************************************
void dispose_control_file(char *bufferin,uint8_t mod)
{
	;
}
//*****************************************************************************
//
//! 得到规则中的数据
//!
//! \param  inputdata 输入的数据
//! \param  data_segment 协议中的段位信息
//!
//! \return none
//!
//*****************************************************************************
void dispose_all_data(char *inputdata,uint8_t data_segment)
{
	;
}
//*****************************************************************************
//
//! rule_data_init 数据初始化程序
//!
//!
//! \return none
//!
//*****************************************************************************
void rule_data_init(void)
{
	;
}

//*****************************************************************************
//
//! 更新种植规则函数
//!
//! \param  filename 要读取的种植文件
//! \param  current_plan_date 当前的种植天数
//! \param  record_rule_date 第一次更新种植文件时获取的日期信息
//! \param
//!
//! \return none
//!
//*****************************************************************************
void updata_plan_day_rule(char *filename,uint8_t current_plan_date,uint8_t * record_rule_date)
{
	;
}
//*****************************************************************************
//
//! fan_control 风扇控制
//! \param mod  FAN_FOREWARD 正转
//!				FAN_REVERSAL 反转
//!				FAN_STOP 停止
//!
//! \return none
//!
//*****************************************************************************

void fan_control(FAN_MOD mod)
{
	;
}
//*****************************************************************************
//
//! fan_control 风扇控制
//! \param mod  FAN_FOREWARD 正转
//!				FAN_REVERSAL 反转
//!				FAN_STOP 停止
//!
//! \return none
//!
//*****************************************************************************
void start_plant(void)
{
	;
}
//*****************************************************************************
//
//! fan_control 风扇控制
//! \param mod  FAN_FOREWARD 正转
//!				FAN_REVERSAL 反转
//!				FAN_STOP 停止
//!
//! \return none
//!
//*****************************************************************************
void stop_plant(void)
{
	;
}
void reset_plant(void)
{
	;
}
//*****************************************************************************
//
//! 日志文件控制函数
//! \param mod  NONE
//!
//! \return none
//!
//*****************************************************************************
void restore_state(TCP_Message control_message)
{
	;
}
//*****************************************************************************
//
//! 发送LCD更新页面ID函数
//! \param mod  old_id   上一界面ID
//!             new_id   新界面ID
//!             data_len 字长
//!             command 命令类型
//!             addr[0] 地址0
//!             addr[1] 地址1
//!             data[0] 数据0
//!             data[1]
//!
//! \return none
//!
//*****************************************************************************
void send_commmand(int command,int parameter)
{
	                LCD_Control page_commond;
	                page_commond.commond=WriteVariableCommond;
	                page_commond.data[1]=parameter;
                	page_commond.data_len=0x05;
                	page_commond.addr[0]=0x00;
	                page_commond.data[0]=0x00;
	                switch(command)
	                {
	                case one_hour_minute:
	                	 page_commond.addr[1]=0x41;       break;
	                case one_hour_second:
	                	 page_commond.addr[1]=0x42;       break;
	                case time_h:                                              //刷新参数，无需保存
	                    page_commond.addr[1]=0x05;       break;
	                case time_m_shi:
	                	page_commond.addr[1]=0x01;       break;
	                case time_h_set:
	                	page_commond.addr[1]=0x31;       break;
	                case time_m_ge:
	                	page_commond.addr[1]=0x02;       break;
	                case time_m_shi_set:
	                	page_commond.addr[1]=0x34;       break;
	                case time_m_ge_set:
	                	page_commond.addr[1]=0x35;       break;
	                case tem:
	                	page_commond.addr[1]=0x09;       break;
	                case humi:
	                	page_commond.addr[1]=0x0b;       break;
	                case lamp_on_h:
	                	page_commond.addr[1]=0x0d;       break;
	                case lamp_on_min:
	                	page_commond.addr[1]=0x0f;       break;
	                case lamp_close_h:
	                	page_commond.addr[1]=0x11;       break;
	                case lamp_clode_min:
	                	page_commond.addr[1]=0x13;       break;
	                case water_yeild_set:
	                	page_commond.data[1]=parameter;
	                	page_commond.data[0]=(parameter>>8);
	                	page_commond.addr[1]=0x17;       break;
	                case water_display:
	                	page_commond.data[1]=parameter;
	                	page_commond.data[0]=(parameter>>8);
	                	page_commond.addr[1]=0x40;       break;
	                case water_interval_set:
	                	page_commond.addr[1]=0x19;       break;
	                case water_on_h:
	                	page_commond.addr[1]=0x1a;       break;
	                case water_on_min:
	                	page_commond.addr[1]=0x1c;       break;
	                case tem_set:
	                	page_commond.addr[1]=0x1e;       break;
	                case humi_set:
	                	page_commond.addr[1]=0x21;       break;
	                case skip:
	                	page_commond.commond=WriteRegisterCommond;
	                	page_commond.data_len=0x04;
	                	page_commond.addr[0]=0x03;       break;
	                case read_id:
	                	page_commond.commond=ReadRegisterCommond;
	                	page_commond.data_len=0x03;
	                	page_commond.addr[0]=0x03;
	                	page_commond.data[0]=parameter;     //0x02
	                case lackwater_blink_on:
	                	page_commond.addr[1]=0x23;       break;
	                case lackwater_blink_off:
	                	page_commond.addr[1]=0x23;       break;
	                case last_water_day:
	                	page_commond.addr[1]=0x36;       break;
	                case last_water_filed:
	                	page_commond.data[1]=parameter;
	                	page_commond.data[0]=(parameter>>8);
	                	page_commond.addr[1]=0x37;       break;
	                case next_water_day:
	                	page_commond.addr[1]=0x38;       break;
	                case next_water_yeild:
	                	page_commond.data[1]=parameter;
	                	page_commond.data[0]=(parameter>>8);
	                	page_commond.addr[1]=0x39;       break;
	            	default:
	            		 error_statue=send_command_error;
	            		 printf("error_statue is %d",send_command_error);
	            		 break;
	                }
	                Send_LCD_Commond(page_commond);
}
void page_skip(uint8_t external_statue)
{
//	if((present_id>0&&present_id<34))
//	{
		switch(external_statue)
		{
		case 0x00:                          //都关
			send_commmand(skip,24);   break;
		case 0x04:                          //开风
			send_commmand(skip,23);   break;
		case 0x02:                          //开水
			send_commmand(skip,20);   break;
		case 0x06:                          //开水  开风
			send_commmand(skip,21);   break;
		case 0x01:                          //开灯
			send_commmand(skip,17);   break;
		case 0x05:                          //开灯  开风
			send_commmand(skip,22);   break;
		case 0x03:                          //开灯 开水
			send_commmand(skip,18);   break;
		case 0x07:                          //全开
			send_commmand(skip,19);   break;
		case 0x08:                          //都关
			send_commmand(skip,16);   break;
		case 0x0c:                          //开风
			send_commmand(skip,1);   break;
		case 0x0a:                          //开水
			send_commmand(skip,7);   break;
		case 0x0e:                          //开水  开风
			send_commmand(skip,5);   break;
		case 0x09:                          //开灯
			send_commmand(skip,3);   break;
		case 0x0d:                          //开灯  开风
			send_commmand(skip,9);   break;
		case 0x0b:                          //开灯 开水
			send_commmand(skip,12);   break;
		case 0x0f:                          //全开
			send_commmand(skip,11);   break;
		default:
			 error_statue=skip_error;
			 printf("error_statue is %d",skip_error);
			 break;
		}
}
