/*
 * Collection.h
 *
 *  Created on: 2016年3月19日
 *      Author: Kang
 */

#ifndef PLANT_CONTROL_PTOTOBUFFER_CONTROL_COLLECTION_H_
#define PLANT_CONTROL_PTOTOBUFFER_CONTROL_COLLECTION_H_
#include "systerm_init.h"
#define 	FLOW_CHANGE 	5/*6.25*/
#define		OUTSIDE_DHT11	30
#define		INSIDE_DHT11	31
#define     time_h          1
#define     time_m_ge       2
#define     time_m_shi      19
#define     time_m_ge_set   20
#define     time_m_shi_set  21
#define     time_h_set      22
#define     tem             3
#define     humi            4
#define     lamp_on_h       5
#define     lamp_on_min     6
#define     lamp_close_h    7
#define     lamp_clode_min  8
#define     water_yeild_set 9
#define     water_interval_set  10
#define     water_on_h      11
#define     water_on_min    12
#define     tem_set         13
#define     humi_set        14
#define     skip            15
#define     read_id         16
#define     lackwater_blink_on    17
#define     lackwater_blink_off   18
#define     water_display         23
#define     last_water_day        24
#define     last_water_filed      25
#define     next_water_day        26
#define     next_water_yeild      27
#define     one_hour_minute       28
#define     one_hour_second       29
//****************************************************************************
//                     结构体定义
//****************************************************************************

typedef struct
{
	uint8_t temp1;
	uint8_t temp2;
	uint8_t humi1;
	uint8_t humi2;
	uint8_t check;
}DHT_GET;

typedef struct
{
	uint8_t uart_temp;
	uint8_t uart_humi;
	uint8_t uart_water_leverl;
}UART_VALUE;

typedef struct
{
	uint8_t water_count;
	uint8_t water_time[6][4+1];
	uint8_t water_yield[6][4+1];
	uint8_t water_frequency[6][2+1];

}Water_Control;

typedef struct
{
	uint8_t fan_count;
	uint8_t fan_time[7][2+1];
	uint8_t fan_temp[7][2+1];
	uint8_t fan_humi[7][2+1];
}FAN_Control;

typedef struct
{
	uint8_t lamp_count;
	uint8_t lamp_time[6][2+1];
	uint8_t lamp_color[6][3+1];
}Lamp_Control;

typedef struct
{
	uint8_t rule_date;
	Water_Control rule_water;
	FAN_Control rule_fan;
	Lamp_Control rule_lamp;
}RULES_DATA;

typedef struct
{
	uint8_t control_temp;
	uint8_t control_humi;
	int8_t control_lamp1_color;
	int8_t control_lamp2_color;
	int8_t control_lamp3_color;
	int8_t control_color;
	uint16_t control_water_yield;
	uint8_t control_water_frequency;
}Control_data;

typedef enum
{
	DATE_MOD,
	DATA_MOD

}RULSE_MODE;
typedef enum
{
	FAN_FOREWARD=0,
	FAN_REVERSAL,
	FAN_STOP
}FAN_MOD;
typedef struct
{
	uint8_t lamp_on[2];
	uint8_t lamp_close[2];
	uint16_t water_yeild;
	uint8_t water_interval;
	uint8_t water_on[2];
	uint8_t Temperature;
	uint8_t humidity;
}Parameter;
//****************************************************************************
//                      变量外部扩展
//****************************************************************************
//extern long test_clock;
extern DHT_GET DHT11_OUT,DHT11_IN;
extern UART_VALUE get_uart_sebsor;
extern uint8_t rulse_date[40];
extern RULES_DATA rule_data;
extern uint8_t plan_date;
extern uint8_t plan_start;
extern Control_data controlrule;
extern uint8_t flag_first_water;
extern char plant_name[20];
extern uint8_t last_updata_plant_day;
extern uint8_t control_time;
extern uint8_t control_lamp_mod;
extern uint8_t FAN_state;
extern uint8_t flag_watering_stop;
//****************************************************************************
//                      LOCAL FUNCTION PROTOTYPES
//****************************************************************************
void task_flow_get(void *pvParameters);
uint8_t dht_read(uint8_t DhtPin);
DHT_GET dht_getdat(uint8_t DhtPin);
void dht_init(void);
void dispose_control_file(char *bufferin,uint8_t mod);
void rule_data_init(void);
void dispose_all_data(char *inputdata,uint8_t data_segment);
void updata_plan_day_rule(char *filename,uint8_t current_plan_date,uint8_t * record_rule_date);
void fan_control(FAN_MOD mod);
void start_plant(void);
void stop_plant(void);
void reset_plant(void);
void restore_state(TCP_Message control_message);
void send_commmand(int command,int parameter);
void page_skip(uint8_t external_statue);
#endif /* PLANT_CONTROL_PTOTOBUFFER_CONTROL_COLLECTION_H_ */
