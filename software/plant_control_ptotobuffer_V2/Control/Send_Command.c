/*
 * Send_Commond.c
 *
 *  Created on: 2016年3月23日
 *      Author: Kang
 */
#include "Send_Command.h"
#include "user.h"
#include "task.h"
//*****************************************************************************
//
//! Sent_ssid
//!	TCP客户端返回扫描到的SSID
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void Sent_ssid(void)
{

//	_i16 check=0;
	uint8_t j=0,i=0;	//
#ifdef  PROTO_BUFFER
	long lRetVal=-1;
	DATA ssid_data=DATA_init_default;
	char mac[18]="";
	char send_ssid[5][34]={""};
#endif
//	if(Current_Mode==ROLE_AP)													//如果当前模式为AP模式，执行下面操作
//	{

	sprintf(mac,"%x:%x:%x:%x:%x:%x",macAddressVal[0],macAddressVal[1],macAddressVal[2],\
									macAddressVal[3],macAddressVal[4],macAddressVal[5]);			//把要本机的mac地址绑定到要发送的mac地址上
		for(j=0;j<6;j+=5)
		{
			Semaphore_pend(TCP_Buffer_Pro, 100);					//等待listen数据信号量释放
			if(Current_Mode==ROLE_AP)
			    g_ulDestinationIp = g_ulStaIp;										//IP为连接到客户机的ip地址
			g_ulPacketCount = 1;												//发送数据量为1
#ifdef  PROTO_BUFFER
			for(i=0;i<5;i++)
			{
				memset(send_ssid[i],'\0',34);
				sprintf(send_ssid[i],"%d%s",RSSI_Strength[j+i],g_NetEntries[j+i].ssid);
			}
			ssid_data.DATA1 = send_ssid[0];//把扫描的的ssid信息绑定到发送的指令上
			ssid_data.DATA2 = send_ssid[1];
			ssid_data.DATA3 = send_ssid[2];
			ssid_data.DATA4 = send_ssid[3];
			ssid_data.DATA5 = send_ssid[4];
			ssid_data.DATA6=mac;
			memset(g_cBsdBuf,'\0',BUF_SIZE);
			lRetVal= sent_tcp_command(g_cBsdBuf,sizeof(g_cBsdBuf),"SEND_SSID",MOD_SSID_MOD,1,ssid_data);	//发送SSID指令
			if(lRetVal<=0)
				ERR_PRINT(lRetVal);
#else
			sprintf(g_cBsdBuf, "CUST,TCP,0,1,0,0,0,0,%s,%s,%s,%s,%s,%d,%d,~\r\n",
							g_NetEntries[j].ssid,g_NetEntries[j+1].ssid,g_NetEntries[j+2].ssid,g_NetEntries[j+3].ssid,\
							g_NetEntries[j+4].ssid,Current_Mode,check);										//发送缓存  设置为SSID命令
#endif
			Semaphore_post(TCP_Buffer_Pro);																	//释放信号量
			BsdTcpClient(g_ulDestinationPort);																//TCP客户端发送数据
			osi_Sleep(200);
		}
}
//*****************************************************************************
//
//! Send_temperature
//!	发送温度指令
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void Send_temperature(void)
{
////	uint8_t check=0;
//#ifdef  PROTO_BUFFER
//	long lRetVal=-1;
//	DATA temDHT11=DATA_init_default;
//	com_DHT11_sent SET_TEM={"","","","",""};
//	char mac[18]="";
//#endif
//	UART_PRINT("tem=%d.%d,shidu=%d.%d,%d\r\n",DHT11.temp1,DHT11.temp2,DHT11.humi1,DHT11.humi2,DHT11.check);
//	Report_User("CUST,UART,1,2,0,0,0,0,%d,0,0,~",1);				//请求前面板发送前面板的温度和湿度以及水位信息
//	osi_Sleep(100);
//	Semaphore_pend(TCP_Buffer_Pro, 100);				//等待listen数据信号量释放
////	g_ulDestinationIp = 0xC0A84F72;									//此为需要接收数据的上位机
////	g_ulDestinationIp=g_ulStaIp;
////	g_ulDestinationIp = 0xc0a80101;
//    if(Current_Mode==ROLE_AP)
//        g_ulDestinationIp = g_ulStaIp;
//	g_ulPacketCount = 1;											//发送数据量为1
//	memset(g_cBsdBuf,'\0',BUF_SIZE);
//#ifdef  PROTO_BUFFER
////	temDHT11.DATA1=(char * )(&(DHT11.temp1));
////	temDHT11.DATA2=(char * )(&(DHT11.humi1));
////	temDHT11.DATA3=(char * )(&Current_Mode );
//	sprintf(mac,"%x:%x:%x:%x:%x:%x",macAddressVal[0],macAddressVal[1],macAddressVal[2],\
//									macAddressVal[3],macAddressVal[4],macAddressVal[5]);
//	sprintf(SET_TEM.temp,"%d",DHT11.temp1);
//	sprintf(SET_TEM.humi,"%d",DHT11.humi1);
//	sprintf(SET_TEM.state,"%d",Current_Mode);
//	sprintf(SET_TEM.temp1,"%d",get_uart_sebsor.uart_temp);
//	sprintf(SET_TEM.humi1,"%d",get_uart_sebsor.uart_humi);
//	temDHT11.DATA1 =SET_TEM.temp;
//	temDHT11.DATA2 =SET_TEM.humi;
//	temDHT11.DATA3 =SET_TEM.state;
//	temDHT11.DATA4 =SET_TEM.temp1;
//	temDHT11.DATA5 =SET_TEM.humi1;
//	temDHT11.DATA6 =mac;
//	lRetVal= sent_tcp_command(g_cBsdBuf,sizeof(g_cBsdBuf),"SEND_TEM",MOD_TEM_MOD,1,temDHT11);//序列化温度指令
//	if (lRetVal <= 0)
//		ERR_PRINT(lRetVal);
//#else
//	sprintf(g_cBsdBuf, "CUST,TCP,3,1,0,0,0,0,%d,%d,%d,%d,~\r\n",
//			DHT11.temp1,DHT11.humi1,Current_Mode,check);			//发送缓存  设置为发送温湿度指令
//#endif
//	Semaphore_post(TCP_Buffer_Pro);									//释放信号量
//	BsdTcpClient(g_ulDestinationPort);								//TCP客户端发送数据
	;
}
//*****************************************************************************
//
//! Sent_ssid
//!	TCP客户端返回扫描到的SSID
//! \param  none
//!
//! \return 如果搜索到的SSID与当前连接的SSID相等，就返回RSSI信息，否则就返回错误
//!
//*****************************************************************************
int8_t Send_RSSI(void)
{
	uint8_t i=0,count=0;
	count=GetScanResult(&g_NetEntries[0]);									//重新扫描rssi信息
	if(count>0)																//如果扫描到的有效SSID的条数大于0打印信息
		UART_PRINT("Scan Success  valile name count is %d\r\n",count);		//打印条目数
	for(i=0;i<20;i++)													//检索当前检测到的的
	{
		if(strcmp((char *)g_NetEntries[i].ssid,(char *)ssidname_get)==0)//判断检索到的SSID是否与当前连接的SSID相等
//			return g_NetEntries[i].rssi;								//相等就返回RSSI
			return RSSI_Strength[i];

	}
	return FALSE;														//没有搜寻到就返回错误
}
//*****************************************************************************
//
//! Send_Tank_State
//!	TCP发送水箱装啊提
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void Send_Tank_State(void)
{
#ifdef  PROTO_BUFFER
	DATA tank_data=DATA_init_default;
	long lRetVal=-1;
	char tank_state[2]={0};
	char mac[18]="";
//	Report_User("CUST,UART,1,2,0,0,0,0,%d,0,0,~",1);
//	osi_Sleep(100);
	sprintf(tank_state,"%d",get_uart_sebsor.uart_water_leverl);
	sprintf(mac,"%x:%x:%x:%x:%x:%x",macAddressVal[0],macAddressVal[1],macAddressVal[2],\
									macAddressVal[3],macAddressVal[4],macAddressVal[5]);
	tank_data.DATA1=tank_state;
	tank_data.DATA2=mac;
	Semaphore_pend(TCP_Buffer_Pro, 100);											   //等待listen数据信号量释放
	//	g_ulDestinationIp = 0xC0A84F72;															   //此为需要接收数据的上位机
	//	g_ulDestinationIp=g_ulStaIp;
	//	g_ulDestinationIp = 0xc0a80101;
    if(Current_Mode==ROLE_AP)
        g_ulDestinationIp = g_ulStaIp;
	g_ulPacketCount = 1;																		   //发送数据量为1
	memset(g_cBsdBuf,'\0',BUF_SIZE);
	lRetVal = sent_tcp_command(g_cBsdBuf, sizeof(g_cBsdBuf), "SEND_TANK",MOD_TANK_MOD,1,tank_data);//发送水箱状态
	if (lRetVal <= 0)
		ERR_PRINT(lRetVal);
	Semaphore_post(TCP_Buffer_Pro);									//释放信号量
	BsdTcpClient(g_ulDestinationPort);								//TCP客户端发送数据
#else
#endif
}
//*****************************************************************************
//
//! Sent_ssid
//!	TCP客户端返回扫描到的SSID
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void Send_Fan_State(void)
{
#ifdef  PROTO_BUFFER
	DATA Fan_data=DATA_init_default;
	long lRetVal=-1;
	char mac[18]="";
	char fan_state[2]="";
	sprintf(mac,"%x:%x:%x:%x:%x:%x",macAddressVal[0],macAddressVal[1],macAddressVal[2],\
										macAddressVal[3],macAddressVal[4],macAddressVal[5]);
//	char tank_state[3]={0};
	sprintf(fan_state,"%d",FAN_state);
	Fan_data.DATA1=fan_state;
//	Fan_data.DATA1=tank_state;
	Semaphore_pend(TCP_Buffer_Pro, 100);				//等待listen数据信号量释放
		//	g_ulDestinationIp = 0xC0A84F72;									//此为需要接收数据的上位机
		//	g_ulDestinationIp=g_ulStaIp;
		//	g_ulDestinationIp = 0xc0a80101;
    if(Current_Mode==ROLE_AP)
        g_ulDestinationIp = g_ulStaIp;
	g_ulPacketCount = 1;											//发送数据量为1
	memset(g_cBsdBuf,'\0',BUF_SIZE);
	lRetVal = sent_tcp_command(g_cBsdBuf, sizeof(g_cBsdBuf), "SEND_FAN",MOD_FAN_MOD,1, Fan_data);
	if (lRetVal <= 0)
		ERR_PRINT(lRetVal);
	Semaphore_post(TCP_Buffer_Pro);									//释放信号量
	BsdTcpClient(g_ulDestinationPort);								//TCP客户端发送数据
#else
#endif
}
//*****************************************************************************
//
//! Send_Plant_Date
//!	向上位机发送当前的种植天数
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void Send_Plant_Date(void)
{
#ifdef  PROTO_BUFFER
	DATA Plant_data=DATA_init_default;
	long lRetVal=-1;
	char Plantdate[3]={0};
	char mac[18]="";
	sprintf(Plantdate,"%d",plan_date);
	sprintf(mac,"%x:%x:%x:%x:%x:%x",macAddressVal[0],macAddressVal[1],macAddressVal[2],\
									macAddressVal[3],macAddressVal[4],macAddressVal[5]);
	Plant_data.DATA1=Plantdate;
	Plant_data.DATA2=mac;
	Semaphore_pend(TCP_Buffer_Pro, 100);				//等待listen数据信号量释放
		//	g_ulDestinationIp = 0xC0A84F72;									//此为需要接收数据的上位机
		//	g_ulDestinationIp=g_ulStaIp;
		//	g_ulDestinationIp = 0xc0a80101;
    if(Current_Mode==ROLE_AP)
        g_ulDestinationIp = g_ulStaIp;
	g_ulPacketCount = 1;											//发送数据量为1
	memset(g_cBsdBuf,'\0',BUF_SIZE);
	lRetVal = sent_tcp_command(g_cBsdBuf, sizeof(g_cBsdBuf), "SEND_DATE",MOD_PLANT_MOD,1, Plant_data);//发送当前的种植天数指令
	if (lRetVal <= 0)
		ERR_PRINT(lRetVal);
	Semaphore_post(TCP_Buffer_Pro);									//释放信号量
	BsdTcpClient(g_ulDestinationPort);								//TCP客户端发送数据
#else
#endif
}
//*****************************************************************************
//
//! Send_Plant_Type
//!	发送当前的种植类型指令
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void Send_Plant_Type(void)
{
#ifdef  PROTO_BUFFER
	DATA type_data=DATA_init_default;
	long lRetVal=-1;
	char mac[18]="";
//	char *typeplant="Eggplant";
	sprintf(mac,"%x:%x:%x:%x:%x:%x",macAddressVal[0],macAddressVal[1],macAddressVal[2],\
									macAddressVal[3],macAddressVal[4],macAddressVal[5]);
//	char tank_state[3]={0};
//	sprintf(tank_state,"%d",get_uart_sebsor.uart_water_leverl);
//	Fan_data.DATA1=tank_state;
	type_data.DATA1=plant_name;
	type_data.DATA2=mac;
	Semaphore_pend(TCP_Buffer_Pro, 100);				//等待listen数据信号量释放
		//	g_ulDestinationIp = 0xC0A84F72;							//此为需要接收数据的上位机
		//	g_ulDestinationIp=g_ulStaIp;
		//	g_ulDestinationIp = 0xc0a80101;
    if(Current_Mode==ROLE_AP)
        g_ulDestinationIp = g_ulStaIp;
	g_ulPacketCount = 1;											//发送数据量为1
	memset(g_cBsdBuf,'\0',BUF_SIZE);
	lRetVal = sent_tcp_command(g_cBsdBuf, sizeof(g_cBsdBuf), "SEND_TYPE",MOD_TYPE_MOD,1, type_data);//序列化当前的种植类型指令
	if (lRetVal <= 0)
		ERR_PRINT(lRetVal);
	Semaphore_post(TCP_Buffer_Pro);									//释放信号量
	BsdTcpClient(g_ulDestinationPort);								//TCP客户端发送数据
#else
#endif
}
//*****************************************************************************
//
//! Send_Time_Updata
//!	发送获取时间指令
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void Send_Time_Updata(void)
{
#ifdef  PROTO_BUFFER
	DATA Time_data=DATA_init_default;
	long lRetVal=-1;
	char *timeget="1";
	char mac[18]="";
	com_ip_sent ip_sent={"","",0};
	sprintf(mac,"%x:%x:%x:%x:%x:%x",macAddressVal[0],macAddressVal[1],macAddressVal[2],\
									macAddressVal[3],macAddressVal[4],macAddressVal[5]);//给mac地址赋值
	unsigned char len = sizeof(SlNetCfgIpV4Args_t);
	unsigned char ucDHCP;
	SlNetCfgIpV4Args_t ipV4 = {0};

			// get network configuration
	sl_NetCfgGet(SL_IPV4_STA_P2P_CL_GET_INFO, &ucDHCP, &len,(unsigned char *) &ipV4);	//获取client的ip地址，网管地址，是否为DHCP
	sprintf(ip_sent.ip,"%d.%d.%d.%d",
					 SL_IPV4_BYTE(ipV4.ipV4,3),
					 SL_IPV4_BYTE(ipV4.ipV4,2),
					 SL_IPV4_BYTE(ipV4.ipV4,1),
					 SL_IPV4_BYTE(ipV4.ipV4,0));
	sprintf(ip_sent.port,"%d",PORT_NUM);
//	sprintf(timeget,"%d",1);
	Time_data.DATA1=timeget;										//请求时间指令
	Time_data.DATA2=mac;
	Time_data.DATA3=ip_sent.ip;
	Time_data.DATA4=ip_sent.port;
	Semaphore_pend(TCP_Buffer_Pro, 100);				//等待listen数据信号量释放
		//	g_ulDestinationIp = 0xC0A84F72;									//此为需要接收数据的上位机
		//	g_ulDestinationIp=g_ulStaIp;
		//	g_ulDestinationIp = 0xc0a80101;
	if(Current_Mode==ROLE_AP)
		g_ulDestinationIp=g_ulStaIp;
	g_ulPacketCount = 1;											//发送数据量为1
	memset(g_cBsdBuf,'\0',BUF_SIZE);
	lRetVal = sent_tcp_command(g_cBsdBuf, sizeof(g_cBsdBuf), "SEND_TIME",MOD_TIME_MOD,1, Time_data);//序列化指令到g_cBsdBuf
	if (lRetVal <= 0)												//判断是否序列化成功
		ERR_PRINT(lRetVal);
	Semaphore_post(TCP_Buffer_Pro);									//释放信号量
	BsdTcpClient(g_ulDestinationPort);								//TCP客户端发送数据
#else
#endif
}
//*****************************************************************************
//
//! Send_Time_Updata
//!	发送获取时间指令
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void Send_IP(void)
{
#ifdef  PROTO_BUFFER
	int8_t lRetVal=-1;
	com_ip_sent ip_sent={"","",0};
	DATA IP_DATA=DATA_init_default;
	char mac[18]="";
	sprintf(mac,"%x:%x:%x:%x:%x:%x",macAddressVal[0],macAddressVal[1],macAddressVal[2],\
									macAddressVal[3],macAddressVal[4],macAddressVal[5]);//给mac地址赋值
	sprintf(ip_sent.ip,"192.168.1.1");
	sprintf(ip_sent.port,"%d",PORT_NUM);
	sprintf(ip_sent.state,"%d",Current_Mode);

	IP_DATA.DATA1=ip_sent.ip;
	IP_DATA.DATA2=ip_sent.port;
//		IP_DATA.DATA3=(char *)(&(ip_sent.state));
	IP_DATA.DATA3=ip_sent.state;
	IP_DATA.DATA4=mac;
	IP_DATA.DATA5=pcSsidName;
	IP_DATA.DATA6=NULL;
	Semaphore_pend(TCP_Buffer_Pro,100);
	if(Current_Mode==ROLE_AP)
		g_ulDestinationIp=g_ulStaIp;
	g_ulPacketCount = 1;
	lRetVal= sent_tcp_command(g_cBsdBuf,sizeof(g_cBsdBuf),"SEND_IP",MOD_SEND_IP,1,IP_DATA);
	if(lRetVal<=0)
		ERR_PRINT(lRetVal);
	Semaphore_post(TCP_Buffer_Pro);
	BsdTcpClient(g_ulDestinationPort);
#else
#endif
}
//*****************************************************************************
//
//! Send_Time_Updata
//!	发送获取时间指令
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void Send_lamp_color(char mod)
{
	int8_t lRetVal=-1;
	com_temp_color_sent lamp_color={"","","",""};
	DATA LAMP_DATA=DATA_init_default;
	uint32_t color=0;
	char mac[18]="";
	sprintf(mac,"%x:%x:%x:%x:%x:%x",macAddressVal[0],macAddressVal[1],macAddressVal[2],\
									macAddressVal[3],macAddressVal[4],macAddressVal[5]);//给mac地址赋值
	color=controlrule.control_lamp1_color*100*100+controlrule.control_lamp2_color*100+controlrule.control_lamp3_color;
	sprintf(lamp_color.mod,"%d",mod);
	sprintf(lamp_color.lamp_color,"%ld",color);
//	sprintf(lamp_color.lamp2_color,"%d",controlrule.control_lamp2_color);
//	sprintf(lamp_color.lamp3_color,"%d",controlrule.control_lamp3_color);
	sprintf(lamp_color.kind,"%d",control_lamp_mod);
	sprintf(lamp_color.time,"%d",control_time);
	if(mod==2)
	{
		LAMP_DATA.DATA1=lamp_color.mod;
		LAMP_DATA.DATA2=lamp_color.lamp_color;
	//		IP_DATA.DATA3=(char *)(&(ip_sent.state));
		LAMP_DATA.DATA3=lamp_color.kind;
		LAMP_DATA.DATA4=lamp_color.time;
		LAMP_DATA.DATA5=mac;
	}
	else
		LAMP_DATA.DATA1=lamp_color.mod;
	Semaphore_pend(TCP_Buffer_Pro,100);
	if(Current_Mode==ROLE_AP)
		g_ulDestinationIp=g_ulStaIp;
	g_ulPacketCount = 1;
	lRetVal= sent_tcp_command(g_cBsdBuf,sizeof(g_cBsdBuf),"SEND_LAMP",MOD_LAMP_MOD,1,LAMP_DATA);
	if(lRetVal<=0)
		ERR_PRINT(lRetVal);
	Semaphore_post(TCP_Buffer_Pro);
	BsdTcpClient(g_ulDestinationPort);
}
//*****************************************************************************
//
//! Send_Time_Updata
//!	发送获取时间指令
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void Send_Device(char mod)
{
	int8_t lRetVal=-1;
	DATA NAME_DATA = DATA_init_default;
	char mac[18] = "";
	char Device_mod[2]="";
	sprintf(mac, "%x:%x:%x:%x:%x:%x", macAddressVal[0], macAddressVal[1],
			macAddressVal[2], macAddressVal[3], macAddressVal[4],
			macAddressVal[5]);								//给mac地址赋值
	sprintf(Device_mod,"%d",mod);
	NAME_DATA.DATA1 = Device_mod;
	NAME_DATA.DATA2 = pcSsidName;
	//		IP_DATA.DATA3=(char *)(&(ip_sent.state));
	NAME_DATA.DATA3 = mac;
	Semaphore_pend(TCP_Buffer_Pro, 100);
	if (Current_Mode == ROLE_AP)
		g_ulDestinationIp = g_ulStaIp;
	g_ulPacketCount = 1;
	lRetVal = sent_tcp_command(g_cBsdBuf, sizeof(g_cBsdBuf), "SEND_NAME",
			MOD_NAME_MOD, 1, NAME_DATA);
	if (lRetVal <= 0)
		ERR_PRINT(lRetVal);
	Semaphore_post(TCP_Buffer_Pro);
	BsdTcpClient(g_ulDestinationPort);
}
