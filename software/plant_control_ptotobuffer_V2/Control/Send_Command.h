/*
 * Send_Command.h
 *
 *  Created on: 2016��3��23��
 *      Author: Kang
 */

#ifndef PLANT_CONTROL_PTOTOBUFFER_CONTROL_SEND_COMMAND_H_
#define PLANT_CONTROL_PTOTOBUFFER_CONTROL_SEND_COMMAND_H_
#include "systerm_init.h"
void Send_Device(char mod);
void Sent_ssid(void);
void Send_temperature(void);
int8_t Send_RSSI(void);
void Send_Tank_State(void);
void Send_Fan_State(void);
void Send_Plant_Date(void);
void Send_Plant_Type(void);
void Send_Time_Updata(void);
void Send_IP(void);
void Send_lamp_color(char mod);
#endif /* PLANT_CONTROL_PTOTOBUFFER_CONTROL_SEND_COMMAND_H_ */
