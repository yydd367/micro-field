/*
 * systerm_init.c
 *
 *  Created on: 2016年2月27日
 *      Author: Kang
 */
#include "systerm_init.h"
#include "Collection.h"
const char     pcDigits[] = "0123456789"; /* variable used by itoa function */
uint8_t steam_len=0;
//uint8_t Device_State=INIT_STATE;
//const char UART_LED_State[]="0knknllvy";
#if defined(ccs)
extern void (* const g_pfnVectors[])(void);
#endif
#if defined(ewarm)
extern uVectorEntry __vector_table;
#endif

void
BoardInit(void)
{
// In case of TI-RTOS vector table is initialize by OS itself
#ifndef USE_TIRTOS
    //
    // Set vector table base
    //
#if defined(ccs) || defined(gcc)
    MAP_IntVTableBaseSet((unsigned long)&g_pfnVectors[0]);
#endif
#if defined(ewarm)
    MAP_IntVTableBaseSet((unsigned long)&__vector_table);
#endif
#endif //USE_TIRTOS
    //
    // Enable Processor
    //
    MAP_IntMasterEnable();
    MAP_IntEnable(FAULT_SYSTICK);

    PRCMCC3200MCUInit();
}

//*****************************************************************************
//
//! itoa
//!
//!    @brief  Convert integer to ASCII in decimal base
//!
//!     @param  cNum is input integer number to convert
//!     @param  cString is output string
//!
//!     @return number of ASCII parameters
//!
//!
//
//*****************************************************************************
unsigned short itoa(short cNum, char *cString)
{
    char* ptr;
    short uTemp = cNum;
    unsigned short length;

    // value 0 is a special case
    if (cNum == 0)
    {
        length = 1;
        *cString = '0';

        return length;
    }

    // Find out the length of the number, in decimal base
    length = 0;
    while (uTemp > 0)
    {
        uTemp /= 10;
        length++;
    }

    // Do the actual formatting, right to left
    uTemp = cNum;
    ptr = cString + length;
    while (uTemp > 0)
    {
        --ptr;
        *ptr = pcDigits[uTemp % 10];
        uTemp /= 10;
    }

    return length;
}
//*****************************************************************************
//
//! RTC 使能函数
//!
//! \param  None
//!
//! \return lRetVal 开启成功返回1，否则返回0
//
//*****************************************************************************
long Systerminit(void)
{
	long lRetVal = -1;

    //7
    // Board Initialization
    //
    BoardInit();
//    lRetVal = ConfigureSimpleLinkToDefaultState();			//初始化设备，
//    if(lRetVal < 0)
//    {
//        if (DEVICE_NOT_IN_STATION_MODE == lRetVal)
//        {
//            UART_PRINT("Failed to configure the device in its default state\n\r");
//        }
//    }

    //UART_PRINT("Device is configured in default state \n\r");

    //
    // Assumption is that the device is configured in station mode already
    // and it is in its default state
    //
//    lRetVal = sl_Start(0, 0, 0);								//开启设备
//
//    UART_PRINT("Device started as STATION \n\r");
    //
    // configure the GPIO pins for LEDs,UART
    //
    PinMuxConfig();

    //
    // Configure the UART
    //
#ifndef NOTERM
    InitTerm();
#endif  //NOTERM
    InitTerm1();
    //
    // enable RTC
    //
    lRetVal=RTC_IF_Enable();
    if(lRetVal==0)
    	return lRetVal;
    //


    // Set RTC
    //
    RTC_IF_SetInitValue(0,0);

    //
    // Set timer
    //
    time_init();
    UART1_IF_ConfigureNIntEnable(UART1IntHandler);
    GPIO_IF_ConfigureNIntEnable(4,GPIO_RISING_EDGE,GpioA0Handle);
	//
	// Set pwm
	//
    pwm_init();
	//
	// DHT init
	//
	dht_init();
    return lRetVal;
}
//*****************************************************************************
//
//! 处理串口接收到的数据
//!
//! \param  data 需要处理的数据
//! \param backdata 返回处理后的数据
//!
//! \return None
//
//*****************************************************************************
void Data_Dispose(char *data ,char (*backdata)[5])
{
	;
}

//*****************************************************************************
//
//! 规则段处理程序
//!
//! \param  data需要处理的数据，count处理的第几段数据，mod处理的方式
//!
//! \return lRetVal 开启成功返回1，否则返回0
//
//*****************************************************************************
void RULSE_Dispose(char *data,char count,char mod)
{
	;
}
#ifdef  PROTO_BUFFER
//*****************************************************************************
//
//! 发送TCP指令
//!
//! \param  buffer 序列化后要存放的地方
//! \param  bufsize 要序列化的长度
//! \param	comname 命令名称
//! \param  sub1 命令指向
//! \param  dat 要序列化的数据内容
//!
//! \return lRetVal 序列成功返回1，否则返回0
//
//*****************************************************************************
long sent_tcp_command(char*buffer,size_t bufsize,char* comname,MOD mod,uint8_t sub1,DATA dat)
{
	TCP_Message command_message=TCP_Message_init_zero;							//定义要序列话的数组，同时初始化
	pb_ostream_t stream = pb_ostream_from_buffer((uint8_t*)buffer, bufsize);	//定义输出流，把序列化后的数组存放到buffer中，尺寸为bufsize
	long lRetVal=-1;
	strcpy(command_message.command_name,comname);								//拷贝命令名
	command_message.has_mod=true;												//序列模式开启
	command_message.has_data=true;												//序列数据量开启
	command_message.mod=mod;													//赋值模式
	command_message.SUB1=sub1;													//赋值命令指向 向tcp发送为1
	command_message.data=dat;													//赋值数据
	lRetVal=pb_encode(&stream, TCP_Message_fields, &command_message);			//进行序列化
	steam_len=stream.bytes_written;												//获取序列化的长度，也就是要通过tcp发送的字节数，这个需要注意，序列多少发送多少，否则上位机解析不成功
//	UART_PRINT("stream.len=%d\r\n",stream.bytes_written);
	return lRetVal;
}
//*****************************************************************************
//
//! 规则段处理程序
//!
//! \param  data需要处理的数据，count处理的第几段数据，mod处理的方式
//!
//! \return lRetVal 开启成功返回1，否则返回0
//
//*****************************************************************************
long gettcpcommand_dispose(char*buffer,size_t bufsize)
{
	long lRetVal=-1;
	pb_istream_t stream=pb_istream_from_buffer((uint8_t *)buffer, bufsize);		//建立输入流，
	lRetVal=pb_decode_user(&stream, TCP_Message_fields);						//反序列输入流，发送控制指令
//	UART_PRINT("len=%d,error=%s",stream.bytes_left,stream.errmsg);
	return lRetVal;
}
#endif
//*****************************************************************************
//
//! 规则段处理程序
//!
//! \param  data需要处理的数据，count处理的第几段数据，mod处理的方式
//!
//! \return lRetVal 开启成功返回1，否则返回0
//
//*****************************************************************************
void UART_State_Commond_Send(uint8_t state,uint8_t time)
{
//	Report_User("CUST,UART,5,2,0,0,0,0,%c,%02d,1,1,1,1,~",UART_LED_State[state],time);
}
