/*
 * systerm_init.h
 *
 *  Created on: 2016年2月27日
 *      Author: Kang
 */

#ifndef PLANT_CONTROL_PTOTOBUFFER_SYSTERM_INIT_SYSTERM_INIT_H_
#define PLANT_CONTROL_PTOTOBUFFER_SYSTERM_INIT_SYSTERM_INIT_H_
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
// Simplelink includes
#include "simplelink.h"

//Driverlib includes
#include "hw_types.h"
#include "hw_ints.h"
#include "hw_memmap.h"
#include "rom.h"
#include "rom_map.h"
#include "interrupt.h"
#include "prcm.h"
#include "utils.h"
#include  "uart.h"
#include "gpio.h"
#include "interrupt.h"
#include "pin.h"
#include "timer.h"
//Free_rtos/ti-rtos includes
#include "osi.h"

//Common interface includes
#include "gpio_if.h"
#ifndef NOTERM
#include "uart_if.h"
#endif
//#include "uart_if.h"
#include "common.h"
#include "pinmux.h"
#include "tftp.h"
#include "timer_if.h"
#include "RTC_if.h"
#include "Interrupt_it.h"

#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Memory.h>
//RTOS includes
#include <ti/sysbios/BIOS.h>

#include <ti/sysbios/family/arm/m3/Hwi.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Mailbox.h>
#include <ti/sysbios/knl/Queue.h>
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/knl/Swi.h>

#include <pb_encode.h>
#include <pb_decode.h>
#include "TCP_PROTO.pb.h"
//STA 信息
#define APPLICATION_NAME        "WLAN STATION"
#define APPLICATION_VERSION     "1.1.1"
//STA 外网测试地址
#define HOST_NAME               "www.ti.com"
//AP 信息
#define APP_NAME                "WLAN AP"
#define ALWAYS_WAIT_TIME		0
typedef enum{
    // Choosing -0x7D0 to avoid overlap w/ host-driver's error codes
    LAN_CONNECTION_FAILED = -0x7D0,
    INTERNET_CONNECTION_FAILED = LAN_CONNECTION_FAILED - 1,
    DEVICE_NOT_IN_STATION_MODE = INTERNET_CONNECTION_FAILED - 1,
	SOCKET_CREATE_ERROR= DEVICE_NOT_IN_STATION_MODE-1,
	BIND_ERROR = SOCKET_CREATE_ERROR - 1,
	LISTEN_ERROR = BIND_ERROR -1,
	SOCKET_OPT_ERROR = LISTEN_ERROR -1,
	CONNECT_ERROR = SOCKET_OPT_ERROR -1,
	ACCEPT_ERROR = CONNECT_ERROR - 1,
	SEND_ERROR = ACCEPT_ERROR -1,
	RECV_ERROR = SEND_ERROR -1,
	SOCKET_CLOSE_ERROR = RECV_ERROR -1,
	FILE_ALREADY_EXIST = SOCKET_CLOSE_ERROR-1,
	FILE_CLOSE_ERROR = FILE_ALREADY_EXIST - 1,
	FILE_NOT_MATCHED = FILE_CLOSE_ERROR - 1,
	FILE_OPEN_READ_FAILED = FILE_NOT_MATCHED - 1,
	FILE_OPEN_WRITE_FAILED = FILE_OPEN_READ_FAILED -1,
	FILE_READ_FAILED = FILE_OPEN_WRITE_FAILED - 1,
	FILE_WRITE_FAILED = FILE_READ_FAILED - 1,
	SERVER_GET_TIME_FAILED = FILE_WRITE_FAILED-1,
	DNS_LOOPUP_FAILED = SERVER_GET_TIME_FAILED-1,
	INIT_FAILED = DNS_LOOPUP_FAILED-1,
    STATUS_CODE_MAX = -0xBB8
}e_AppStatusCodes;
typedef struct
{
	char ip[16];
	char port[6];
	char  state[2];
}com_ip_sent;
typedef struct
{
	char temp[3];
	char humi[3];
	char  state[2];
	char temp1[3];
	char humi1[3];
}com_DHT11_sent;
typedef struct
{
	char mod[2];
	char lamp_color[13];
	char kind[2];
	char time[3];
}com_temp_color_sent;
//typedef struct
//{
//	char systerm_wifi_net_state;
//	char systerm_run_state;
//}systerm_state;
//typedef enum
//{
//	INIT_STATE=0,
//	AP_WATER_CONNECT,
//	AP_CONNECT,
//	CHANGE_NET,
//	STA_CONNECT_SUCCESSFUL,
//	GET_MDNS,
//	GET_COMMOND,
//	SYSTERM_START,
//	NORMAL_MOD,
//	SYSTERM_STOP
//}device_run_state;
typedef enum
{
	SSID_MOD=0,
	CONNECT_MOD,
	SEND_IP,
	TEM_MOD,
	LAMP_MOD
}Commond_TCP_mod_list;
typedef enum
{
	WIFI_RSSI_MOD=0,
	GET_VALUE_MOD,
	KEY1_MOD,
	KEY2_MOD,
	DOUBLE_KEY_MOD
}Commond_UART_mod_list;
typedef enum
{
	KEY1_SINGLE_SHAOT=1,
	KEY1_DOUBLE_SHAOT,
	KEY1_SINGLE_LONG
}UART_KEY1_COMMAND;
typedef enum
{
	KEY2_SINGLE_SHAOT=1,
	KEY2_DOUBLE_SHAOT,
	KEY2_SINGLE_LONG
}UART_KEY2_COMMAND;

//os configue
#define OSI_STACK_SIZE      2048

#define COMMUNICATE_TCP		"TCP";
#define COMMUNICATE_UART	"UART";
extern uint8_t steam_len;
//extern uint8_t time_begin=0;
//extern uint8_t Device_State;
void BoardInit(void);
unsigned short itoa(short cNum, char *cString);

void Data_Dispose(char *data ,char (*backdata)[5]);
void RULSE_Dispose(char *data,char count,char mod);
long Systerminit(void);

long sent_tcp_command(char*buffer,size_t bufsize,char* comname,MOD mod,uint8_t sub1,DATA dat);
long gettcpcommand_dispose(char*buffer,size_t bufsize);
void UART_State_Commond_Send(uint8_t state,uint8_t time);
#endif /* PLANT_CONTROL_PTOTOBUFFER_SYSTERM_INIT_SYSTERM_INIT_H_ */
