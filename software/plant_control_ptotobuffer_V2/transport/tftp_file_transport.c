/*
 * tftp_file_transport.c
 *
 *  Created on: 2016年2月29日
 *      Author: Kang
 */
#include "tftp_file_transport.h"
char tftp_get_char[GEI_FILE_SIZE_MAX]={0};
long tftp_getfile(char *filename, unsigned long uiFileSize)
{
	long pFileHandle;			// Pointer to file handle
	SlFsFileInfo_t pFsFileInfo;
	long lRetVal = -1;
	//_i32 DeviceFileHandle = -1;
//		_u8 ReadBuffer[100];
	//_u32 Offset=0;
	unsigned short uiTftpErrCode;
//	 uiFileSize = GEI_FILE_SIZE_MAX;

//	pucFileBuffer = malloc(uiFileSize);					//分配内存
//	if (NULL == pucFileBuffer)							//为空返回
//	{
//		UART_PRINT("Can't Allocate Resources\r\n");
//		return lRetVal;
//	}
	UART_PRINT("RECv begin.../r/n");
	memset(tftp_get_char, '\0', uiFileSize);			//给分配的内存赋值

	lRetVal = sl_TftpRecv(TFTP_IP, filename, (char *) tftp_get_char,\
							&uiFileSize, &uiTftpErrCode);				//从tftp中读取文件名为filename的文件，写入到filebuffer
	if (lRetVal < 0)													//判断返回值是否正确
	{
//		free(pucFileBuffer);
//		pucFileBuffer=NULL;
		ERR_PRINT(lRetVal);
		return lRetVal;
	}
	_u32 a=sizeof(tftp_get_char);
	UART_PRINT("jieshoude wenjian size is %d",a);
	UART_PRINT("RECV over\r\n");
//	UART_PRINT("%s\r\n",pucFileBuffer);


	lRetVal = sl_FsGetInfo((unsigned char *) filename, NULL, &pFsFileInfo);//获取文件信息

	if (lRetVal < 0)													   //lretval《0证明文件不存在，需要创建文件
		lRetVal = sl_FsOpen((unsigned char *) filename,\
				FS_MODE_OPEN_CREATE(uiFileSize,_FS_FILE_OPEN_FLAG_COMMIT |\
						_FS_FILE_PUBLIC_WRITE),NULL, &pFileHandle);
	else																	//否则证明文件存在，以读取的方式打开文件
		lRetVal = sl_FsOpen((unsigned char *) filename, FS_MODE_OPEN_WRITE,\
							NULL, &pFileHandle);
	if (lRetVal < 0)//判断返回值
	{
//		free(pucFileBuffer);
//		pucFileBuffer=NULL;
		ERR_PRINT(lRetVal);
		return lRetVal;
	}
	UART_PRINT("get info successful");


//	lRetVal=sl_FsDel((unsigned char *)"1.bin",token1);
//		if(lRetVal < 0)
//		{
//			ERR_PRINT(lRetVal);
//		}
//		UART_PRINT("del successful");
	lRetVal = sl_FsWrite(pFileHandle, 0,(uint8_t *) tftp_get_char, uiFileSize);//把缓存中的数据filebuffer写入到文件中

	if (lRetVal < 0)//判断返回值返回错误信息
	{
//		free(pucFileBuffer);
		lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
		ERR_PRINT(lRetVal);
		return lRetVal;
	}
//	free(pucFileBuffer);											//释放缓存
//	pucFileBuffer=NULL;												//指针空值
	UART_PRINT("TFTP Read Successful \r\n");

	lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);						//关闭文件
	return lRetVal;
}
//*****************************************************************************
//
//! Load the application from sFlash and execute
//!
//! \param ImgName is the name of the application image on sFlash
//! \param ulToken is the token for reading file (relevant on secure devices only)
//!
//! This function loads the specified application from sFlash and executes it.
//!
//! \return None.
//
//*****************************************************************************
//long tftp_sentfile(char *filename,char *newfilename)
//{
//	long pFileHandle;			// Pointer to file handle
//	SlFsFileInfo_t pFsFileInfo;
//	long lRetVal = -1;
////	unsigned short uiTftpErrCode;
//	unsigned long uiFileSize=0;
//	unsigned char *pucFileBuffer=NULL;
//
//	/* open same file which has been written with the server's file content */
//    lRetVal = sl_FsOpen((unsigned char *)filename,FS_MODE_OPEN_READ, \
//                            NULL,&pFileHandle);		 //打开要发送的文件
//    if(lRetVal < 0)	//判断返回值，返回错误信息
//    {
//        free(pucFileBuffer);
//        pucFileBuffer=NULL;
//        ERR_PRINT(lRetVal);
//        return lRetVal;
//    }
//
//    lRetVal = sl_FsGetInfo((unsigned char *)filename, NULL, &pFsFileInfo);//获取文件信息
//    if(lRetVal < 0)//判断返回值
//    {
//        lRetVal = sl_FsClose(pFileHandle,0,0,0);
//        free(pucFileBuffer);
//        pucFileBuffer=NULL;
//        ERR_PRINT(lRetVal);
//        return lRetVal;
//    }
//    uiFileSize = (&pFsFileInfo)->FileLen;//获取文件长度
//    pucFileBuffer = malloc(uiFileSize);					//分配内存
//    if (NULL == pucFileBuffer)							//为空返回
//    {
//    	UART_PRINT("Can't Allocate Resources\r\n");
//    	return lRetVal;
//    }
//    lRetVal = sl_FsRead(pFileHandle, 0,  pucFileBuffer, uiFileSize);//读取文件的内容到缓存中
////    UART_PRINT("%d,%s\r\n",uiFileSize,pucFileBuffer);
//    if(lRetVal < 0)
//    {
//        lRetVal = sl_FsClose(pFileHandle,0,0,0);
//        free(pucFileBuffer);
//        pucFileBuffer=NULL;
//        ERR_PRINT(lRetVal);
//        return lRetVal;
//    }
//
//    lRetVal = sl_FsClose(pFileHandle,0,0,0);//关闭文件
//
//    /* write to server with different file name */
////    lRetVal = sl_TftpSend(TFTP_IP, newfilename,(char *) pucFileBuffer,\
////                        &uiFileSize, &uiTftpErrCode  );//把缓存中的数据发送到tftp服务器上，存储为新的文件名
////    if(lRetVal < 0)
////    {
////        free(pucFileBuffer);
////        pucFileBuffer=NULL;
////        ERR_PRINT(lRetVal);
////        return lRetVal;
////    }
//    free(pucFileBuffer);
//    pucFileBuffer=NULL;
//    UART_PRINT("TFTP Write Successful \r\n");
//    return lRetVal;
//}
