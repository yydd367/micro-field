/*
 * tftp_file_transport.h
 *
 *  Created on: 2016��2��29��
 *      Author: Kang
 */

#ifndef PLANT_CONTROL_PTOTOBUFFER_TRANSPORT_TFTP_FILE_TRANSPORT_H_
#define PLANT_CONTROL_PTOTOBUFFER_TRANSPORT_TFTP_FILE_TRANSPORT_H_

#include "systerm_init.h"

extern char tftp_get_char[];
//#define TFTP_IP			0xc0a84f67	/* This is the host IP *0xac159002*/
#define GEI_FILE_SIZE_MAX (4*1024)
//#define TFTP_IP			0xac159001	/* This is the host IP *0xac159002*/
#define TFTP_IP			 0xc0a80103	/* This is the host IP *0xac159002  0x7B38450A*/
#define FILE_SIZE_MAX	(65*1024)	/* Max File Size set to 20KB */
#define UPDATA_SIZE_MAX	(51*1024)	/* Max File Size set to 20KB */
long tftp_getfile(char *filename, unsigned long uiFileSize);
//long tftp_sentfile(char *filename,char *newfilename);

#endif /* PLANT_CONTROL_PTOTOBUFFER_TRANSPORT_TFTP_FILE_TRANSPORT_H_ */
