/*
 * updata.c
 *
 *  Created on: 2016年3月2日
 *      Author: Kang
 */
#include "updata.h"
#include "tftp_file_transport.h"
#include "network_config.h"

const char g_acSNTPserver[30] = "s1a.time.edu.cn"; //Add any one of the above servers

// Tuesday is the 1st day in 2013 - the relative year
const char g_acDaysOfWeek2013[7][3] = {{"Tue"},
                                    {"Wed"},
                                    {"Thu"},
                                    {"Fri"},
                                    {"Sat"},
                                    {"Sun"},
                                    {"Mon"}};

const char g_acMonthOfYear[12][3] = {{"Jan"},
                                  {"Feb"},
                                  {"Mar"},
                                  {"Apr"},
                                  {"May"},
                                  {"Jun"},
                                  {"Jul"},
                                  {"Aug"},
                                  {"Sep"},
                                  {"Oct"},
                                  {"Nov"},
                                  {"Dec"}};

char g_acNumOfDaysPerMonth[12] = {31, 28, 31, 30, 31, 30,
                                        31, 31, 30, 31, 30, 31};

const char g_acDigits[] = "0123456789";
g_sAppData mytimedata;
real_time time_axis={0,0,0,0,0,0};
//unsigned long time_get=3688551110u;
unsigned long time_get=1356998400u/*1449410400u*/;

//unsigned long time_get=1479816221u;
#ifndef ccs
void Run(unsigned long ulBaseLoc)
{

  //
  // Set the SP
  //
  __asm("	ldr    sp,[r0]\n"
	"	add    r0,r0,#4");

  //
  // Jump to entry code
  //
  __asm("	ldr    r1,[r0]\n"
        "	bx     r1");
}
#else
__asm("    .sect \".text:Run\"\n"
      "    .clink\n"
      "    .thumbfunc Run\n"
      "    .thumb\n"
      "Run:\n"
      "    ldr    sp,[r0]\n"
      "    add    r0,r0,#4\n"
      "    ldr    r1,[r0]\n"
      "    bx     r1");
#endif
/*void Run(unsigned long ulBaseLoc)
{

  //
  // Set the SP
  //
  __asm("	ldr    sp,[r0]\n"
	"	add    r0,r0,#4");

  //
  // Jump to entry code
  //
  __asm("	ldr    r1,[r0]\n"
        "	bx     r1");
}*/
//*****************************************************************************
//
//!ImageLoader_reboot
//!
//! \brief  更新固件函数
//!
//! \param  filename 需要下载的文件名
//! \param  replacename 保存到flash中的固件名称
//!
//! \return 0 : success, -ve : failure
//
//*****************************************************************************
long ImageLoader_reboot(char *filename ,char *replacename)
{
	long pFileHandle;									// Pointer to file handle
	SlFsFileInfo_t pFsFileInfo;							//获取文件参数变量
	long lRetVal = -1;									//错误代码
	uint32_t savesize=0;
	unsigned char *pucFileBuffer=NULL;					//文件缓存指针
	unsigned long uiFileSize=0;							//传输文件的大小
	uiFileSize = UPDATA_SIZE_MAX;						//定义文件传输的大小

	lRetVal = sl_FsOpen((unsigned char *) filename, FS_MODE_OPEN_READ,
								NULL, &pFileHandle);		 //打开要发送的文件
	if (lRetVal < 0)	//判断返回值，返回错误信息
	{
		lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
//		pucFileBuffer=NULL;
		ERR_PRINT(lRetVal);
		return lRetVal;
	}

	lRetVal = sl_FsGetInfo((unsigned char *) filename, NULL, &pFsFileInfo);	//获取文件信息
	if (lRetVal < 0)	//判断返回值
	{
		lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
//		pucFileBuffer=NULL;
		ERR_PRINT(lRetVal);
		return lRetVal;
	}
	savesize=(&pFsFileInfo)->FileLen;//获取文件长度
	UART_PRINT("readsize=%d\r\n",savesize);
//	pucFileBuffer = malloc(savesize+1);					//分配内存
//	if (NULL == pucFileBuffer)							//为空返回
//	{
//	    UART_PRINT("Can't Allocate Resources\r\n");
//	    return lRetVal;
//	}
//	memset(pucFileBuffer,'\0',savesize+1);
	//memset(pucFileBuffer,0,savesize);
	lRetVal = sl_FsRead(pFileHandle, 0, (unsigned char *)pucFileBuffer, savesize);//读取文件的内容到缓存中
	if(lRetVal < 0)
	{
		lRetVal = sl_FsClose(pFileHandle,0,0,0);
//	    free(pucFileBuffer);
//	    pucFileBuffer=NULL;
	    ERR_PRINT(lRetVal);
	    return lRetVal;
	}
	lRetVal = sl_FsClose(pFileHandle,0,0,0);//关闭文件
//    UART_PRINT("%s\r\n",pucFileBuffer);
//	    free(pucFileBuffer);.0
//	    pucFileBuffer=NULL;
	UART_PRINT("Read file Successful \r\n");

	lRetVal = sl_FsGetInfo((unsigned char *) replacename, NULL, &pFsFileInfo);						//获取文件信息
	// UART_PRINT("nb\r\n");
	if (lRetVal < 0)																			//lretval《0证明文件不存在，需要创建文件
	{
		lRetVal = sl_FsOpen((unsigned char *) replacename,FS_MODE_OPEN_CREATE((4*1024),_FS_FILE_OPEN_FLAG_COMMIT |\
								_FS_FILE_PUBLIC_WRITE), NULL,&pFileHandle);						//创建文件
		if(lRetVal < 0)
		{
		        //
		        // File may already be created
		        //
			lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
			ASSERT_ON_ERROR(lRetVal);
		}
	}
	else																						//如果文件存在
	{
		lRetVal = sl_FsOpen((unsigned char *) replacename,FS_MODE_OPEN_WRITE,NULL, &pFileHandle);	//打开文件
		if (lRetVal < 0)																		//判断返回值
		{
			lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
			ASSERT_ON_ERROR(lRetVal);
		}

	}
	lRetVal = sl_FsWrite(pFileHandle, 0, (unsigned char *)pucFileBuffer, savesize);					//把缓存中的数据filebuffer写入到文件中
	if (lRetVal < 0)																			//判断返回值
	{
		lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
		ASSERT_ON_ERROR(lRetVal);
	}
	lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
/*	pucFileBuffer=(unsigned char *)0x20030bb0;			//分配文件传输的地址///c000时剩余空间为51K
	memset(pucFileBuffer, '\0', uiFileSize);			//给分配的内存赋值
	UART_PRINT("g_ulStaIp is %x",g_ulStaIp);
	lRetVal = sl_TftpRecv(g_ulStaIp, filename, (char *) pucFileBuffer,\
							&uiFileSize, &uiTftpErrCode);						//从tftp中读取文件名为filename的文件，写入到filebuffer
	if(lRetVal==0)
	{
		UART_PRINT("file large");
	}
	if (lRetVal < 0)															//判断返回值是否正确
	{
		free(pucFileBuffer);
		ERR_PRINT(lRetVal);
		return lRetVal;
	}
	UART_PRINT("jieshou successful");
	lRetVal=sl_FsDel((unsigned char *)replacename,token1);						//删除文件中原来的固件
	lRetVal = sl_FsGetInfo((unsigned char *) replacename, NULL, &pFsFileInfo);	//获取文件信息

	if (lRetVal < 0)													   		//lretval《0证明文件不存在，需要创建文件，FILE_SIZE_MAX定义最大的文件尺寸
		lRetVal = sl_FsOpen((unsigned char *) replacename,\
				FS_MODE_OPEN_CREATE(FILE_SIZE_MAX,_FS_FILE_OPEN_FLAG_COMMIT |\
						_FS_FILE_PUBLIC_WRITE),NULL, &pFileHandle);
	else																		//否则证明文件存在，以读取的方式打开文件
	{
		lRetVal=sl_FsDel((unsigned char *)replacename,token1);					//如果文件存在就删除源文件
		lRetVal = sl_FsOpen((unsigned char *) replacename,\
						FS_MODE_OPEN_CREATE(FILE_SIZE_MAX,_FS_FILE_OPEN_FLAG_COMMIT |\
								_FS_FILE_PUBLIC_WRITE),NULL, &pFileHandle);
	}
	if (lRetVal < 0)															//判断返回值
	{
		free(pucFileBuffer);
		ERR_PRINT(lRetVal);
		return lRetVal;
	}

	lRetVal = sl_FsWrite(pFileHandle, 0, pucFileBuffer, uiFileSize);			//把缓存中的数据filebuffer写入到文件中
	UART_PRINT("xieru successful");
	if (lRetVal < 0)															//判断返回值返回错误信息
	{
		free(pucFileBuffer);
		lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
		ERR_PRINT(lRetVal);
		return lRetVal;
	}
	UART_PRINT("TFTP Read Successful \r\n");

	lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);									//关闭文件*/
	return lRetVal;
}
//long ImageLoader_reboot(char *filename ,char *replacename)
//{
//	long pFileHandle,pFileHandle1;									// Pointer to file handle
//	SlFsFileInfo_t pFsFileInfo;							//获取文件参数变量
//	long lRetVal = -1;									//错误代码
//	unsigned char *pucFileBuffer=NULL;					//文件缓存指针
//	unsigned long token1=0;
//	uint32_t savesize=0;
//	/* open same file which has been written with the server's file content */
//	/* open same file which has been written with the server's file content */
////		pucFileBuffer=(unsigned char *)0x2001F000;			//分配文件传输的地址
////		memset(pucFileBuffer, '\0', uiFileSize);			//给分配的内存赋值
////	lRetVal = sl_FsOpen((unsigned char *) filename, FS_MODE_OPEN_READ,
////							NULL, &pFileHandle);		 //打开要发送的文件
////	if (lRetVal < 0)	//判断返回值，返回错误信息
////	{
////		lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
////		free(pucFileBuffer);
////		pucFileBuffer=NULL;
////		ERR_PRINT(lRetVal);
////		return lRetVal;
////	}
////	UART_PRINT("open file successful\r\n");
//	lRetVal = sl_FsGetInfo((unsigned char *) filename, NULL, &pFsFileInfo);	//获取文件信息
//	if (lRetVal < 0)	//判断返回值
//	{
//		lRetVal = sl_FsClose(pFileHandle, 0, 0, 0);
//		free(pucFileBuffer);
//		pucFileBuffer=NULL;
//		ERR_PRINT(lRetVal);
//		return lRetVal;
//	}
//	UART_PRINT("get file info successful\r\n");
//	savesize=(&pFsFileInfo)->FileLen;//获取文件长度
//	UART_PRINT("readsize=%d\r\n",savesize);
//	pucFileBuffer = malloc(savesize+1);
//    if (NULL == pucFileBuffer)							//为空返回
//    {
//    	UART_PRINT("Can't Allocate Resources\r\n");
//    	return lRetVal;
//    }
//    memset(pucFileBuffer,'\0',savesize+1);
////    lRetVal = sl_FsRead(pFileHandle, 0,pucFileBuffer, savesize);//读取文件的内容到缓存中
////    if(lRetVal < 0)
////    {
////        lRetVal = sl_FsClose(pFileHandle,0,0,0);
////        free(pucFileBuffer);
////        pucFileBuffer=NULL;
////        ERR_PRINT(lRetVal);
////        return lRetVal;
////    }
////    UART_PRINT("huancun successful\r\n");
//////    for(i=0;i<savesize;i++)
//////    	UART_PRINT("%2x ",*(pucFileBuffer+i));
//////    UART_PRINT("\r\n");
////    lRetVal=sl_FsDel((unsigned char *)replacename,token1);						//删除文件中原来的固件
////    lRetVal = sl_FsClose(pFileHandle,0,0,0);//关闭文件
////
//////	lRetVal = sl_FsGetInfo((unsigned char *) replacename, NULL, &pFsFileInfo);	//获取文件信息
//////	UART_PRINT("del successful\r\n");
//////	if (lRetVal < 0)													   		//lretval《0证明文件不存在，需要创建文件，FILE_SIZE_MAX定义最大的文件尺寸
//////		lRetVal = sl_FsOpen((unsigned char *) replacename,\
//////				FS_MODE_OPEN_CREATE(FILE_SIZE_MAX,_FS_FILE_OPEN_FLAG_COMMIT |\
//////						_FS_FILE_PUBLIC_WRITE),NULL, &pFileHandle1);
////
////	lRetVal = sl_FsWrite(pFileHandle1, 0, pucFileBuffer, savesize);			//把缓存中的数据filebuffer写入到文件中
////	UART_PRINT("xieru successful\r\n");
////	if (lRetVal < 0)															//判断返回值返回错误信息
////	{
////		lRetVal = sl_FsClose(pFileHandle1, 0, 0, 0);
////		ERR_PRINT(lRetVal);
////		return lRetVal;
////	}
////	UART_PRINT("Change Successful \r\n");
////	free(pucFileBuffer);
////	lRetVal = sl_FsClose(pFileHandle1, 0, 0, 0);									//关闭文件
////	return lRetVal;
//}
//*****************************************************************************
//
//!ImageLoader_reboot
//!
//! \brief  更新固件函数
//!
//! \param  filename 需要下载的文件名
//! \param  replacename 保存到flash中的固件名称
//!
//! \return 0 : success, -ve : failure
//
//*****************************************************************************
//void RunMagic(char *fileName)
//{
//
//	long retVal,fileHandle;
//	unsigned long token;
//	SlFsFileInfo_t fsFileInfo;
////	unsigned char *pucFileBuffer=NULL;
//	uint32_t savesize=0;
//	token = 0;
//	retVal = sl_FsOpen((unsigned char *)fileName, FS_MODE_OPEN_READ, &token, &fileHandle);
//	MAP_UtilsDelay(51000);
//	if (0 == retVal) {
//		//
//		// Get the file size using File Info structure
//		//
//		MAP_UtilsDelay(51000);
//		retVal = sl_FsGetInfo((unsigned char *)fileName, token, &fsFileInfo);
//		//
//		// Check for failure
//		//
//		savesize=(&fsFileInfo)->FileLen;//获取文件长度
//		UART_PRINT("readsize=%d\r\n",savesize);
////	    pucFileBuffer = malloc(savesize+1);
//		if (0 == retVal)
//		{
//			//
//			// Read the application into SRAM
//			//
//			MAP_UtilsDelay(51000);
//			retVal = sl_FsRead(fileHandle, 0,
//					(unsigned char *) 0x2001E000, savesize);//(unsigned char *) APP_IMG_SRAM_OFFSET
//			//
//			// Execute the application.
//			//
////			retVal = sl_FsClose(fileHandle,0,0,0);
////			MAP_UtilsDelay(51000);
//			sl_Stop(30);
////			MAP_UtilsDelay(51000);
//			Run(0x2001E000);
//		}
//	}
//}

//*****************************************************************************
//
//! Gets the current time from the selected SNTP server
//!
//! \brief  This function obtains the NTP time from the server.
//!
//! \param  GmtDiffHr is the GMT Time Zone difference in hours
//! \param  GmtDiffMins is the GMT Time Zone difference in minutes
//!
//! \return 0 : success, -ve : failure
//!
//
//*****************************************************************************
long GetSNTPTime(void)
{

/*
                            NTP Packet Header:


       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9  0  1
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |LI | VN  |Mode |    Stratum    |     Poll      |   Precision    |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                          Root  Delay                           |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                       Root  Dispersion                         |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                     Reference Identifier                       |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                                                                |
      |                    Reference Timestamp (64)                    |
      |                                                                |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                                                                |
      |                    Originate Timestamp (64)                    |
      |                                                                |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                                                                |
      |                     Receive Timestamp (64)                     |
      |                                                                |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                                                                |
      |                     Transmit Timestamp (64)                    |
      |                                                                |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                 Key Identifier (optional) (32)                 |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                                                                |
      |                                                                |
      |                 Message Digest (optional) (128)                |
      |                                                                |
      |                                                                |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

*/
    char cDataBuf[48];
    long lRetVal = 0;
    int iAddrSize;
//    unsigned int year=0;
    SlSockAddr_t sAddr;
    SlSockAddrIn_t sLocalAddr;
    //
    // Send a query ? to the NTP server to get the NTP time
    //
    memset(cDataBuf, 0, sizeof(cDataBuf));//初始化发送请求
    cDataBuf[0] = '\x1b';

    sAddr.sa_family = AF_INET;			//设定发送协议
    // the source port
    sAddr.sa_data[0] = 0x00;
    sAddr.sa_data[1] = 0x7B;    // UDP port number for NTP is 123 端口
    sAddr.sa_data[2] = (char)((mytimedata.ulDestinationIP>>24)&0xff);//服务器的ip地址
    sAddr.sa_data[3] = (char)((mytimedata.ulDestinationIP>>16)&0xff);
    sAddr.sa_data[4] = (char)((mytimedata.ulDestinationIP>>8)&0xff);
    sAddr.sa_data[5] = (char)(mytimedata.ulDestinationIP&0xff);

    lRetVal = sl_SendTo(mytimedata.iSockID,
                     cDataBuf,
                     sizeof(cDataBuf), 0,
                     &sAddr, sizeof(sAddr));//向sntp服务器发送请求
    if (lRetVal != sizeof(cDataBuf))		//判断是否发送陈宫
    {
        // could not send SNTP request
        ASSERT_ON_ERROR(SERVER_GET_TIME_FAILED);
    }

    //
    // Wait to receive the NTP time from the server
    //
    sLocalAddr.sin_family = SL_AF_INET;
    sLocalAddr.sin_port = 0;
    sLocalAddr.sin_addr.s_addr = 0;
    if(mytimedata.ulElapsedSec == 0)
    {
        lRetVal = sl_Bind(mytimedata.iSockID,
                (SlSockAddr_t *)&sLocalAddr,
                sizeof(SlSockAddrIn_t));	//绑定本地的端口号和IP地址到socket上
    }

    iAddrSize = sizeof(SlSockAddrIn_t);

    lRetVal = sl_RecvFrom(mytimedata.iSockID,
                       cDataBuf, sizeof(cDataBuf), 0,
                       (SlSockAddr_t *)&sLocalAddr,
                       (SlSocklen_t*)&iAddrSize);//从服务器接收数据 存储到cdatabuf中
    ASSERT_ON_ERROR(lRetVal);

    //
    // Confirm that the MODE is 4 --> server
    //
    if ((cDataBuf[0] & 0x7) != 4)    // expect only server response 服务模式为为4
    {
         ASSERT_ON_ERROR(SERVER_GET_TIME_FAILED);  // MODE is not server, abort
    }
    else
    {
//        unsigned char iIndex;

//        //
//        // Getting the data from the Transmit Timestamp (seconds) field
//        // This is the time at which the reply departed the
//        // server for the client
//        //拆分得到的数据
        mytimedata.ulElapsedSec = cDataBuf[40];
        mytimedata.ulElapsedSec <<= 8;
        mytimedata.ulElapsedSec += cDataBuf[41];
        mytimedata.ulElapsedSec <<= 8;
        mytimedata.ulElapsedSec += cDataBuf[42];
        mytimedata.ulElapsedSec <<= 8;
        mytimedata.ulElapsedSec += cDataBuf[43];
        time_get=mytimedata.ulElapsedSec;
    }
    return SUCCESS;
}
//****************************************************************************
//
//! Task function implementing the gettime functionality using an NTP server
//!
//! \param none
//!
//! This function
//!    1. Initializes the required peripherals
//!    2. Initializes network driver and connects to the default AP
//!    3. Creates a UDP socket, gets the NTP server IP address using DNS
//!    4. Periodically gets the NTP time and displays the time
//!
//! \return None.
//
//****************************************************************************
long LinkNTPServerAndGet(void)
{
	int iSocketDesc;
	long lRetVal = -1;
	uint8_t get_time_flag=0;

	UART_PRINT("GET_TIME: Test Begin\n\r");
	//
	    // Create UDP socket
	    //
	    iSocketDesc = sl_Socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);//创建一个UDP 的数据socket
	    if(iSocketDesc < 0)
	    {
	        ERR_PRINT(iSocketDesc);
	        goto end;
	    }
	    mytimedata.iSockID = iSocketDesc;

	    UART_PRINT("Socket created\n\r");

	    //
	    // Get the NTP server host IP address using the DNS lookup
	    //
	    lRetVal = Network_IF_GetHostIP((char*)g_acSNTPserver, \
	                                    &mytimedata.ulDestinationIP);//通过域名得到服务器的ip地址

	    if( lRetVal >= 0)
	    {

	        struct SlTimeval_t timeVal;
	        timeVal.tv_sec =  SERVER_RESPONSE_TIMEOUT;    // Seconds
	        timeVal.tv_usec = 0;     // Microseconds. 10000 microseconds resolution
	        lRetVal = sl_SetSockOpt(mytimedata.iSockID,SL_SOL_SOCKET,SL_SO_RCVTIMEO,\
	                        (unsigned char*)&timeVal, sizeof(timeVal));// 设置最大的超时时间为10s
	        if(lRetVal < 0)
	        {
	           ERR_PRINT(lRetVal);
	           return lRetVal;
	        }

	        while(!get_time_flag)
	        {
	            //
	            // Get the NTP time and display the time
	            //
	            lRetVal = GetSNTPTime();//获取时间
	            if(lRetVal < 0)
	            {
	                UART_PRINT("Server Get Time failed\n\r");
	                break;
	            }
	            else
	            	get_time_flag=1;
	            //
	            // Wait a while before resuming
	            //
//	            MAP_UtilsDelay(SLEEP_TIME);
	        }
	        get_time_flag=0;
	    }
	    else
	    {
	        UART_PRINT("DNS lookup failed. \n\r");
	    }

	    //
	    // Close the socket
	    //
	    close(iSocketDesc);
	    UART_PRINT("Socket closed\n\r");

	end:
	    UART_PRINT("GET_TIME: Test Complete\n\r");

	    //
	    // Loop here
	    //
	    return lRetVal;

}
//****************************************************************************
//
//! Task function implementing the gettime functionality using an NTP server
//!
//! \param none
//!
//! This function
//!    1. Initializes the required peripherals
//!    2. Initializes network driver and connects to the default AP
//!    3. Creates a UDP socket, gets the NTP server IP address using DNS
//!    4. Periodically gets the NTP time and displays the time
//!
//! \return None.
//
//****************************************************************************
void get_real_time(int all_second,unsigned char ucGmtDiffHr, unsigned char ucGmtDiffMins)
{
	int totole_days=0,year=0,buffer=0;
	uint8_t iIndex=0;
	all_second -= TIME2013; //以2013年为基准开始算

	//
	// in order to correct the timezone
	//
	all_second += (ucGmtDiffHr * SEC_IN_HOUR);	        //加上时区时偏差
	all_second += (ucGmtDiffMins * SEC_IN_MIN);	        //加上时区分偏差
	//
	// day, number of days since beginning of 2013
	//
	totole_days =all_second / SEC_IN_DAY;
	//
	// month
	//
	year = totole_days/ 365 + YEAR2013;				//算出年份
	time_axis.year=year;
	if (year % 4 == 0)											//判断平年闰年
		g_acNumOfDaysPerMonth[1] = 29;
	else
		g_acNumOfDaysPerMonth[1] = 28;
	totole_days %= 365;
	for (iIndex = 0; iIndex < 12; iIndex++)					//index就是月份
			{
		totole_days -= g_acNumOfDaysPerMonth[iIndex];
		if (totole_days< 0)
			break;
	}
	if (iIndex == 12) {
		iIndex = 0;
	}
	time_axis.month=iIndex+1;
	//
	// date
	// restore the day in current month
	//
	totole_days += g_acNumOfDaysPerMonth[iIndex];//加上最后减去的月份的天数用来算日期
	time_axis.day=totole_days+1;

	//
	// time
	//
	totole_days = all_second % SEC_IN_DAY;

	// number of seconds per hour
	buffer = totole_days % SEC_IN_HOUR;

//	time_axis.minute=
	// number of hours
	totole_days /= SEC_IN_HOUR;					//得到小时信息
	time_axis.hour=totole_days;
	/**************************/
//	buffer %= SEC_IN_MIN;
//	time_axis.hour=buffer;
	/*****************************/
	// number of minutes per hour
	totole_days =buffer / SEC_IN_MIN;	//得到分钟信息
	time_axis.minute=totole_days;

	// number of seconds per minute
	buffer %= SEC_IN_MIN;					//得到秒信息
	time_axis.second=buffer;

}
