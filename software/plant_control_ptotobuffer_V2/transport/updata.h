/*
 * updata.h
 *
 *  Created on: 2016年3月2日
 *      Author: Kang
 */

#ifndef PLANT_CONTROL_PTOTOBUFFER_TRANSPORT_UPDATA_H_
#define PLANT_CONTROL_PTOTOBUFFER_TRANSPORT_UPDATA_H_
#include "systerm_init.h"




#define APP_IMG_SRAM_OFFSET     0x20004000				//image运行地址
//#define IMG_OLD     			"uart_demo.bin"			//要下载的文件名
//#define IMG_OLD     			"plant_control.bin"			//要下载的文件名
//#define IMG_OLD     			"blinky.bin"			//要下载的文件名
#define IMG_FACTORY_DEFAULT     "/sys/mcuimg.bin"		//要执行的文件名，不要动
#define IMG_OLD     			"1.txt"					//要下载的文件名

#define TIME2013                1356998400u      /* 113 years + 28 days(leap) */
#define YEAR2013                2013		 		//时间基准
#define SEC_IN_MIN              60
#define SEC_IN_HOUR             3600
#define SEC_IN_DAY              86400

#define SERVER_RESPONSE_TIMEOUT 10
#define GMT_DIFF_TIME_HRS       0					//时区时偏差
#define GMT_DIFF_TIME_MINS      0					//时区分偏差

typedef struct
{
    unsigned long ulDestinationIP;
    int iSockID;
    unsigned long ulElapsedSec;
    short isGeneralVar;
    unsigned long ulGeneralVar;
    unsigned long ulGeneralVar1;
    char acTimeStore[30];
    char *pcCCPtr;
    unsigned short uisCCLen;
}g_sAppData;

typedef struct
{
	uint16_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
}real_time;
extern real_time time_axis;
extern unsigned long time_get;
long ImageLoader_reboot(char *filename ,char *replacename);
long updata_reboot(char *filename ,char *replacename);
void get_real_time(int all_second,unsigned char ucGmtDiffHr, unsigned char ucGmtDiffMins);
long GetSNTPTime(void);
long LinkNTPServerAndGet(void);
void RunMagic(char *fileName);
#endif /* PLANT_CONTROL_PTOTOBUFFER_TRANSPORT_UPDATA_H_ */
