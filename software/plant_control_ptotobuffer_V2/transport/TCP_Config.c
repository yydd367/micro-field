/*
 * TCP_Congfig.c
 *
 *  Created on: 2016年2月28日
 *      Author: Kang
 */
#include "TCP_Config.h"
#include "task.h"
char g_cBsdBuf[BUF_SIZE];
volatile unsigned long  g_ulPacketCount = TCP_PACKET_COUNT;
unsigned long  g_ulDestinationIp = IP_ADDR;
unsigned short g_ulDestinationPort=PORT_NUM;
int             iSockIDServer;
int             iStatusServer;
int             iNewSockIDServer;
//uint8_t flag_socket1=0,flag_socket=0;
uint8_t flag_protect_bling=0;
//****************************************************************************
//
//! \brief Opening a TCP client side socket and sending data
//!
//! This function opens a TCP socket and tries to connect to a Server IP_ADDR
//!    waiting on port PORT_NUM.
//!    If the socket connection is successful then the function will send 1000
//! TCP packets to the server.
//!
//! \param[in]      port number on which the server will be listening on
//!
//! \return    0 on success, -1 on Error.
//
//****************************************************************************
int BsdTcpClient(unsigned short usPort)
{
//    int             iCounter;
    short           sTestBufLen;
    SlSockAddrIn_t  sAddr;
    int             iAddrSize;
    int             iSockID;
    int             iStatus;
    long            lLoopCount = 0;
    Bool reval=0;
    // filling the buffer
//    for (iCounter=0 ; iCounter<BUF_SIZE ; iCounter++)//填充发送缓存
//    {
//        g_cBsdBuf[iCounter] = (char)(iCounter % 10);
//    }
//    memset(g_cBsdBuf,'a',BUF_SIZE-2);
//    g_cBsdBuf[BUF_SIZE-2]='\r';
//    g_cBsdBuf[BUF_SIZE-1]='\n';
//    sTestBufLen  = BUF_SIZE;
    reval=Semaphore_pend(TCP_Client_Pro,100);
    if(reval==FALSE )
    {
        Semaphore_post(TCP_Client_Pro);
        return -1;
    }
    sTestBufLen=steam_len;								//赋值要发送的tcp的长度，也即protobuffer序列化的长度
    //filling the TCP server socket address
    sAddr.sin_family = SL_AF_INET;						//设置ipv4协议
    sAddr.sin_port = sl_Htons((unsigned short)usPort);	//设置端口号
    sAddr.sin_addr.s_addr = sl_Htonl((unsigned int)g_ulDestinationIp);//设置服务器的ip地址

    iAddrSize = sizeof(SlSockAddrIn_t);//得到saddr的长度

    // creating a TCP socket
    iSockID = sl_Socket(SL_AF_INET,SL_SOCK_STREAM, 0);		//开启一个套接字端点，类型为ipv4 传送类型为数据流
    if( iSockID < 0 )										//判断创建是否完成
    {
    	Semaphore_post(TCP_Client_Pro);
        ASSERT_ON_ERROR(SOCKET_CREATE_ERROR);
    }

    // connecting to TCP server
    iStatus = sl_Connect(iSockID, ( SlSockAddr_t *)&sAddr, iAddrSize);//设置为TCP客户端
    if( iStatus < 0 )//判断是否连接
    {
        // error
        sl_Close(iSockID);
        Semaphore_post(TCP_Client_Pro);
        ASSERT_ON_ERROR(CONNECT_ERROR);
    }
//    sl_SetSockOpt(iSockIDServer, SL_SOL_SOCKET, SL_SO_NONBLOCKING,&lNonBlocking, sizeof(lNonBlocking));//设置socket为无阻塞类型
    // sending multiple packets to the TCP server
//    UART_PRINT("sTestBufLen=%d\r\n",sTestBufLen);
//     strcpy(g_cBsdBuf,"719Radio\r\n");
    while (lLoopCount < g_ulPacketCount)							//连续发送5次包
    {
        // sending packet
        iStatus = sl_Send(iSockID, g_cBsdBuf, sTestBufLen, 0 );		//发送包  包的大小为1-1460字节
        if( iStatus < 0 )											//判断是否发送成功
        {
            // error
            sl_Close(iSockID);
            Semaphore_post(TCP_Client_Pro);//关闭socket
            ASSERT_ON_ERROR(SEND_ERROR);
        }
//        osi_Sleep(10);
        lLoopCount++;
    }
   Report("Sent %u packets successfully\n\r",g_ulPacketCount);
 //   osi_Sleep(100);
 //   osi_Sleep(10000);
   iStatus = sl_Close(iSockID);									//释放socket
   ASSERT_ON_ERROR(iStatus);
   //   closing the socket after sending 1000 packets
   Semaphore_post(TCP_Client_Pro);
   return SUCCESS;
}

//****************************************************************************
//
//! \brief Opening a TCP server side socket and receiving data
//!
//! This function opens a TCP socket in Listen mode and waits for an incoming
//!    TCP connection.
//! If a socket connection is established then the function will try to read
//!    1000 TCP packets from the connected client.
//!
//! \param[in] port number on which the server will be listening on
//!
//! \return     0 on success, -1 on error.
//!
//! \note   This function will wait for an incoming connection till
//!                     one is established
//
//****************************************************************************
int BsdTcpServer(unsigned short usPort)
{
    SlSockAddrIn_t  sAddr;
    SlSockAddrIn_t  sLocalAddr;
    int             iAddrSize;
    long            lLoopCount = 0;
    long            lNonBlocking = 1;
    int             iTestBufLen;
    iTestBufLen  = BUF_SIZE;

    //filling the TCP server socket address
    sLocalAddr.sin_family = SL_AF_INET;						//设置ip4协议
    sLocalAddr.sin_port = sl_Htons((unsigned short)usPort);	//设置端口号
    sLocalAddr.sin_addr.s_addr = 0;							//ip地址设为0

    // creating a TCP socket
    iSockIDServer = sl_Socket(SL_AF_INET,SL_SOCK_STREAM, 0);		//建立一个ip4 socket
//    flag_socket=1;
    if( iSockIDServer < 0)										//判断是否建立完成
    {
        ASSERT_ON_ERROR(SOCKET_CREATE_ERROR);
    }
    iAddrSize = sizeof(SlSockAddrIn_t);

    // binding the TCP socket to the TCP server address

    iStatusServer = sl_Bind(iSockIDServer, (SlSockAddr_t *)&sLocalAddr, iAddrSize);//把建好的socket分配给TCP服务器地址

    if( iStatusServer < 0 )										//判断分配是否完成
    {
        // error
        sl_Close(iSockIDServer);                                //关闭socket
//        flag_protect_bling=0;
        ASSERT_ON_ERROR(BIND_ERROR);
    }

    // putting the socket for listening to the incoming TCP connection
    iStatusServer = sl_Listen(iSockIDServer, 0);						//开启监听模式，监听队列设为0
    if( iStatusServer < 0  )										//判断监听是否建立成功
    {
        sl_Close(iSockIDServer);
        flag_protect_bling=0;
        ASSERT_ON_ERROR(LISTEN_ERROR);
    }

//    // setting socket option to make the socket as non blocking
    iStatusServer = sl_SetSockOpt(iSockIDServer, SL_SOL_SOCKET, SL_SO_NONBLOCKING,
                            &lNonBlocking, sizeof(lNonBlocking));//设置socket为无阻塞类型
    if( iStatusServer < 0 )										//判断是否设置成功
    {
        sl_Close(iSockIDServer);
        ASSERT_ON_ERROR(SOCKET_OPT_ERROR);
    }
    iNewSockIDServer = SL_EAGAIN;

    // waiting for an incoming TCP connection
    while( iNewSockIDServer < 0 )									//等待客户机的连接
    {
        // accepts a connection form a TCP client, if there is any
        // otherwise returns SL_EAGAIN
        iNewSockIDServer = sl_Accept(iSockIDServer, ( struct SlSockAddr_t *)&sAddr,
                                (SlSocklen_t*)&iAddrSize);	//建立连接
        if( iNewSockIDServer == SL_EAGAIN )						//返回SL_EAGAIN 证明连接失败，延时10000 重新连接，否则关闭socket
        {
           MAP_UtilsDelay(10000);
//        	osi_Sleep(100);
        	;
        }
        else if( iNewSockIDServer < 0 || Close_soket_flag==1)
        {
            // error

            sl_Close(iNewSockIDServer);
            sl_Close(iSockIDServer);
            ASSERT_ON_ERROR(ACCEPT_ERROR);
        }
        osi_Sleep(10);

    }
    // waits for 1000 packets from the connected TCP client
//    Semaphore_pend(TCP_Buffer_Pro,BIOS_WAIT_FOREVER);
	g_ulPacketCount = 1;
	memset(g_cBsdBuf, '\0', BUF_SIZE);
//	Semaphore_post(TCP_Buffer_Pro);
    while (lLoopCount < g_ulPacketCount)				//接收5个包
    {
        iStatusServer = sl_Recv(iNewSockIDServer, g_cBsdBuf, iTestBufLen, 0);//接收数据
        if( iStatusServer <= 0 || Close_soket_flag==1)								//如果接收的数据长度小于0说明接收失败，关闭socket
        {
          // error
          sl_Close(iNewSockIDServer);
          sl_Close(iSockIDServer);
          ASSERT_ON_ERROR(RECV_ERROR);
        }
        UART_PRINT("packet%d=%s\n\r",lLoopCount,g_cBsdBuf);
        lLoopCount++;
    }

    Report("Recieved %u packets successfully\n\r",g_ulPacketCount);

    // close the connected socket after receiving from connected TCP client

    iStatusServer = sl_Close(iNewSockIDServer);						//关闭与客户端建立的socket
    ASSERT_ON_ERROR(iStatusServer);
    // close the listening socket
    iStatusServer = sl_Close(iSockIDServer);						//关闭绑定的socket
    ASSERT_ON_ERROR(iStatusServer);

    return SUCCESS;
}
