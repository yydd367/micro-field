/*
 * TCP_Config.h
 *
 *  Created on: 2016��2��28��
 *      Author: Kang
 */

#ifndef PLANT_CONTROL_PTOTOBUFFER_TRANSPORT_TCP_CONFIG_H_
#define PLANT_CONTROL_PTOTOBUFFER_TRANSPORT_TCP_CONFIG_H_
#include "systerm_init.h"
#include "network_config.h"

#define IP_ADDR             0xac1a9501 /* 172.21.144.1 */
//#define IP_ADDR             0xc0a84f64 /* 172.21.144.1 */
#define PORT_NUM            5001
#define BUF_SIZE            200
#define TCP_PACKET_COUNT    1

extern char g_cBsdBuf[BUF_SIZE];
extern volatile unsigned long  g_ulPacketCount;
extern unsigned long  g_ulDestinationIp;
extern unsigned short g_ulDestinationPort;
extern uint8_t flag_protect_bling;
int BsdTcpClient(unsigned short usPort);
int BsdTcpServer(unsigned short usPort);
//int close_socket(void);
#endif /* PLANT_CONTROL_PTOTOBUFFER_TRANSPORT_TCP_CONFIG_H_ */
