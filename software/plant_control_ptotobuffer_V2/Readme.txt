2016.3.1 网络部分库搭建完成
主要功能
1、使用TI-RTOS
2、使用simplink中的文件系统
3、sta和ap连接
4、扫描ssid
5、mdns
6、tcp
7、tftp
8、update
主要文件
maim.c                                   存放系统主函数
systerm_init.c				存放系统初始化函数
gpio_if.c				存放gpio功能函数
uart_if.c				存放串口功能函数
pinmux.c				存放引脚复用函数
file_opration.c				存放文件操作函数
mdns_server.c				存放mdns设置函数
network_config.c			存放网络配置函数
tcp_config.c				存放tcp功能函数
tftp_file_transport.c			存放tftp文件传输函数
updata.c				存放自动更新函数

注意：
1、使用时需要包含的静态链接库
libc.a
driverlib.a
tftplib.a
simplink.a
ti_rtos.a
2、编译之前需要先编译下列工程
ti_ritos_config
simplelink
driverlib
oslib
tftplib	
2016.3.2 22:07
1、在updata.c中增加通过sntp 服务器获取时间的功能。 
2016.3.3
1、增加GPIO库，包括中断程序
2、增加串口1、和pwm输出库
3、增加定时器和RTC


2016.3.5
1、给common中各个文件添加注释