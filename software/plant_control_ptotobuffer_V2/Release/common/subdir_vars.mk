################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../common/Interrupt_it.c \
../common/RTC_if.c \
../common/gpio_if.c \
../common/startup_ccs.c \
../common/task.c \
../common/timer_if.c \
../common/uart_if.c 

OBJS += \
./common/Interrupt_it.obj \
./common/RTC_if.obj \
./common/gpio_if.obj \
./common/startup_ccs.obj \
./common/task.obj \
./common/timer_if.obj \
./common/uart_if.obj 

C_DEPS += \
./common/Interrupt_it.pp \
./common/RTC_if.pp \
./common/gpio_if.pp \
./common/startup_ccs.pp \
./common/task.pp \
./common/timer_if.pp \
./common/uart_if.pp 

C_DEPS__QUOTED += \
"common\Interrupt_it.pp" \
"common\RTC_if.pp" \
"common\gpio_if.pp" \
"common\startup_ccs.pp" \
"common\task.pp" \
"common\timer_if.pp" \
"common\uart_if.pp" 

OBJS__QUOTED += \
"common\Interrupt_it.obj" \
"common\RTC_if.obj" \
"common\gpio_if.obj" \
"common\startup_ccs.obj" \
"common\task.obj" \
"common\timer_if.obj" \
"common\uart_if.obj" 

C_SRCS__QUOTED += \
"../common/Interrupt_it.c" \
"../common/RTC_if.c" \
"../common/gpio_if.c" \
"../common/startup_ccs.c" \
"../common/task.c" \
"../common/timer_if.c" \
"../common/uart_if.c" 


