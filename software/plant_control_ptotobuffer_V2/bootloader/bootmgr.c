/*
 * bootmgr.c
 *
 *  Created on: 2017��6��7��
 *      Author: yudi
 */
#include "hw_ints.h"
#include "hw_types.h"
#include "hw_memmap.h"
#include "hw_gprcm.h"
#include "hw_common_reg.h"
#include "rom.h"
#include "rom_map.h"
#include "prcm.h"
#include "simplelink.h"
#include "interrupt.h"
#include "gpio.h"
#include "udma_if.h"
#include "flc.h"
#include "bootmgr.h"
#include "common.h"
#include "systerm_init.h"
//*****************************************************************************
// Local Variables
//*****************************************************************************
static long lFileHandle;
static int  iRetVal;
static SlFsFileInfo_t pFsFileInfo;

static unsigned long ulFactoryImgToken;
static unsigned long ulUserImg1Token;
static unsigned long ulUserImg2Token;
static unsigned long ulBootInfoToken;
static unsigned long ulBootInfoCreateFlag;

//*****************************************************************************
//
//! Checks if the device is secure
//!
//! This function checks if the device is a secure device or not.
//!
//! \return Returns \b true if device is secure, \b false otherwise
//
//*****************************************************************************
static inline tBoolean IsSecureMCU()
{
  unsigned long ulChipId;

  ulChipId =(HWREG(GPRCM_BASE + GPRCM_O_GPRCM_EFUSE_READ_REG2) >> 16) & 0x1F;

  if((ulChipId != DEVICE_IS_CC3101RS) &&(ulChipId != DEVICE_IS_CC3101S))
  {
    //
    // Return non-Secure
    //
    return false;
  }

  //
  // Return secure
  //
  return true;
}
//*****************************************************************************
//
//!\internal
//!
//! Creates default boot info structure
//!
//! \param psBootInfo is pointer to boot info structure to be initialized
//!
//! This function initializes the boot info structure \e psBootInfo based on
//! application image(s) found on sFlash storage. The default boot image is set
//! to one of Factory, User1 or User2 image in the same priority order.
//!
//! \retunr Returns 0 on success, -1 otherwise.
//
//*****************************************************************************
static int CreateDefaultBootInfo(sBootInfo_t *psBootInfo)
{

    //
    // Set the status to no test
    //
    psBootInfo->ulImgStatus = IMG_STATUS_NOTEST;

    //
    // Check if factor default image exists
    //
    iRetVal = sl_FsGetInfo((unsigned char *)IMG_FACTORY_DEFAULT, 0,&pFsFileInfo);
    if(iRetVal == 0)
    {
      psBootInfo->ucActiveImg = IMG_ACT_FACTORY;
      return 0;
    }

    iRetVal = sl_FsGetInfo((unsigned char *)IMG_USER_1, 0,&pFsFileInfo);
    if(iRetVal == 0)
    {
      psBootInfo->ucActiveImg = IMG_ACT_USER1;
      return 0;
    }

    iRetVal = sl_FsGetInfo((unsigned char *)IMG_USER_2, 0,&pFsFileInfo);
    if(iRetVal == 0)
    {
      psBootInfo->ucActiveImg = IMG_ACT_USER2;
      return 0;
    }

    return -1;
}
//*****************************************************************************
//
//! Load the application from sFlash and execute
//!
//! \param ImgName is the name of the application image on sFlash
//! \param ulToken is the token for reading file (relevant on secure devices only)
//!
//! This function loads the specified application from sFlash and executes it.
//!
//! \return None.
//
//*****************************************************************************
void LoadAndExecute(unsigned char *ImgName, unsigned long ulToken)
{

  //
  // Open the file for reading
  //
	UART_PRINT("load begin");
  iRetVal = sl_FsOpen(ImgName, FS_MODE_OPEN_READ,
                        &ulToken, &lFileHandle);
  //
  // Check if successfully opened
  //
  if( 0 == iRetVal )
  {
    //
    // Get the file size using File Info structure
    //
    iRetVal = sl_FsGetInfo(ImgName, ulToken,&pFsFileInfo);

    //
    // Check for failure
    //
    if( 0 == iRetVal )
    {

      //
      // Read the application into SRAM
      //
      iRetVal = sl_FsRead(lFileHandle,0, (unsigned char *)APP_IMG_SRAM_OFFSET,
                 pFsFileInfo.FileLen );
      if( iRetVal<0)
      {
    	  UART_PRINT("read fail");
      }
      //
      // Stop the network services
      //
      sl_Stop(30);

      //
      // Execute the application.
      //
      Run(APP_IMG_SRAM_OFFSET);
    }
  }
  else
  {
	  UART_PRINT("open fail");
  }
}
//*****************************************************************************
//
//! Writes into the boot info file.
//!
//! \param psBootInfo is pointer to the boot info structure.
//!
//! This function writes the boot info into the boot info file in the sFlash
//!
//! \return Return 0 on success, -1 otherwise.
//
//*****************************************************************************
static long BootInfoWrite(sBootInfo_t *psBootInfo)
{
  long lFileHandle;
  unsigned long ulToken;

  //
  // Open the boot info file for write
  //
  if( 0 == sl_FsOpen((unsigned char *)IMG_BOOT_INFO, FS_MODE_OPEN_WRITE,
                      &ulToken, &lFileHandle) )
  {
    //
    // Write the boot info
    //
    if( 0 < sl_FsWrite(lFileHandle,0, (unsigned char *)psBootInfo,
                         sizeof(sBootInfo_t)) )
    {

    //
    // Close the file
    //
    sl_FsClose(lFileHandle, 0, 0, 0);

    //
    // Return success
    //
    return 0;
   }

  }

  //
  // Return failure
  //
  return -1;
}
#ifndef ccs
void Run(unsigned long ulBaseLoc)
{

  //
  // Set the SP
  //
  __asm("	ldr    sp,[r0]\n"
	"	add    r0,r0,#4");

  //
  // Jump to entry code
  //
  __asm("	ldr    r1,[r0]\n"
        "	bx     r1");
}
#else
__asm("    .sect \".text:Run\"\n"
      "    .clink\n"
      "    .thumbfunc Run\n"
      "    .thumb\n"
      "Run:\n"
      "    ldr    sp,[r0]\n"
      "    add    r0,r0,#4\n"
      "    ldr    r1,[r0]\n"
      "    bx     r1");
#endif

//*****************************************************************************
//
//! Load the proper image based on information from boot info and executes it.
//!
//! \param psBootInfo is pointer to the boot info structure.
//!
//! This function loads the proper image based on information from boot info
//! and executes it. \e psBootInfo should be properly initialized.
//!
//! \return None.
//
//*****************************************************************************
static void ImageLoader(sBootInfo_t *psBootInfo)
{
  unsigned char ucActiveImg;
  unsigned long ulImgStatus;

  //
  // Get the active image and image status
  //
  ucActiveImg = psBootInfo->ucActiveImg;
  ulImgStatus = psBootInfo->ulImgStatus;

  //
  // Boot image based on image status and active image configuration
  //
  if( IMG_STATUS_NOTEST == ulImgStatus )
  {

    //
    // Since no test image boot the acive image.
    //
    switch(ucActiveImg)
    {

    case IMG_ACT_USER1:
      LoadAndExecute((unsigned char *)IMG_USER_1,ulUserImg1Token);
      break;

    case IMG_ACT_USER2:
      LoadAndExecute((unsigned char *)IMG_USER_2,ulUserImg2Token);
      break;

    default:
      LoadAndExecute((unsigned char *)IMG_FACTORY_DEFAULT,ulFactoryImgToken);
      break;
    }
  }
  else if( IMG_STATUS_TESTREADY == ulImgStatus )
  {
    //
    // Some image waiting to be tested; Change the status to testing
    // in boot info file
    //
    psBootInfo->ulImgStatus = IMG_STATUS_TESTING;
    BootInfoWrite(psBootInfo);

    //
    // Boot the test image ( the non-active image )
    //
    switch(ucActiveImg)
    {

    case IMG_ACT_USER1:
      LoadAndExecute((unsigned char *)IMG_USER_2,ulUserImg2Token);
      break;

    default:
      LoadAndExecute((unsigned char *)IMG_USER_1,ulUserImg1Token);
    }
  }
  else if( IMG_STATUS_TESTING == ulImgStatus )
  {

    //
    // Something went wrong while in testing.
    // Change the status to no test
    //
    psBootInfo->ulImgStatus = IMG_STATUS_NOTEST;
    BootInfoWrite(psBootInfo);

    //
    // Boot the active image.
    //
    switch(ucActiveImg)
    {

    case IMG_ACT_USER1:
      LoadAndExecute((unsigned char *)IMG_USER_1,ulUserImg1Token);
      break;

    case IMG_ACT_USER2:
      LoadAndExecute((unsigned char *)IMG_USER_2,ulUserImg2Token);
      break;

    default:
      LoadAndExecute((unsigned char *)IMG_FACTORY_DEFAULT,ulFactoryImgToken);
      break;
    }
  }

  //
  // Boot info might be corrupted go into infinite loop
  //
  while(1)
  {

  }

}
int bootloader()
{
	  sBootInfo_t sBootInfo;

	  sBootInfo.ucActiveImg = IMG_ACT_FACTORY;
	  sBootInfo.ulImgStatus = IMG_STATUS_NOTEST;

	  //
	  // Initialize boot info file create flag
	  //
	  ulBootInfoCreateFlag  = _FS_FILE_OPEN_FLAG_COMMIT|_FS_FILE_PUBLIC_WRITE;

	  //
	  // Check if its a secure MCU
	  //
	  if ( IsSecureMCU() )
	  {
	    ulFactoryImgToken     = FACTORY_IMG_TOKEN;
	    ulUserImg1Token       = USER_IMG_1_TOKEN;
	    ulUserImg2Token       = USER_IMG_2_TOKEN;
	    ulBootInfoToken       = USER_BOOT_INFO_TOKEN;
	    ulBootInfoCreateFlag  = _FS_FILE_OPEN_FLAG_COMMIT|_FS_FILE_OPEN_FLAG_SECURE|
	                            _FS_FILE_OPEN_FLAG_NO_SIGNATURE_TEST|
	                            _FS_FILE_PUBLIC_WRITE|_FS_FILE_OPEN_FLAG_VENDOR;
	  }


	  //
	  // Start slhost to get NVMEM service
	  //
	  sl_Start(NULL, NULL, NULL);

	  //
	  // Open Boot info file for reading
	  //
	  iRetVal = sl_FsOpen((unsigned char *)IMG_BOOT_INFO,
	                        FS_MODE_OPEN_READ,
	                        &ulBootInfoToken,
	                        &lFileHandle);

	  //
	  // If successful, load the boot info
	  // else create a new file with default boot info.
	  //
	  if( 0 == iRetVal )
	  {
	    iRetVal = sl_FsRead(lFileHandle,0,
	                         (unsigned char *)&sBootInfo,
	                         sizeof(sBootInfo_t));
	  }
	  else
	  {

	    //
	    // Create a new boot info file
	    //
	    iRetVal = sl_FsOpen((unsigned char *)IMG_BOOT_INFO,
	                        FS_MODE_OPEN_CREATE(2*sizeof(sBootInfo_t),
	                                            ulBootInfoCreateFlag),
	                                            &ulBootInfoToken,
	                                            &lFileHandle);

	    //
	    // Create a default boot info
	    //
	    iRetVal = CreateDefaultBootInfo(&sBootInfo);

	    if(iRetVal != 0)
	    {
	      //
	      // Can't boot no bootable image found
	      //
	      while(1)
	      {

	      }
	    }

	    //
	    // Write the default boot info.
	    //
	    iRetVal = sl_FsWrite(lFileHandle,0,
	                         (unsigned char *)&sBootInfo,
	                         sizeof(sBootInfo_t));
	  }

	  //
	  // Close boot info function
	  //
	  sl_FsClose(lFileHandle, 0, 0, 0);
	  //
	  // Load and execute the image base on boot info.
	  //
	  ImageLoader(&sBootInfo);

	  //
	  // Infinite loop
	  //
	  while(1)
	  {

	  }
}
