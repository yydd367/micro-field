//*****************************************************************************
// pinmux.c
//
// configure the device pins for different peripheral signals
//
// Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

// This file was automatically generated on 7/21/2014 at 3:06:20 PM
// by TI PinMux version 3.0.334
//
//*****************************************************************************

#include "pinmux.h"
#include "hw_types.h"
#include "hw_memmap.h"
#include "hw_gpio.h"
#include "pin.h"
#include "rom.h"
#include "rom_map.h"
#include "gpio.h"
#include "prcm.h"

//*****************************************************************************
void PinMuxConfig(void) {
	//
	// Enable Peripheral Clocks
	//
	MAP_PRCMPeripheralClkEnable(PRCM_GPIOA0, PRCM_RUN_MODE_CLK);
	MAP_PRCMPeripheralClkEnable(PRCM_GPIOA2, PRCM_RUN_MODE_CLK);
	MAP_PRCMPeripheralClkEnable(PRCM_GPIOA1, PRCM_RUN_MODE_CLK);
	MAP_PRCMPeripheralClkEnable(PRCM_UARTA0, PRCM_RUN_MODE_CLK);
	MAP_PRCMPeripheralClkEnable(PRCM_UARTA1, PRCM_RUN_MODE_CLK);
	MAP_PRCMPeripheralClkEnable(PRCM_TIMERA2, PRCM_RUN_MODE_CLK);
	MAP_PRCMPeripheralClkEnable(PRCM_TIMERA3, PRCM_RUN_MODE_CLK);
	MAP_PRCMPeripheralClkEnable(PRCM_GPIOA3, PRCM_RUN_MODE_CLK);
	/*
	 *
	 *
	 * ������������
	 *
	 * */
	//
	// Configure PIN_55 for UART0 UART0_TX			GPIO_01
	//
	MAP_PinTypeUART(PIN_55, PIN_MODE_3);

	//
	// Configure PIN_57 for UART0 UART0_RX			GPIO_02
	//
	MAP_PinTypeUART(PIN_57, PIN_MODE_3);

	//
	// Configure PIN_07 for UART1 UART1_TX			GPIO_16
	//
	MAP_PinTypeUART(PIN_07, PIN_MODE_5);

	//
	// Configure PIN_08 for UART1 UART1_RX			GPIO_17
	//
	MAP_PinTypeUART(PIN_08, PIN_MODE_5);

	/*
	 *
	 * PWM��������
	 *
	 * */
	//
	// Configure PIN_64 for TIMERPWM5 GT_PWM05			GPIO_09		FAN
	//
	MAP_PinTypeTimer(PIN_64, PIN_MODE_3);

//	//
//         	// Configure PIN_01 for TIMERPWM6 GT_PWM06			GPIO_10  	LAMP1
//	//
//	MAP_PinTypeTimer(PIN_01, PIN_MODE_0);
//
//	//
//         	// Configure PIN_02 for TIMERPWM7 GT_PWM07			GPIO_11		LAMP2
//	//
//	MAP_PinTypeTimer(PIN_02, PIN_MODE_0);

	/*
	 *
	 * GPIOOUT��������
	 *
	 * */
	//
	// Configure PIN_60 for GPIO Output					GPIO_05
	//
	MAP_PinTypeGPIO(PIN_60, PIN_MODE_0, false);			//�̵���IO����
	MAP_GPIODirModeSet(GPIOA0_BASE, 0x20, GPIO_DIR_MODE_OUT);

	//
	// Configure PIN_61 for GPIO Output					GPIO_06
	//
	MAP_PinTypeGPIO(PIN_61, PIN_MODE_0, false);			//���ȷ�������IO
	MAP_GPIODirModeSet(GPIOA0_BASE, 0x40, GPIO_DIR_MODE_OUT);

	//
	// Configure PIN_62 for GPIO Output					GPIO_07
	//
	MAP_PinTypeGPIO(PIN_62, PIN_MODE_0, false);			//���Ե�1IO
	MAP_GPIODirModeSet(GPIOA0_BASE, 0x80, GPIO_DIR_MODE_OUT);

	//
	// Configure PIN_63 for GPIO Output					GPIO_08
	//
	MAP_PinTypeGPIO(PIN_63, PIN_MODE_0, false);			//���Ե�2IO
	MAP_GPIODirModeSet(GPIOA1_BASE, 0x1, GPIO_DIR_MODE_OUT);

	//
	// Configure PIN_03 for GPIO Output					GPIO_12
	//
	MAP_PinTypeGPIO(PIN_03, PIN_MODE_0, false);			//ģ��pwmIO
	MAP_GPIODirModeSet(GPIOA1_BASE, 0x10, GPIO_DIR_MODE_OUT);

	//
	// Configure PIN_02 for GPIO Output					GPIO_11
	//
	MAP_PinTypeGPIO(PIN_02, PIN_MODE_0, false);			//ģ��pwmIO
	MAP_GPIODirModeSet(GPIOA1_BASE, 0x08, GPIO_DIR_MODE_OUT);

	//
	// Configure PIN_01 for GPIO Output					GPIO_10
	//
	MAP_PinTypeGPIO(PIN_01, PIN_MODE_0, false);			//ģ��pwmIO
	MAP_GPIODirModeSet(GPIOA1_BASE, 0x04, GPIO_DIR_MODE_OUT);

	/*
	 *
	 * GPIOIN��������
	 *
	 * */
	//
	// Configure PIN_59 for GPIO Input					GPIO_04
	//
	MAP_PinTypeGPIO(PIN_59, PIN_MODE_0, false);			//����������IO����
	MAP_PinConfigSet(PIN_59, PIN_STRENGTH_2MA, PIN_TYPE_STD_PD);
	MAP_GPIODirModeSet(GPIOA0_BASE, 0x10, GPIO_DIR_MODE_IN);

	//
	// Configure PIN_04 for GPIO Input					GPIP_13
	//
	MAP_PinTypeGPIO(PIN_04, PIN_MODE_0, false);			//�û�����IO����
	MAP_GPIODirModeSet(GPIOA1_BASE, 0x20, GPIO_DIR_MODE_IN);

	//
	// Configure PIN_05 for GPIO Output OD				GPIP_14
	//
	MAP_PinTypeGPIO(PIN_05, PIN_MODE_0, false);
	MAP_GPIODirModeSet(GPIOA1_BASE, 0x40, GPIO_DIR_MODE_OUT);

    //
    // Configure PIN_53 for GPIO Output
    //
    MAP_PinTypeGPIO(PIN_53, PIN_MODE_0, false);
    MAP_GPIODirModeSet(GPIOA3_BASE, 0x40, GPIO_DIR_MODE_OUT);

    //
    // Configure PIN_45 for GPIO Output
    //
    MAP_PinTypeGPIO(PIN_45, PIN_MODE_0, false);
    MAP_GPIODirModeSet(GPIOA3_BASE, 0x80, GPIO_DIR_MODE_OUT);
}
