//*****************************************************************************
//
//  Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//  Redistributions of source code must retain the above copyright 
//  notice, this list of conditions and the following disclaimer.
//
//  Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the 
//  documentation and/or other materials provided with the   
//  distribution.
//
//  Neither the name of Texas Instruments Incorporated nor the names of
//  its contributors may be used to endorse or promote products derived
//  from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************


//*****************************************************************************
//
// Application Name     -   Getting started with WLAN STATION
// Application Overview -   This is a sample application demonstrating how to
//                          start CC3200 in WLAN-Station mode and connect to a
//                          Wi-Fi access-point. The application connects to an
//                          access-point and ping the gateway. It also checks
//                          for an internet connectivity by pinging "www.ti.com"
// Application Details  -
// http://processors.wiki.ti.com/index.php/CC32xx_Getting_Started_with_WLAN_Station
// or
// doc\examples\CC32xx_Getting_Started_with_WLAN_Station.pdf
//
//*****************************************************************************


//****************************************************************************
//
//! \addtogroup getting_started_sta
//! @{
//
//****************************************************************************

// Standard includes
#include "systerm_init.h"
#include "task.h"
#include "user.h"
//#include "bootmgr.h"
//*****************************************************************************
//                            MAIN FUNCTION
//*****************************************************************************
//static void DisplayInformation()
//{
//	UART_PRINT("\n\n\r");
//	UART_PRINT("\t\t *************************************************\n\r");
//	UART_PRINT("\t\t	     ΢�ﲩ����ֲ��ϵͳ                \n\r");
//	UART_PRINT("\r\ndescription:");
//	UART_PRINT("\r\nVersion:1.0.0\n\r");
//	UART_PRINT("\r\nauthor:719 team\n\r");
//	UART_PRINT("\t\t *************************************************\n\r");
//	UART_PRINT("\n\n\n\r");
//}
void main(){
    long lRetVal = -1;
    //
    // systerm Initialization
    //
    //DisplayInformation();
	lRetVal=Systerminit();
	if (lRetVal <= 0)
	{
		ERR_PRINT(lRetVal);
//		LOOP_FOREVER();
	}

    //
    // Display Application Banner
    //
//    DisplayBanner(APP_NAME);
     task_init();
////	//send_commmand(read_id,0x02);
//////     Start the task scheduler
////
   osi_start();
	//UART_PRINT("\t\t	     ΢�ﲩ����ֲ��ϵͳ                \n\r");


	while(1)
	{
		;
//		GPIO_IF_Set(5, 1);
//		MAP_UtilsDelay(5100000);
//		GPIO_IF_Set(5, 0);
//		MAP_UtilsDelay(5100000);
//		GPIO_IF_Set(12, 1);
//		GPIO_IF_Set(11, 1);
//		GPIO_IF_Set(10, 1);
	}
  }

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************
