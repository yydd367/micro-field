/*
 * MDNS_server.h
 *
 *  Created on: 2016��3��1��
 *      Author: Kang
 */

#ifndef PLANT_CONTROL_PTOTOBUFFER_NETWORK_CONFIG_MDNS_SERVER_H_
#define PLANT_CONTROL_PTOTOBUFFER_NETWORK_CONFIG_MDNS_SERVER_H_
#include "systerm_init.h"
typedef struct
{
    _u32 addr;
    _u32 port;
    _u16 TextLen;
    char Text[10];
}MDNS_getdata;
//void MDNS_server(_u16 port,_u32 ttl);
//void MDNS_Listen(_u32 Addr,_u32* Port,_u16 *TextLen,char * Text );
void MDNS_broadcast(MDNS_getdata uldata ,_u16 TextLen);
#endif /* PLANT_CONTROL_PTOTOBUFFER_NETWORK_CONFIG_MDNS_SERVER_H_ */
