/*
 * STA_config.c
 *
 *  Created on: 2016年2月27日
 *      Author: Kang
 */
#include "network_config.h"
#include "task.h"
#include "user.h"
#include "stdio.h"
#include "bootmgr.h"
#include "systerm_init.h"
//static char mem_test[UPDATA_SIZE_MAX]={0};
/* XDCtools Header files */


//*****************************************************************************
//                 GLOBAL VARIABLES -- Start
//*****************************************************************************
unsigned long  g_ulStatus = 0;//SimpleLink Status
unsigned long  g_ulPingPacketsRecv = 0; //Number of Ping Packets received
unsigned long  g_ulGatewayIP = 0; //Network Gateway IP address
unsigned char  g_ucConnectionSSID[SSID_LEN_MAX+1]; //Connection SSID
unsigned char  g_ucConnectionBSSID[BSSID_LEN_MAX]; //Connection BSSID
Sl_WlanNetworkEntry_t g_NetEntries[SCAN_TABLE_SIZE];//存放扫描的SSD信息
uint8_t RSSI_Strength[SCAN_TABLE_SIZE]={0};
unsigned char macAddressVal[SL_MAC_ADDR_LEN];
unsigned char macAddressLen = SL_MAC_ADDR_LEN;
unsigned long  g_ulStaIp = 0;
_u8 flag_disconnect=0,flag_mode_change=0;
long Current_Mode=0;
char    pcSsidName[33]={0};
char ssidname_get[33]={0};							//存储上位机下发的连接文件名
char key_get[33]={0};								//存储上位机下发的密码
char security_key_get=0;							//存储上位机下发的安全类型
char Client_statue=0;
//*****************************************************************************
//                 GLOBAL VARIABLES -- End
//*****************************************************************************

//*****************************************************************************
// SimpleLink Asynchronous Event Handlers -- Start
//*****************************************************************************


//*****************************************************************************
//
//! \brief The Function Handles WLAN Events    异步事件校验同步事件
//!
//! \param[in]  pWlanEvent - Pointer to WLAN Event Info
//!
//! \return None
//!
//*****************************************************************************

void SimpleLinkWlanEventHandler(SlWlanEvent_t *pWlanEvent)
{
    switch(pWlanEvent->Event)
    {
    /*
     * 这个连接状态下，EventData返回的数据是STAandP2PModeWlanConnected包含
     * ssid_name
     * ssid_len
     * bssid
     * go_peer_device_name
     * go_peer_device_name_len
     * */
        case SL_WLAN_CONNECT_EVENT:										//STA或者P2P客户连接指示事件
        {
            SET_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION);			//设置连接状态位
//            task_restore();												//网络连接到AP模式的时候，重新开启TCP服务器，和DNS搜寻，这个只有在有网的情况下才能进行，无网进行会出现错误，整个系统崩溃
            //
            // Information about the connected AP (like name, MAC etc) will be
            // available in 'slWlanConnectAsyncResponse_t'-Applications
            // can use it if required
            //

            // Copy new connection SSID and BSSID to global parameters
            memcpy(g_ucConnectionSSID,pWlanEvent->EventData.
                   STAandP2PModeWlanConnected.ssid_name,
                   pWlanEvent->EventData.STAandP2PModeWlanConnected.ssid_len);//把ssid_name拷贝到g_ucConnectionSSID中
            memcpy(g_ucConnectionBSSID,
                   pWlanEvent->EventData.STAandP2PModeWlanConnected.bssid,
                   SL_BSSID_LENGTH);										  //把Bssid_name拷贝到g_ucConnectionBSSID中

            UART_PRINT("[WLAN EVENT] STA Connected to the AP: %s ,"
                        "BSSID: %x:%x:%x:%x:%x:%x\n\r",
                      g_ucConnectionSSID,g_ucConnectionBSSID[0],
                      g_ucConnectionBSSID[1],g_ucConnectionBSSID[2],
                      g_ucConnectionBSSID[3],g_ucConnectionBSSID[4],
                      g_ucConnectionBSSID[5]);								 //串口打印ssid 和bssid信息

        }
        break;
        /*
		 * 这个连接状态下，EventData返回的数据是STAandP2PModeDisconnected包含
		 * ssid_name
		 * ssid_len
		 * reason_code
		 * */
        case SL_WLAN_DISCONNECT_EVENT:									//STA或者P2P客户未连接指示事件
        {
            slWlanConnectAsyncResponse_t*  pEventData = NULL;

            CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION);			//清除连接标志位
            CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_AQUIRED);			//清除IP请求标志位

//            task_delete();												//断开连接的时候应该删除TCP服务器监听任务，和DNS搜寻任务
            pEventData = &pWlanEvent->EventData.STAandP2PModeDisconnected;

            // If the user has initiated 'Disconnect' request,
            //'reason_code' is SL_USER_INITIATED_DISCONNECTION
            if(SL_USER_INITIATED_DISCONNECTION == pEventData->reason_code)	//用户调用disconnect函数返回的的代码
            {
                UART_PRINT("[WLAN EVENT]Device disconnected from the AP: %s,"
                "BSSID: %x:%x:%x:%x:%x:%x on application's request \n\r",
                           g_ucConnectionSSID,g_ucConnectionBSSID[0],
                           g_ucConnectionBSSID[1],g_ucConnectionBSSID[2],
                           g_ucConnectionBSSID[3],g_ucConnectionBSSID[4],
                           g_ucConnectionBSSID[5]);							//打印调试信息，和BSSID代码
            }
            else
            {
                UART_PRINT("[WLAN ERROR]Device disconnected from the AP AP: %s,"
                "BSSID: %x:%x:%x:%x:%x:%x on an ERROR..!! \n\r",
                           g_ucConnectionSSID,g_ucConnectionBSSID[0],
                           g_ucConnectionBSSID[1],g_ucConnectionBSSID[2],
                           g_ucConnectionBSSID[3],g_ucConnectionBSSID[4],
                           g_ucConnectionBSSID[5]);						//打印调试信息，和BSSID代码
            }
            memset(g_ucConnectionSSID,0,sizeof(g_ucConnectionSSID));	//SSID清零
            memset(g_ucConnectionBSSID,0,sizeof(g_ucConnectionBSSID));	//BSSID清零
//            SL_ERROR_CON_MGMT_STATUS_DISCONNECT_DURING_CONNECT
        }
        break;
        /*
		 * 这个连接状态下，EventData返回的数据是APModeStaConnected包含
		 * go_peer_device_name
		 * mac
		 * go_peer_device_name_len
		 * wps_dev_password_id
		 * own_ssid: relevant for event sta-connected only
		 * own_ssid_len: relevant for event sta-connected only
		 * */
        case SL_WLAN_STA_CONNECTED_EVENT:								// AP/P2P(Go)连接到STA/P2P(Client)
        {
		// when device is in AP mode and any client connects to device cc3xxx
        	SET_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION);			//设置相应的标志位
		//
		// Information about the connected client (like SSID, MAC etc) will be
		// available in 'slPeerInfoAsyncResponse_t' - Applications
		// can use it if required
		//
		// slPeerInfoAsyncResponse_t *pEventData = NULL;
		// pEventData = &pSlWlanEvent->EventData.APModeStaConnected;
		//

        }
		break;
        /*
		 * 这个连接状态下，EventData返回的数据是APModeStaConnected包含
		 * go_peer_device_name
		 * mac
		 * go_peer_device_name_len
		 * wps_dev_password_id
		 * own_ssid: relevant for event sta-connected only
		 * own_ssid_len: relevant for event sta-connected only
		 * */
        case SL_WLAN_STA_DISCONNECTED_EVENT: 						// AP/P2P(Go)连接到STA/P2P(Client)
		{
		// when client disconnects from device (AP)
			CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION);		//清除连接标志位
			CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_LEASED);		//清除未连接标志位
		//
		// Information about the connected client (like SSID, MAC etc) will
		// be available in 'slPeerInfoAsyncResponse_t' - Applications
		// can use it if required
		//
		// slPeerInfoAsyncResponse_t *pEventData = NULL;
		// pEventData = &pSlWlanEvent->EventData.APModestaDisconnected;
		//
		}
		break;
		/*
		 *还有一些中断事件见下、具体的说明见API手册
		 *SL_WLAN_SMART_CONFIG_COMPLETE_EVENT
		 *SL_WLAN_SMART_CONFIG_STOP_EVENT
		 *SL_WLAN_P2P_DEV_FOUND_EVENT
		 *SL_WLAN_P2P_NEG_REQ_RECEIVED_EVENT
		 *SL_WLAN_CONNECTION_FAILED_EVENT ,
		 * */
        default:
        {
            UART_PRINT("[WLAN EVENT] Unexpected event [0x%x]\n\r",
                       pWlanEvent->Event);
        }
        break;
    }
}

//*****************************************************************************
//
//! \brief This function handles network events such as IP acquisition, IP
//!           leased, IP released etc.
//!
//! \param[in]  pNetAppEvent - Pointer to NetApp Event Info
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkNetAppEventHandler(SlNetAppEvent_t *pNetAppEvent)					//NETAPP 非同步的事件 处理程序
{
//    long lRetVal = -1;
//	char *FileRead = "1.txt";
//
//
//    //SlSecParams_t secParams;
//    unsigned char *pucFileBuffer = NULL;  // Data read or to be written
//    unsigned long uiFileSize;

  //  char *FileRead = "readFromServer.txt";  // File to be read using TFTP. Change string to filename
    //char *FileWrite = "writeToServer.txt"; // File to be written using TFTP. Change string to filename.

//    long pFileHandle;			// Pointer to file handle
//    SlFsFileInfo_t pFsFileInfo;
//    //long lRetVal = -1;
//    unsigned short uiTftpErrCode;


    switch(pNetAppEvent->Event)
    {
		/*
		 * 这个连接状态下，EventData返回的数据是ipAcquiredV4包含
		 * ip
		 * gateway
		 * dns
		 * */
        case SL_NETAPP_IPV4_IPACQUIRED_EVENT:									//获取IPV4事件
        {
            SlIpV4AcquiredAsync_t *pEventData = NULL;

            SET_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_AQUIRED);					//设置ip请求标志位

            //Ip Acquired Event Data
            pEventData = &pNetAppEvent->EventData.ipAcquiredV4;

            //Gateway IP address
            g_ulGatewayIP = pEventData->gateway;								//网关地址赋值

            UART_PRINT("[NETAPP EVENT] IP Acquired: IP=%d.%d.%d.%d , "
            "Gateway=%d.%d.%d.%d\n\r",
            SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,3),
            SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,2),
            SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,1),
            SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,0),
            SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.gateway,3),
            SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.gateway,2),
            SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.gateway,1),
            SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.gateway,0));		//串口打印ip地址和网关地址
        }
        break;
        case SL_NETAPP_IPV6_IPACQUIRED_EVENT:
        {
            SET_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_AQUIRED);
        }
		break;
		/*
		 * 这个连接状态下，EventData返回的数据是ipLeased 包含
		 * ip_address
		 * lease_time
		 * mac
		 * */
        case SL_NETAPP_IP_LEASED_EVENT:										//AP和P2P go dhcp ip分配事件
        {
			SET_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_LEASED);				//设置ip请求标志位

			g_ulStaIp = (pNetAppEvent)->EventData.ipLeased.ip_address;		//DHCP分配的IP地址

			UART_PRINT("[NETAPP EVENT] IP Leased to Client: IP=%d.%d.%d.%d , ",
					SL_IPV4_BYTE(g_ulStaIp, 3), SL_IPV4_BYTE(g_ulStaIp, 2),
					SL_IPV4_BYTE(g_ulStaIp, 1), SL_IPV4_BYTE(g_ulStaIp, 0));//打印分配给client的地址
			UART_PRINT("Client connected \r\n");
			Client_statue=1;
//    	    uiFileSize=GEI_FILE_SIZE_MAX;
//			lRetVal=tftp_getfile(FileRead,GEI_FILE_SIZE_MAX);
//			if(lRetVal < 0)
//			{
//				ERR_PRINT(lRetVal);
//			}
//			lRetVal=ReadFile(FileRead,0,0);
//			if (lRetVal < 0)
//			{
//				ERR_PRINT(lRetVal);
//			}
//			lRetVal=tftp_sentfile(FileRead,"2.txt");
//			if(lRetVal < 0)
//			{
//				ERR_PRINT(lRetVal);
//				ImageLoader_reboot("2.txt",IMG_FACTORY_DEFAULT);//"blinky.bin"IMG_FACTORY_DEFAULT
//			}
////			        pucFileBuffer = malloc(uiFileSize);
////			        if(NULL == pucFileBuffer)
////			        {
////			            UART_PRINT("Can't Allocate Resources\r\n");
////			            LOOP_FOREVER();
////			        }
//
//			       // memset(pucFileBuffer,'\0',uiFileSize);
//
//			        lRetVal = sl_TftpRecv(TFTP_IP, FileRead, (char *)pucFileBuffer,\
//			                                &uiFileSize, &uiTftpErrCode );
//			        if(lRetVal < 0)
//			        {
//			            free(pucFileBuffer);
//			            ERR_PRINT(lRetVal);
//			            LOOP_FOREVER();
//			        }
//
//			        lRetVal = sl_FsGetInfo((unsigned char *)FileRead, NULL, &pFsFileInfo);
//
//			        if(lRetVal < 0 )
//			            lRetVal = sl_FsOpen((unsigned char *)FileRead,\
//			                    FS_MODE_OPEN_CREATE(FILE_SIZE_MAX,_FS_FILE_OPEN_FLAG_COMMIT|\
//			                      _FS_FILE_PUBLIC_WRITE), NULL,&pFileHandle);
//			        else
//			            lRetVal = sl_FsOpen((unsigned char *)FileRead,FS_MODE_OPEN_WRITE, \
//			                                NULL,&pFileHandle);
//
//			        if(lRetVal < 0)
//			        {
//			            free(pucFileBuffer);
//			            ERR_PRINT(lRetVal);
//			            LOOP_FOREVER();
//			        }
//
//			        lRetVal = sl_FsWrite(pFileHandle,0, pucFileBuffer, uiFileSize);
//
//			        if(lRetVal < 0)
//			        {
//			            free(pucFileBuffer);
//			            lRetVal = sl_FsClose(pFileHandle,0,0,0);
//			            ERR_PRINT(lRetVal);
//			            LOOP_FOREVER();
//			        }
//
//			        UART_PRINT("TFTP Read Successful \r\n");
//
//			        lRetVal = sl_FsClose(pFileHandle,0,0,0);
        }
		break;
		/*
		 * 这个连接状态下，EventData返回的数据是ipLeased 包含
		 * ip_address
		 * lease_time
		 * mac
		 * */
        case SL_NETAPP_IP_RELEASED_EVENT:							//AP和P2P go dhcp ip释放事件
        {
			CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_LEASED);

			UART_PRINT("[NETAPP EVENT] IP Released for Client: IP=%d.%d.%d.%d , ",
					SL_IPV4_BYTE(g_ulStaIp, 3), SL_IPV4_BYTE(g_ulStaIp, 2),
				SL_IPV4_BYTE(g_ulStaIp, 1), SL_IPV4_BYTE(g_ulStaIp, 0));

        }
		break;
        default:
        {
            UART_PRINT("[NETAPP EVENT] Unexpected event [0x%x] \n\r",
                       pNetAppEvent->Event);
        }
        break;
    }
}


//*****************************************************************************
//
//! \brief This function handles HTTP server events
//!
//! \param[in]  pServerEvent - Contains the relevant event information
//! \param[in]    pServerResponse - Should be filled by the user with the
//!                                      relevant response information
//!
//! \return None
//!
//****************************************************************************
void SimpleLinkHttpServerCallback(SlHttpServerEvent_t *pHttpEvent,
                                  SlHttpServerResponse_t *pHttpResponse)
{
    // Unused in this application
}

//*****************************************************************************
//
//! \brief This function handles General Events
//!
//! \param[in]     pDevEvent - Pointer to General Event Info
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkGeneralEventHandler(SlDeviceEvent_t *pDevEvent)
{
    //
    // Most of the general errors are not FATAL are are to be handled
    // appropriately by the application
    //
    UART_PRINT("[GENERAL EVENT] - ID=[%d] Sender=[%d]\n\n",
               pDevEvent->EventData.deviceEvent.status,
               pDevEvent->EventData.deviceEvent.sender);
}


//*****************************************************************************
//
//! This function handles socket events indication
//!
//! \param[in]      pSock - Pointer to Socket Event Info
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkSockEventHandler(SlSockEvent_t *pSock)				//套接字异步事件处理程序
{
    //
    // This application doesn't work w/ socket - Events are not expected
    //
    switch( pSock->Event )
    {
		/*
		 * 这个连接状态下，EventData返回的数据是SlSockTxFailEventData_t 包含
		 * sd
		 * status
		 * mac
		 * */
        case SL_SOCKET_TX_FAILED_EVENT:														//socket 发送失败事件
            switch( pSock->socketAsyncEvent.SockTxFailData.status)
            {
                case SL_ECLOSE:																//关闭失败，传输所有队列中的包
                    UART_PRINT("[SOCK ERROR] - close socket (%d) operation "
                                "failed to transmit all queued packets\n\n",
                                    pSock->socketAsyncEvent.SockTxFailData.sd);				//打印事件sd
                    break;
                default:
                    UART_PRINT("[SOCK ERROR] - TX FAILED  :  socket %d , reason "
                                "(%d) \n\n",
                                pSock->socketAsyncEvent.SockTxFailData.sd, pSock->socketAsyncEvent.SockTxFailData.status);
                  break;
            }
            break;
			/*
			 * 这个连接状态下，EventData返回的数据是SockAsyncData 包含
			 * sd
			 * type: SSL_ACCEPT or RX_FRAGMENTATION_TOO_BIG or OTHER_SIDE_CLOSE_SSL_DATA_NOT_ENCRYPTED
			 * val
			 * */
            case SL_SOCKET_ASYNC_EVENT:														//socket 非同步事件

                    	 switch(pSock->socketAsyncEvent.SockAsyncData.type)
                    	 {
                    	 case SSL_ACCEPT:/*accept failed due to ssl issue ( tcp pass)*/		//接收失败由于ssl问题
                    		 UART_PRINT("[SOCK ERROR] - close socket (%d) operation"
                    				 	 "accept failed due to ssl issue\n\r",
                    				 	 pSock->socketAsyncEvent.SockAsyncData.sd);
                    	 case RX_FRAGMENTATION_TOO_BIG:										//连接少，接收碎片包正在大于16k，包正在被释放
                    		 UART_PRINT("[SOCK ERROR] -close scoket (%d) operation"
            							 "connection less mode, rx packet fragmentation\n\r"
                    				 	 "> 16K, packet is being released",
            							 pSock->socketAsyncEvent.SockAsyncData.sd);
                    	 case OTHER_SIDE_CLOSE_SSL_DATA_NOT_ENCRYPTED:						//正在从安全的站点到不安全的站点进行远程访问
                    		 UART_PRINT("[SOCK ERROR] -close socket (%d) operation"
                    				 	 "remote side down from secure to unsecure\n\r",
                    		 			pSock->socketAsyncEvent.SockAsyncData.sd);
                    	 default:
                    		 UART_PRINT("unknown sock async event: %d\n\r",
                    				 	 pSock->socketAsyncEvent.SockAsyncData.type);
                    	 }
                    	break;
        default:
        	UART_PRINT("[SOCK EVENT] - Unexpected Event [%x0x]\n\n",pSock->Event);
          break;
    }

}


//*****************************************************************************
//
//! \brief This function handles ping report events
//!
//! \param[in]     pPingReport - Ping report statistics
//!
//! \return None
//!
//*****************************************************************************
//static void SimpleLinkPingReport(SlPingReport_t *pPingReport)
//{
//    SET_STATUS_BIT(g_ulStatus, STATUS_BIT_PING_DONE);		//ping相应状态位置位
//    g_ulPingPacketsRecv = pPingReport->PacketsReceived;		//存储获得的ping的数量
//}

//*****************************************************************************
// SimpleLink Asynchronous Event Handlers -- End
//*****************************************************************************



//*****************************************************************************
//
//! \brief This function initializes the application variables
//!
//! \param    None
//!
//! \return None
//!
//*****************************************************************************
static void InitializeAppVariables()
{
    g_ulStatus = 0;
    g_ulPingPacketsRecv = 0;
    g_ulGatewayIP = 0;
    g_ulStaIp = 0;//change
    memset(g_ucConnectionSSID,0,sizeof(g_ucConnectionSSID));
    memset(g_ucConnectionBSSID,0,sizeof(g_ucConnectionBSSID));
}


//*****************************************************************************
//! \brief This function puts the device in its default state. It:
//!           - Set the mode to STATION
//!           - Configures connection policy to Auto and AutoSmartConfig
//!           - Deletes all the stored profiles
//!           - Enables DHCP
//!           - Disables Scan policy
//!           - Sets Tx power to maximum
//!           - Sets power policy to normal
//!           - Unregister mDNS services
//!           - Remove all filters
//!
//! \param   none
//! \return  On success, zero is returned. On error, negative is returned
//*****************************************************************************

static long ConfigureSimpleLinkToDefaultState()
{
    SlVersionFull   ver = {0};
    _WlanRxFilterOperationCommandBuff_t  RxFilterIdMask = {0};

    unsigned char ucVal = 1;
    unsigned char ucConfigOpt = 0;
    unsigned char ucConfigLen = 0;
    unsigned char ucPower = 0;

    long lRetVal = -1;
    long lMode = -1;
    lMode = sl_Start(0, 0, 0);	//开启 simplelink 设备，成功后返回当前的激活的状态(AP/STA/P2Pa)或者错误状态
    ASSERT_ON_ERROR(lMode);
    lRetVal = sl_WlanPolicySet(SL_POLICY_CONNECTION,
                                    SL_CONNECTION_POLICY(0, 0, 0, 0, 0), NULL, 0);		//设置连接规则为自动和智能连接
    Task_setPri(task_start_linik_handle,2);
    ASSERT_ON_ERROR(lRetVal);
    // If the device is not in station-mode, try configuring it in station-mode
    if (ROLE_STA != lMode)					//判断设备是否是STA模式
    {
        if (ROLE_AP == lMode)				//如果要是AP模式需要等待分配ip完成
        {
            // If the device is in AP mode, we need to wait for this event
            // before doing anything
            while(!IS_IP_ACQUIRED(g_ulStatus))
            {
#ifndef SL_PLATFORM_MULTI_THREADED
              _SlNonOsMainLoopTask();
#endif
            }
        }

        // Switch to STA role and restart
        lRetVal = sl_WlanSetMode(ROLE_STA);	//转换当前的模式为STA模式，转换模式后需要重新启动设备，完成设置
        ASSERT_ON_ERROR(lRetVal);
        lRetVal = sl_Stop(0xFF);			//停止当前的设别
        ASSERT_ON_ERROR(lRetVal);

        lRetVal = sl_Start(0, 0, 0);		//重新启动设备
        ASSERT_ON_ERROR(lRetVal);

        // Check if the device is in station again
        if (ROLE_STA != lRetVal)			//判断STA模式是否设置成功
        {
            // We don't want to proceed if the device is not coming up in STA-mode
            ASSERT_ON_ERROR(DEVICE_NOT_IN_STATION_MODE);
        }
    }

    // Get the device's version-information
    ucConfigOpt = SL_DEVICE_GENERAL_VERSION;
    ucConfigLen = sizeof(ver);
    lRetVal = sl_DevGet(SL_DEVICE_GENERAL_CONFIGURATION, &ucConfigOpt,
                                &ucConfigLen, (unsigned char *)(&ver));//获取设备的版本信息
    ASSERT_ON_ERROR(lRetVal);

    UART_PRINT("Host Driver Version: %s\n\r",SL_DRIVER_VERSION);
    UART_PRINT("\n\rBuild Version %d.%d.%d.%d.31.%d.%d.%d.%d.%d.%d.%d.%d\n\r",
    ver.NwpVersion[0],ver.NwpVersion[1],ver.NwpVersion[2],ver.NwpVersion[3],
    ver.ChipFwAndPhyVersion.FwVersion[0],ver.ChipFwAndPhyVersion.FwVersion[1],
    ver.ChipFwAndPhyVersion.FwVersion[2],ver.ChipFwAndPhyVersion.FwVersion[3],
    ver.ChipFwAndPhyVersion.PhyVersion[0],ver.ChipFwAndPhyVersion.PhyVersion[1],
    ver.ChipFwAndPhyVersion.PhyVersion[2],ver.ChipFwAndPhyVersion.PhyVersion[3]);

    // Set connection policy to Auto + SmartConfig
    //      (Device's default connection policy)
    lRetVal = sl_WlanPolicySet(SL_POLICY_CONNECTION,
                                SL_CONNECTION_POLICY(1, 0, 0, 0, 1), NULL, 0);		//设置连接规则为自动和智能连接
    ASSERT_ON_ERROR(lRetVal);

    // Remove all profiles
    lRetVal = sl_WlanProfileDel(0xFF);												//删除所有的配置文件
    ASSERT_ON_ERROR(lRetVal);
    lRetVal=sl_NetCfgGet(SL_MAC_ADDRESS_GET,NULL, &macAddressLen,(unsigned char *) macAddressVal);	//获取mac地址，网管地址
    UART_PRINT("\r\nDevice MAC is %x:%x:%x:%x:%x:%x\r\n",macAddressVal[0], macAddressVal[1],macAddressVal[2],\
    		macAddressVal[3],macAddressVal[4],macAddressVal[5]);					//打印获取到的mac信息


    //
    // Device in station-mode. Disconnect previous connection if any
    // The function returns 0 if 'Disconnected done', negative number if already
    // disconnected Wait for 'disconnection' event if 0 is returned, Ignore
    // other return-codes
    //
    lRetVal = sl_WlanDisconnect();								//断开当前的连接
    if(0 == lRetVal)
    {
        // Wait
        while(IS_CONNECTED(g_ulStatus))
        {
#ifndef SL_PLATFORM_MULTI_THREADED
              _SlNonOsMainLoopTask();
#endif
        }
    }

    // Enable DHCP client    连接设置，可以设置mac地址，dhcp，和静态ip设置，等等
    lRetVal = sl_NetCfgSet(SL_IPV4_STA_P2P_CL_DHCP_ENABLE,1,1,&ucVal);		//设置自动分配IP
    ASSERT_ON_ERROR(lRetVal);

    // Disable scan
    ucConfigOpt = SL_SCAN_POLICY(0);
    lRetVal = sl_WlanPolicySet(SL_POLICY_SCAN , ucConfigOpt, NULL, 0);		//禁止扫描ssid
    ASSERT_ON_ERROR(lRetVal);

    // Set Tx power level for station mode
    // Number between 0-15, as dB offset from max power - 0 will set max power
    ucPower = 0;
    lRetVal = sl_WlanSet(SL_WLAN_CFG_GENERAL_PARAM_ID,
            WLAN_GENERAL_PARAM_OPT_STA_TX_POWER, 1, (unsigned char *)&ucPower);
    ASSERT_ON_ERROR(lRetVal);												//设置最大发射功率

    // Set PM policy to normal					//设置正常的电源模式
    lRetVal = sl_WlanPolicySet(SL_POLICY_PM , SL_NORMAL_POLICY, NULL, 0);
    ASSERT_ON_ERROR(lRetVal);

    // Unregister mDNS services					//不注册DNS服务
    lRetVal = sl_NetAppMDNSUnRegisterService(0, 0);
    ASSERT_ON_ERROR(lRetVal);

    // Remove  all 64 filters (8*8)
    memset(RxFilterIdMask.FilterIdMask, 0xFF, 8);
    lRetVal = sl_WlanRxFilterSet(SL_REMOVE_RX_FILTER, (_u8 *)&RxFilterIdMask,
                       sizeof(_WlanRxFilterOperationCommandBuff_t));			//删除接收滤波
    ASSERT_ON_ERROR(lRetVal);

    lRetVal = sl_Stop(SL_STOP_TIMEOUT);											//停止当前的设备 相应时间为200ms
    ASSERT_ON_ERROR(lRetVal);

    InitializeAppVariables();													//初始化变量

    return lRetVal; // Success
}

//*****************************************************************************
//! \brief This function checks the LAN connection by pinging the AP's gateway
//!
//! \param  None
//!
//! \return 0 on success, negative error-code on error
//!
//*****************************************************************************
//static long CheckLanConnection()
//{
//    SlPingStartCommand_t pingParams = {0};
//    SlPingReport_t pingReport = {0};
//
//    long lRetVal = -1;
//
//    CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_PING_DONE);		//清空ping的标志位
//    g_ulPingPacketsRecv = 0;
//
//    // Set the ping parameters
//    pingParams.PingIntervalTime = PING_INTERVAL;			//设置每次ping的时间间隔 单位ms
//    pingParams.PingSize = PING_PKT_SIZE;					//设置ping包的大小
//    pingParams.PingRequestTimeout = PING_TIMEOUT;			//设置ping的超时时间	  单位ms
//    pingParams.TotalNumberOfAttempts = NO_OF_ATTEMPTS;		//ping 请求的最大次数
//    pingParams.Flags = 0;									//所有的请求发送完成后调用回调函数
//    pingParams.Ip = g_ulGatewayIP;							//AP的网关地址
//
//    // Check for LAN connection
//    lRetVal = sl_NetAppPingStart((SlPingStartCommand_t*)&pingParams, SL_AF_INET,
//                            (SlPingReport_t*)&pingReport, SimpleLinkPingReport);//设置ping开始，ping的类型为IP4 ,ping存放的数据在pingReport,回调函数为SimpleLinkPingReport
//    ASSERT_ON_ERROR(lRetVal);
//
//    // Wait for NetApp Event
//    while(!IS_PING_DONE(g_ulStatus))						//等待ping完成
//    {
//#ifndef SL_PLATFORM_MULTI_THREADED
//        _SlNonOsMainLoopTask();
//#endif
//    }
//
//    if(0 == g_ulPingPacketsRecv)							//如果ping接收到数量为0，ping失败，打印错误信息
//    {
//        //Problem with LAN connection
//        ASSERT_ON_ERROR(LAN_CONNECTION_FAILED);
//    }
//
//    // LAN connection is successful
//    return SUCCESS;
//}


//*****************************************************************************
//! \brief This function checks the internet connection by pinging
//!     the external-host (HOST_NAME)
//!
//! \param  None
//!
//! \return  0 on success, negative error-code on error
//!
//*****************************************************************************
//static long CheckInternetConnection()
//{
//    SlPingStartCommand_t pingParams = {0};
//    SlPingReport_t pingReport = {0};
//
//    unsigned long ulIpAddr = 0;
//    long lRetVal = -1;
//
//    CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_PING_DONE);		//清空ping的标志位
//    g_ulPingPacketsRecv = 0;
//
//    // Set the ping parameters
//    pingParams.PingIntervalTime = PING_INTERVAL;			//设置每次ping的时间间隔 单位ms
//    pingParams.PingSize = PING_PKT_SIZE;					//设置ping包的大小
//    pingParams.PingRequestTimeout = PING_TIMEOUT;			//设置ping的超时时间	  单位ms
//    pingParams.TotalNumberOfAttempts = NO_OF_ATTEMPTS;		//ping 请求的最大次数
//    pingParams.Flags = 0;									//所有的请求发送完成后调用回调函数
//    pingParams.Ip = g_ulGatewayIP;							//AP的网关地址
//
//    // Get external host IP address   通过域名获取外网的ip地址
//    lRetVal = sl_NetAppDnsGetHostByName((signed char*)HOST_NAME, sizeof(HOST_NAME),
//                                                &ulIpAddr, SL_AF_INET);//通过IP4 获取HOST_NAME的IP地址，存放到ulIpAddr
//    ASSERT_ON_ERROR(lRetVal);
//
//    // Replace the ping address to match HOST_NAME's IP address
//    pingParams.Ip = ulIpAddr;								//设置要ping的外网服务器地址
//
//    // Try to ping HOST_NAME
//    lRetVal = sl_NetAppPingStart((SlPingStartCommand_t*)&pingParams, SL_AF_INET,
//                            (SlPingReport_t*)&pingReport, SimpleLinkPingReport);//设置ping开始，ping的类型为IP4 ,ping存放的数据在pingReport,回调函数为SimpleLinkPingReport
//    ASSERT_ON_ERROR(lRetVal);
//
//    // Wait
//    while(!IS_PING_DONE(g_ulStatus))						//等待ping完成
//    {
//      // Wait for Ping Event
//#ifndef SL_PLATFORM_MULTI_THREADED
//        _SlNonOsMainLoopTask();
//#endif
//    }
//
//    if (0 == g_ulPingPacketsRecv)							//如果ping接收到数量为0，ping失败，打印错误信息
//    {
//        // Problem with internet connection
//        ASSERT_ON_ERROR(INTERNET_CONNECTION_FAILED);
//    }
//
//    // Internet connection is successful
//    return SUCCESS;
//}


//****************************************************************************
//
//! \brief Connecting to a WLAN Accesspoint
//!
//!  This function connects to the required AP (SSID_NAME) with Security
//!  parameters specified in te form of macros at the top of this file
//!
//! \param  None
//!
//! \return  None
//!
//! \warning    If the WLAN connection fails or we don't aquire an IP
//!            address, It will be stuck in this function forever.
//
//****************************************************************************
//static long WlanConnect()
//{
//    SlSecParams_t secParams = {0};
//    long lRetVal = 0;
//    uint16_t Connect_time_out=0;
////        strcpy(ssidname_get,"719Radio");
////        strcpy(key_get,"1234567890");
////        security_key_get=SL_SEC_TYPE_WPA;
//
////    secParams.Key = (signed char*)SECURITY_KEY;	//连接AP的密码
////    secParams.KeyLen = strlen(SECURITY_KEY);	//连接AP的密码长度
////    secParams.Type = SECURITY_TYPE;				//连接AP的密码类型
//    strcpy(ssidname_get, "gyynb");
//    strcpy(key_get, "1234612346");
//    security_key_get = SL_SEC_TYPE_WPA;
//
//    secParams.Key = (signed char*)key_get;					//连接AP的密码
//    secParams.KeyLen = strlen(key_get);						//连接AP的密码长度
//    secParams.Type = security_key_get;						//连接AP安全模式
//
//    lRetVal = sl_WlanProfileAdd((signed char*) ssidname_get, strlen(ssidname_get), 0,
//			&secParams, 0, 0, 0);							//添加连接规则，用于断网重连
//	ASSERT_ON_ERROR(lRetVal);
//	lRetVal = sl_WlanConnect((signed char*) ssidname_get, strlen(ssidname_get), 0,
//			&secParams, 0);								   //连接到ssidname_get的AP上
//	ASSERT_ON_ERROR(lRetVal);
//    // Wait for WLAN Event
//    while((Connect_time_out<=800)&&((!IS_CONNECTED(g_ulStatus)) || (!IS_IP_ACQUIRED(g_ulStatus))))	//等待连接完成
//    {
//        // Toggle LEDs to Indicate Connection Progress
////    	MAP_UtilsDelay(800000);
//    	osi_Sleep(100);
//    	Connect_time_out++;
////    	UART_PRINT("Connect_time_out=%d\r\n",Connect_time_out);
//    	//       GPIO_IF_LedOn(MCU_IP_ALLOC_IND);
//    }
//    if(Connect_time_out>800)
//    {
//    	GetScanResult(&g_NetEntries[0]);
//    	UART_PRINT("STA TIME OUT!!!!");
//		return 2;
//    }
//    else
//    {
////    	Connect_time_out=0;
//    	return SUCCESS;
//    }
//
//}

//****************************************************************************
//
//! \brief Start simplelink, connect to the ap and run the ping test
//!
//! This function starts the simplelink, connect to the ap and start the ping
//! test on the default gateway for the ap
//!
//! \param[in]  pvParameters - Pointer to the list of parameters that
//!             can bepassed to the task while creating it
//!
//! \return  None
//
//****************************************************************************
void WlanStationMode( void *pvParameters )
{
    long lRetVal = -1;
  //  long lCountSSID;
   // LCD_Control page_commond;
   // int i=0;
//	unsigned long uiFileSize;
	char *FileRead = "tftp_client.bin";
//    char *filename="test";
//    char *testword="wertyuiosdfghjkdfghjkdfghj\r\n";
//    unsigned long uiFileSize;
//    char *FileRead = "1.txt";
  //  unsigned long token1=0;
    InitializeAppVariables();
    //
    // Following function configure the device to default state by cleaning
    // the persistent settings stored in NVMEM (viz. connection profiles &
    // policies, power policy etc)
    //
    // Applications may choose to skip this step if the developer is sure
    // that the device is in its default state at start of applicaton
    //
    // Note that all profiles and persistent settings that were done on the
    // device will be lost
    //
    lRetVal = ConfigureSimpleLinkToDefaultState();			//初始化设备，
    if(lRetVal < 0)
    {
        if (DEVICE_NOT_IN_STATION_MODE == lRetVal)
        {
            UART_PRINT("Failed to configure the device in its default state\n\r");
        }
    }

    //UART_PRINT("Device is configured in default state \n\r");

    //
    // Assumption is that the device is configured in station mode already
    // and it is in its default state
    //
    lRetVal = sl_Start(0, 0, 0);								//开启设备

    Current_Mode=lRetVal;										//获取当前工作模式
    if (lRetVal < 0 || ROLE_STA != lRetVal)						//判断是否为STA模式，不是的话打印错误信息，进入死循环
    {
        UART_PRINT("Failed to start the device \n\r");
//        LOOP_FOREVER();
    }
    UART_PRINT("Device started as STATION \n\r");
    send_commmand(skip,92);
//	lRetVal=sl_FsDel((unsigned char *)IMG_OLD1,token1);
//	if(lRetVal < 0)
//	{
//		ERR_PRINT(lRetVal);
//	}
	lRetVal = osi_TaskCreate(task_file_save, (const signed char*) "task_file_save",
			1024, NULL, 2, NULL);																	//系统待机任务
	if (lRetVal < 0)
	{
		ERR_PRINT(lRetVal);
//		LOOP_FOREVER();
	}

	//ConfigureMode(ROLE_AP);
//    lRetVal = osi_TaskCreate(task_TFTP, (const signed char *)"task_TFTP",
//    	OSI_STACK_SIZE,NULL,2,NULL );
//    if(lRetVal < 0)
//    {
//        ERR_PRINT(lRetVal);
//    //    LOOP_FOREVER();
//    }
    //搜索附近的SSID
//    lCountSSID=GetScanResult(&g_NetEntries[0]);
    //Connecting to WLAN AP
    //
//    lRetVal=Connect_to_ap();
//    if(lRetVal < 0)
//    {
//        UART_PRINT("Failed to establish connection w/ an AP \n\r");
//        ERR_PRINT(lRetVal);
//    }

//    UART_PRINT("Connection established w/ AP and IP is aquired \n\r");
/*************************************************************/
//     //LCD Commond 测试
//    page_commond.commond=WriteVariableCommond;
//    page_commond.addr[0]=0x00;
//    page_commond.addr[1]=0x10;
//    page_commond.data_len=5;
//    page_commond.data[0]=0x00;
//    page_commond.data[1]=0x10;
/*************************************************************/

/*************************************************************/
    //文件读写测试
//    for(i=0;i<5;i++)
//    	lRetVal=WriteFile(filename,testword);
//	if (lRetVal < 0)
//	{
//		ERR_PRINT(lRetVal);
//	}
//	lRetVal=ReadFile(filename,0,0);
//    if (lRetVal < 0)
//    {
//    	ERR_PRINT(lRetVal);
//    }
/*************************************************************/

/*************************************************************/

/*************************************************************/
/*************************************************************/
	while(1)
	{
//	//	UART_PRINT("in the BGN");
//        //tftp 测试
		//_i32 DeviceFileHandle = -1;

//		_u8 ReadBuffer[100];
//		_u32 Offset=0;
//		unsigned long token1=0;
		osi_Sleep(100);
		//	lRetVal = sl_FsOpen("plant_control.bin",
//    		Offset = 50;
//    		lRetVal = sl_FsWrite( DeviceFileHandle,
//    		Offset,
//    		(_u8 *)&ReadBuffer[50],
//    		50);
//			if (lRetVal < 0)															//判断返回值返回错误信息
//			{
//				ERR_PRINT(lRetVal);
//			}
//			UART_PRINT("successful....");
//								if(Client_statue==1)
//								{
//									osi_Sleep(4000);
//									UART_PRINT("jie shou wen jian ....\r\n");
//									ImageLoader_reboot(FileRead ,IMG_FACTORY_DEFAULT);
//									Client_statue=0;
//								}
//			lRetVal=tftp_getfile(FileRead,GEI_FILE_SIZE_MAX);
//			if(lRetVal < 0)
//			{
//				ERR_PRINT(lRetVal);
//			}



//    		lRetVal = sl_FsOpen(IMG_FACTORY_DEFAULT,FS_MODE_OPEN_WRITE,NULL,&DeviceFileHandle);
//    		if(lRetVal < 0)
//    		{
//    			ERR_PRINT(lRetVal);
//    		}
//    		UART_PRINT("open yes");
//			lRetVal = sl_FsWrite( DeviceFileHandle,Offset,(_u8 *)tftp_get_char,FILE_SIZE_MAX);
//			if(lRetVal < 0)
//			{
//				ERR_PRINT(lRetVal);
//			}
//			UART_PRINT("write yes");
//			lRetVal = sl_FsClose(DeviceFileHandle,
//			NULL,
//			NULL,
//			NULL );
//			UART_PRINT("close yes");
//			lRetVal = sl_FsOpen(IMG_FACTORY_DEFAULT,FS_MODE_OPEN_WRITE,NULL,&DeviceFileHandle);
//			if(lRetVal < 0)
//			{
//				ERR_PRINT(lRetVal);
//			}
//			UART_PRINT("open yes");
//			lRetVal = sl_FsWrite( DeviceFileHandle,Offset,(_u8 *)FileRead,GEI_FILE_SIZE_MAX);
//			if(lRetVal < 0)
//			{
//				ERR_PRINT(lRetVal);
//			}
//			UART_PRINT("write yes");
//			lRetVal = sl_FsClose(DeviceFileHandle,
//			NULL,
//			NULL,
//			NULL );
//			UART_PRINT("close yes");
//			lRetVal=sl_FsDel((unsigned char *)"1.bin",token1);
//				if(lRetVal < 0)
//				{
//					ERR_PRINT(lRetVal);
//				}
//				UART_PRINT("del successful");
//			if(rename("plant_control.bin",IMG_FACTORY_DEFAULT) == 0)
//			{
//				UART_PRINT("已经把文件 %s 修改为 %s.\n", IMG_OLD, IMG_FACTORY_DEFAULT);
//			}
//			else
//			{
//				UART_PRINT("rename error\n");
//			}
		 //  ImageLoader_reboot(IMG_FACTORY_DEFAULT,IMG_FACTORY_DEFAULT);//"blinky.bin"IMG_FACTORY_DEFAULT
//    		lRetVal = sl_FsOpen(IMG_FACTORY_DEFAULT,
//    				FS_MODE_OPEN_CREATE(GEI_FILE_SIZE_MAX,_FS_FILE_OPEN_FLAG_COMMIT),
//					                    NULL,
//										&DeviceFileHandle);
//			if (lRetVal < 0)															//判断返回值返回错误信息
//			{
//				ERR_PRINT(lRetVal);
//			}
//    		lRetVal = sl_FsClose(DeviceFileHandle,
//    		NULL,
//    		NULL,
//    		NULL );
//			if (lRetVal < 0)															//判断返回值返回错误信息
//			{
//				ERR_PRINT(lRetVal);
//			}

//			lRetVal = sl_FsWrite( 0,
//    		0,
//			"plant_control.bin",
//			GEI_FILE_SIZE_MAX);
//			UART_PRINT("xieru successful\r\n");
//			if (lRetVal < 0)															//判断返回值返回错误信息
//			{
//			//	lRetVal = sl_FsClose(pFileHandle1, 0, 0, 0);
//				ERR_PRINT(lRetVal);
//				//return lRetVal;
//			}
//			lRetVal=ReadFile(FileRead,0,0);
//			if (lRetVal < 0)
//			{
//				ERR_PRINT(lRetVal);
//			}
//			lRetVal=tftp_sentfile(FileRead,"2.txt");
//			if(lRetVal < 0)
//			{
//				ERR_PRINT(lRetVal);
//				ImageLoader_reboot("2.txt",IMG_FACTORY_DEFAULT);//"blinky.bin"IMG_FACTORY_DEFAULT
//			}
//		    Client_statue=0;
//    	}
	}
//		///*************************************************************/
		  // ImageLoader_reboot("2.txt",IMG_FACTORY_DEFAULT);//"blinky.bin"IMG_FACTORY_DEFAULT
//		   osi_Sleep(100);
//    	}
//    //	plant_control_ptotobuffer_V2
///*************************************************************/
/////*固件升级*/
//    	if(GPIO_IF_Get(13)==1)
//		{
//    		MAP_UtilsDelay(5100);
//    		if(GPIO_IF_Get(13)==1)
//    		{
//    			UART_PRINT("************************* \n\r");
////    			RunMagic("/sys/mcuimg1.bin");//
//    			ImageLoader_reboot("blinky.bin",IMG_FACTORY_DEFAULT);
//    		}
//    		while(GPIO_IF_Get(13)==1);
//		}
///*************************************************************/
//    	uiFileSize=GEI_FILE_SIZE_MAX;
//    	lRetVal=tftp_getfile(FileRead,GEI_FILE_SIZE_MAX);
//    	if(lRetVal < 0)
//    	{
//    		ERR_PRINT(lRetVal);
//    	}
//    	lRetVal=ReadFile(FileRead,0,0);
//    	if (lRetVal < 0)
//    	{
//    		ERR_PRINT(lRetVal);
//    	}
//    	lRetVal=tftp_sentfile(FileRead,"2.txt");
//    	if(lRetVal < 0)
//    	{
//    		ERR_PRINT(lRetVal);
//    	}
//    	 ImageLoader_reboot("2.bin",IMG_FACTORY_DEFAULT);//"blinky.bin"IMG_FACTORY_DEFAULT
///*风扇测试*/
//	if(GPIO_IF_Get(13)==1)
//	{
//		MAP_UtilsDelay(5100);
//		if(GPIO_IF_Get(13)==1)
//		 {
//
////				switch (++i)
////				{
////
////					case 1:														//风扇正转控制
////						GPIO_IF_Set(6, 1);
////						Timer_IF_SetDutyCycle(FAN, 50);
////						break;
////					case 2:
////						GPIO_IF_Set(6, 0);
////						Timer_IF_SetDutyCycle(FAN, 50);
////						break;
////					case 3:															//风扇停转
////						Timer_IF_SetDutyCycle(FAN, 100);
////						i=0;
////						break;
////					default:
////						break;
////
////				}
//
//			Timer_IF_SetDutyCycle(FAN, 100-i);
//			Timer_IF_SetDutyCycle(LAMP1, i);
//			Timer_IF_SetDutyCycle(LAMP2, i);
//			i+=10;
//			page_commond.data[1]=i;
//			Send_LCD_Commond(page_commond);
////			GPIO_IF_Toggle(5);
//			UART_PRINT("*************************i=%d \n\r",i);
//
//			if(i>=110)
//				i=0;
////    			RunMagic("/sys/mcuimg1.bin");//
//			}
//		while(GPIO_IF_Get(13)==1);
//	}
//	UART_PRINT("flowcount=%d, \n\r",flow_count);
//	if(flow_count>=300)
//		flow_count=0;
/*************************************************************/
 //LCD Commond 测试
//    	Send_LCD_Commond(page_commond);
/*************************************************************/

/*************************************************************/
/*扫描SSID测试*/
//    	if(GPIO_IF_Get(13)==1)
//		{
//    		Event_post(Scan_SSID_Event,Event_Id_00);
//		}
/*************************************************************/

/*************************************************************/
//从sntp服务器中获取时间测试
//		lRetVal = LinkNTPServerAndGet();
//		if (lRetVal < 0)
//		{
//			ERR_PRINT(lRetVal);
//		}
//		get_real_time(time_get, GMT_DIFF_TIME_HRS, GMT_DIFF_TIME_MINS);
//		UART_PRINT("%d-%d-%d %d:%d:%d\r\n",time_axis.year,time_axis.month,time_axis.day,time_axis.hour,time_axis.minute,time_axis.second);
/*************************************************************/

//   	BsdTcpClient(5050);								//TCP客户端发送数据

}
//*****************************************************************************
//
//! Start_link
//! 初始化连接程序，此任务开始状态，会扫描SSID信息，完成后转化为AP模式，然后上传到上位机
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void Start_link(void)
{
	;
}


//*****************************************************************************
//
//! Application startup display on UART
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void DisplayBanner(char * AppName)
{

    UART_PRINT("\n\n\n\r");
    UART_PRINT("\t\t *************************************************\n\r");
    UART_PRINT("\t\t           CC3200 %s Application       \n\r", AppName);
    UART_PRINT("\t\t *************************************************\n\r");
    UART_PRINT("\n\n\n\r");
}
//change
//****************************************************************************
//
//! Confgiures the mode in which the device will work
//!
//! \param iMode is the current mode of the device
//!
//! This function
//!    1. prompt user for desired configuration and accordingly configure the
//!          networking mode(STA or AP).
//!       2. also give the user the option to configure the ssid name in case of
//!       AP mode.
//!
//! \return sl_start return value(int).
//
//****************************************************************************
int ConfigureMode(int iMode)
{
    long   lRetVal = -1;
    _u8 val=0;
    strcpy(pcSsidName, "gyynb");
    strcpy(key_get, "1234612346");
    lRetVal = sl_WlanSetMode(iMode);
    ASSERT_ON_ERROR(lRetVal);

    if(iMode==ROLE_AP)
    {
		//设置ap的ssid

    	//设置ap的ssid
			lRetVal = sl_WlanSet(SL_WLAN_CFG_AP_ID, WLAN_AP_OPT_SSID, strlen(pcSsidName),
									(unsigned char*)pcSsidName);						//设置SSID名称
			ASSERT_ON_ERROR(lRetVal);
			val=SL_SEC_TYPE_WPA;
			lRetVal = sl_WlanSet(SL_WLAN_CFG_AP_ID, WLAN_AP_OPT_SECURITY_TYPE,1,
									(_u8 *)&val);										//设置安全类型
			ASSERT_ON_ERROR(lRetVal);
			lRetVal = sl_WlanSet(SL_WLAN_CFG_AP_ID, WLAN_AP_OPT_PASSWORD,strlen(key_get),
									(unsigned char*) key_get);						//设置连接密码
				ASSERT_ON_ERROR(lRetVal);
			UART_PRINT("Device is configured in AP mode\n\r");
    }
    /* Restart Network processor */
    lRetVal = sl_Stop(SL_STOP_TIMEOUT);												//重新启动设备

    // reset status bits
    CLR_STATUS_BIT_ALL(g_ulStatus);													//清空所有的状态位

    return sl_Start(NULL,NULL,NULL);
}

//****************************************************************************
//
//!    \brief device will try to ping the machine that has just connected to the
//!           device.
//!
//!    \param  ulIpAddr is the ip address of the station which has connected to
//!            device
//!
//!    \return 0 if ping is successful, -1 for error
//
//****************************************************************************
//int PingTest(unsigned long ulIpAddr)
//{
//    signed long           lRetVal = -1;
//    SlPingStartCommand_t PingParams;
//    SlPingReport_t PingReport;
//    PingParams.PingIntervalTime = PING_INTERVAL;			//设置ping间隔时间，单位ms
//    PingParams.PingSize = PING_PKT_SIZE;					//设置ping包的大小
//    PingParams.PingRequestTimeout = PING_TIMEOUT;			//设置ping超时时间单位ms
//    PingParams.TotalNumberOfAttempts = NO_OF_ATTEMPTS;		//设置ping的次数
//    PingParams.Flags = PING_FLAG;							//所有的ping完成后返回
//    PingParams.Ip = ulIpAddr; /* Cleint's ip address */
//
//    UART_PRINT("Running Ping Test...\n\r");
//    /* Check for LAN connection */
//    lRetVal = sl_NetAppPingStart((SlPingStartCommand_t*)&PingParams, SL_AF_INET,
//                            (SlPingReport_t*)&PingReport, NULL);//开始ping
//    ASSERT_ON_ERROR(lRetVal);
//
//    g_ulPingPacketsRecv = PingReport.PacketsReceived;			//接收返回的数据
//
//    if (g_ulPingPacketsRecv > 0 && g_ulPingPacketsRecv <= NO_OF_ATTEMPTS)//如果收到的返回数据大于0≤NO_OF_ATTEMPTS则ping成功否则ping失败
//    {
//      // LAN connection is successful
//      UART_PRINT("Ping Test successful\n\r");
//    }
//    else
//    {
//        // Problem with LAN connection
//        ASSERT_ON_ERROR(LAN_CONNECTION_FAILED);
//    }
//
//    return SUCCESS;
//}
//*****************************************************************************
//
//! \brief This function Get the Scan Result
//!
//! \param[out]      netEntries - Scan Result
//!
//! \return         Success - Size of Scan Result Array
//!                    Failure   - -1
//!
//! \note			可改进
//!
//
//*****************************************************************************
int GetScanResult(Sl_WlanNetworkEntry_t* netEntries )
{
    unsigned char   policyOpt;
    unsigned long IntervalVal = 60;
    int lRetVal,i=0;

//    policyOpt = SL_CONNECTION_POLICY(0, 0, 0, 0, 0);
//    lRetVal = sl_WlanPolicySet(SL_POLICY_CONNECTION , policyOpt, NULL, 0);
//	ASSERT_ON_ERROR(lRetVal);


    // enable scan
    policyOpt = SL_SCAN_POLICY(1);												  //使能扫描信号

    // set scan policy - this starts the scan
    lRetVal = sl_WlanPolicySet(SL_POLICY_SCAN , policyOpt,
                            (unsigned char *)(IntervalVal), sizeof(IntervalVal));//开启扫描模式 ,设置1分钟扫描一次
	ASSERT_ON_ERROR(lRetVal);


    // delay 1 second to verify scan is started
    osi_Sleep(1000);															//等待扫描开始

    // lRetVal indicates the valid number of entries
    // The scan results are occupied in netEntries[]
    lRetVal = sl_WlanGetNetworkList(0, SCAN_TABLE_SIZE, netEntries);			//从0开始存储扫描信息，共存储20条
	ASSERT_ON_ERROR(lRetVal);

    // Disable scan
    policyOpt = SL_SCAN_POLICY(0);												//关闭扫描信号

    // set scan policy - this stops the scan
    sl_WlanPolicySet(SL_POLICY_SCAN , policyOpt,(unsigned char *)(IntervalVal), sizeof(IntervalVal));//关闭扫描模式，只扫描一次
	ASSERT_ON_ERROR(lRetVal);
	memset(RSSI_Strength,0,20);
	//应该排序
	for(i=0;i<lRetVal;i++)
	{
		RSSI_Strength[i]=(-((netEntries+i)->rssi))/33;
//		UART_PRINT("RSSI_Strength[%d]=%d,%d\r\n",i,RSSI_Strength[i],(netEntries+i)->rssi);
	}
    return lRetVal;

}
//*****************************************************************************
//
//! Disconnect  Disconnects from an Access Point
//!
//! \param  none
//!
//! \return 0 disconnected done, other already disconnected
//
//*****************************************************************************
long
DisconnectFromAP()
{
    long lRetVal = 0;

    if (IS_CONNECTED(g_ulStatus))//判断当然是否有连接
    {
        lRetVal = sl_WlanDisconnect();//断开所有连接
        if(0 == lRetVal)
        {
            // Wait
            while(IS_CONNECTED(g_ulStatus))//等待断开连接操作完成
            {
    #ifndef SL_PLATFORM_MULTI_THREADED
                  _SlNonOsMainLoopTask();
    #else
                  osi_Sleep(1);
    #endif
            }
            return lRetVal;
        }
        else
        {
            return lRetVal;
        }
    }
    else
    {
        return lRetVal;
    }

}
//*****************************************************************************
//
//! Network_IF_GetHostIP
//!
//! \brief  This function obtains the server IP address using a DNS lookup
//!
//! \param[in]  pcHostName        The server hostname
//! \param[out] pDestinationIP    This parameter is filled with host IP address.
//!
//! \return On success, +ve value is returned. On error, -ve value is returned
//!
//
//*****************************************************************************
long Network_IF_GetHostIP( char* pcHostName,unsigned long * pDestinationIP )
{
    long lStatus = 0;

    lStatus = sl_NetAppDnsGetHostByName((signed char *) pcHostName,
                                            strlen(pcHostName),
                                            pDestinationIP, SL_AF_INET);//通过主机的域名获取主机的ip地址，存放到pDestinationIP中
    ASSERT_ON_ERROR(lStatus);

    UART_PRINT("Get Host IP succeeded.\n\rHost: %s IP: %d.%d.%d.%d \n\r\n\r",
                    pcHostName, SL_IPV4_BYTE(*pDestinationIP,3),
                    SL_IPV4_BYTE(*pDestinationIP,2),
                    SL_IPV4_BYTE(*pDestinationIP,1),
                    SL_IPV4_BYTE(*pDestinationIP,0));//打印服务器的ip地址
    return lStatus;

}
//*****************************************************************************
//
//! Network_IF_DeInitDriver
//! The function de-initializes a CC3200 device
//!
//! \param  None
//!
//! \return On success, zero is returned. On error, other
//
//*****************************************************************************
//long
//Network_IF_DeInitDriver(void)
//{
//    long lRetVal = -1;
//    UART_PRINT("SL Disconnect...\n\r");
//
//    //
//    // Disconnect from the AP
//    //
//    if(Current_Mode==ROLE_STA)							//如果为STA模式的话需要调用sl_WlanDisconnect断开连接
//    	lRetVal = DisconnectFromAP();//断开连接
//    else
//    	lRetVal=0;
//
//    //
//    // Stop the simplelink host
//    //
//    sl_Stop(SL_STOP_TIMEOUT);
//
//    //
//    // Reset the state to uninitialized
//    //
//    // reset status bits
//    CLR_STATUS_BIT_ALL(g_ulStatus);													//清空所有的状态位
//
//    return lRetVal;
//}
//*****************************************************************************
//
//! Mode_Change
//! 模式转换函数，模式为STA时转换为AP模式，模式为AP市转换为STA模式
//!
//! \param  None
//!
//! \return On success, zero is returned. On error, other
//
//*****************************************************************************
//*****************************************************************************
//
//! Connect_to_ap
//! 在STA模式 转换连接到的ap
//!
//! \param  None
//!
//! \return On success, zero is returned. On error, other
//
//*****************************************************************************
//long Connect_to_ap(void)
//{
//	long lRetVal = -1;
//	task_delete();																//删除TCP服务器监听服务，和mdns搜寻服务
//	flag_mode_change=1;															//设置手动转换模式标志位
//	lRetVal = sl_WlanPolicySet(SL_POLICY_CONNECTION,							\
//		                  SL_CONNECTION_POLICY(0, 0, 0, 0, 0), NULL, 0);		//设置连接规则为自动和智能连接
//	ASSERT_ON_ERROR(lRetVal);
//
//	lRetVal = DisconnectFromAP();												//断开连接
//
//	sl_Stop(SL_STOP_TIMEOUT);													//停止当前的网络
//	CLR_STATUS_BIT_ALL(g_ulStatus);												//清除所有标志位
//	ASSERT_ON_ERROR(lRetVal);
//
//	lRetVal = sl_Start(0, 0, 0);												//重新启动设备
//	ASSERT_ON_ERROR(lRetVal);
//	if (lRetVal >= 0)
//		Current_Mode = lRetVal;
//	else
//	{
//		UART_PRINT("Failed to start the device \n\r");
////		LOOP_FOREVER();
//	}
//	UART_PRINT("Device started as STATION \n\r");
//	lRetVal = WlanConnect();													//连接到相应的AP模式
//	if (lRetVal < 0)
//	{
//		UART_PRINT("Failed to establish connection w/ an AP \n\r");
////		LOOP_FOREVER();
//	}
//
//	UART_PRINT("Connection established w/ AP and IP is aquired \n\r");
//	lRetVal = sl_WlanPolicySet(SL_POLICY_CONNECTION,
//			SL_CONNECTION_POLICY(1, 0, 0, 0, 0), NULL, 0);						//设置连接规则为自动和智能连接
//	flag_mode_change=0;															//清除手动转换标志位
//	return lRetVal;
//}
