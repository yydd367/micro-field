/*
 * MDNS_server.c
 *
 *  Created on: 2016年3月1日
 *      Author: Kang
 */

#ifndef NETWORK_CONFIG_MDNS_SERVER_C_
#define NETWORK_CONFIG_MDNS_SERVER_C_

#include "MDNS_server.h"
#include "task.h"
#define SERVICE_NAME            "CC3200._uart._tcp.local"
#endif /* NETWORK_CONFIG_MDNS_SERVER_C_ */
//*****************************************************************************
//
//! MDNS_broadcast
//!
//! \param  uldata mdns获取任务后返回的数据
//! \param TextLen mdns的自己长度
//!
//! \return none
//!
//*****************************************************************************
void MDNS_broadcast(MDNS_getdata uldata ,_u16 TextLen)
{
	int iretvalmDNS=1;
	_u16 len=0;
	while (iretvalmDNS)
	{
		sl_NetAppStart(SL_NET_APP_MDNS_ID);
		len = TextLen;
		iretvalmDNS = sl_NetAppDnsGetHostByService(SERVICE_NAME,
				(unsigned char) strlen(SERVICE_NAME), SL_AF_INET,
				(unsigned long *) &(uldata.addr), &uldata.port, &len,
				(_i8 *) uldata.Text);								//返回名称为SERVICE_NAME服务器的ip地址，端口号，和服务信息
		Report("NB%d,\r\n", iretvalmDNS);
	    if(flag_mdns_shut==1)                           //判断是否接收到关闭mdns 闭嘴指令
	    {
	        osi_Sleep(15000);                           //任务休眠6s
	        flag_mdns_shut=0;                           //标志位清零
	    }

		osi_Sleep(100);
	}
	if (iretvalmDNS == 0)										    //如果搜寻到DNS服务器，打印端口号，服务器地址和服务信息
	{
		sl_NetAppStop  (SL_NET_APP_MDNS_ID);
		uldata.TextLen = len;
		uldata.Text[uldata.TextLen] = '\0';
		iretvalmDNS=1;
		Mailbox_post(MDNS_mail,&uldata,BIOS_WAIT_FOREVER);			//邮递当前获得的ip地址和端口号
	}
}

