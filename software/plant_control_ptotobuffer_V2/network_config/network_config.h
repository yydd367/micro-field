/*
 * STA_config.h
 *
 *  Created on: 2016年2月27日
 *      Author: Kang
 */

#ifndef NETWORK_CONFIG_STA_CONFIG_H_
#define NETWORK_CONFIG_STA_CONFIG_H_
// Values for below macros shall be modified for setting the 'Ping' properties
//
#include "systerm_init.h"
#define PING_INTERVAL       1000    /* In msecs */
#define PING_TIMEOUT        3000    /* In msecs */
#define PING_PKT_SIZE       20      /* In bytes */
#define NO_OF_ATTEMPTS      3
#define PING_FLAG           0		/*AP ping 标志位*/

#define SCAN_TABLE_SIZE           20	//设置SSID扫描条数，最大为20条
// Application specific status/error codes
//****************************************************************************
//                      变量外部扩展
//****************************************************************************
extern Sl_WlanNetworkEntry_t g_NetEntries[SCAN_TABLE_SIZE];//存放扫描的SSD信息
extern  unsigned long g_ulStatus;
extern char ssidname_get[33];
extern char key_get[33];
extern char security_key_get;
extern unsigned long  g_ulStaIp;
extern long Current_Mode;
extern unsigned char macAddressVal[SL_MAC_ADDR_LEN];
extern char  pcSsidName[33];
extern uint8_t RSSI_Strength[SCAN_TABLE_SIZE];
extern char Client_statue;
//extern _u8 flag_disconnect,flag_mode_change;
//****************************************************************************
//                      LOCAL FUNCTION PROTOTYPES
//****************************************************************************
static long WlanConnect();
void WlanStationMode( void *pvParameters );
void Start_link(void);
static long CheckLanConnection();
static long CheckInternetConnection();
static void InitializeAppVariables();
static long ConfigureSimpleLinkToDefaultState();
void DisplayBanner(char * AppName);
int PingTest(unsigned long ulIpAddr);
int ConfigureMode(int iMode);
//static int GetSsidName(char *pcSsidName, unsigned int uiMaxLen);
void WlanAPMode( void *pvParameters );
void WorkAsStaMode(void);

int GetScanResult(Sl_WlanNetworkEntry_t* netEntries );

long Network_IF_GetHostIP( char* pcHostName,unsigned long * pDestinationIP );
long Network_IF_DeInitDriver(void);
long DisconnectFromAP(void);
//long Mode_Change(void);
long Connect_to_ap(void);
#endif /* NETWORK_CONFIG_STA_CONFIG_H_ */
