/*
 * task.h
 *
 *  Created on: 2016年3月5日
 *      Author: Kang
 */

#ifndef PLANT_CONTROL_PTOTOBUFFER_COMMON_TASK_H_
#define PLANT_CONTROL_PTOTOBUFFER_COMMON_TASK_H_
#include "systerm_init.h"


//****************************************************************************
//                      变量外部扩展
//****************************************************************************


extern Mailbox_Handle MDNS_mail;
extern Mailbox_Handle UART_mail;
//extern Task_Handle task_mdnsget_handle;
//extern Task_Handle task_tcp_server_handle;
extern Task_Handle task_start_linik_handle;
extern Event_Handle Mode_Change_Event;
extern Event_Handle Restart_wifi_Event;
extern Event_Handle MDNS_Event;
extern Event_Handle Get_Controlfile_Event;
extern Event_Handle Scan_SSID_Event;
extern Event_Handle Updata_time_Event;
extern Event_Handle Start_link_Event;
extern Semaphore_Handle TCP_Client_Pro;
extern Semaphore_Handle UART_SENT_Pro;
extern Semaphore_Handle TCP_Buffer_Pro;

extern Clock_Handle flow_get;
extern uint8_t flag_mdns_shut,Close_soket_flag,protect_release_flag;
extern char OGB_sign;
extern uint8_t present_id;
//extern Event_Handle UART1_Event;
//uint8_t time_begin=0;
//****************************************************************************
//                      LOCAL FUNCTION PROTOTYPES
//****************************************************************************
void task_init(void);
void task_timer_delay(void *pvParameters);
//void task_test2(void *pvParameters);
void task_real_time_skip(void *pvParameters);
//void task_mdnsget(void *pvParameters);
//void task_tcp_server(void *pvParameters);
void task_uart_server(void *pvParameters);
//void task_TFTP(void *pvParameters);
//void task_mode_change(void *pvParameters);
//void task_scan_ssid(void *pvParameters);
//void task_wifi_restart(void *pvParameters);
//void task_start_link(void *pvParameters);
//void task_Get_Controlfile(void *pvParameters);
void task_updata_time(void *pvParameters);
void task_time_control(void *pvParameters);
void task_Control_Water(void *pvParameters);
void task_Control_Tem_color(void *pvParameters);
//void task_send_rssi(void *pvParameters);
//void task_Breath_led(void *pvParameters);
void task_delete(void);
//void task_restore(void);
//void task_commond_Send(char (*backdata)[5]);
//void calculate_flow_task_set(void);
void tcp_command_send(TCP_Message control_message);
//void task_page_skip(void *pvParameters);
//void task_Standby(void *pvParameters);
//void task_blink(void *pvParameters);
void task_file_save(void *pvParameters);
void task_run_statue(void *pvParameters);
void task_Parameters_print(void *pvParameters);
//void task_obg_updata(void *pvParameters);
#endif /* PLANT_CONTROL_PTOTOBUFFER_COMMON_TASK_H_ */
