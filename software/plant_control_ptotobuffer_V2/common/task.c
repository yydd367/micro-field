 /*
 * task.c
 *
 *  Created on: 2016年3月5日
 *      Author: Kang
 */
#include "task.h"
#include "user.h"
#include "MDNS_server.h"
#include "user.h"
Event_Handle Scan_SSID_Event;						//扫描SSID事件
Event_Handle MDNS_Event;							//mdns事件
Event_Handle Control_water_Event;					//流量控制事件
//Event_Handle Lack_water_Event;

Mailbox_Handle MDNS_mail;							//mdns邮箱
Mailbox_Handle UART_mail;							//串口邮箱
Mailbox_Handle water_mail;

#ifdef  PROTO_BUFFER
Semaphore_Handle TCP_Buffer_Pro;					//TCP缓存保护信号量
Semaphore_Handle TCP_Client_Pro;
Semaphore_Handle UART_SENT_Pro;
#else
Semaphore_Handle TCP_UART_Compose_Pro;				//TCP控制台和UART控制台保护信号量
#endif

Task_Handle task_mdnsget_handle;					//mdns获取任务句柄
Task_Handle task_tcp_server_handle;					//TCP 服务器句柄
Task_Handle task_start_linik_handle;
Task_Handle	task_test2_handle;
char test[100]={0};
uint8_t next_water_date=6;
uint8_t last_water_date=1;
uint16_t Temp_water_yeild=800;
////                      lamp_on close 水量  间隔   开水时间 温度    湿度
uint16_t save_Parameter[10]={8,0,20,0,600,3,8,0,27,75};       //没保存数据时显示的数据
uint8_t water_day_array[]={6,11,16,21,25};
Parameter Parameter_set = {{8,0},{20,0},600,3,{8,0},27,75};
//#define total_water     20*1000
//Task_Handle task_send_rssi_handle;
Clock_Handle flow_get;								//流量获取时钟句柄
uint8_t flag_mdns_shut=0,Close_soket_flag=0,save_data_chang=0,data_chang=1,Water_period=0;
uint8_t plan_date=0,plant_statue=0;//收到mdns后闭嘴表中未	protect_release_flag=0
int plant_day=0;
uint8_t data_chang_day=0;
uint8_t external_statue=0x0;       //界面跳转标志位
uint8_t TIME_15min_out=0;
uint8_t water_complete=0;              //防止在进行手动浇水时，系统进行自动浇水
uint8_t standby_statue=0;              //屏幕保护标志    1为屏保     0无屏保
uint8_t chang_time_number=0;          //保存参数次数
uint8_t present_id=0;
uint8_t last_water_day_record=1;
int i;
uint16_t last_water_filed_record=0;
char OGB_sign=0;                      //进入后台标志位
char time_begin_standby=0;
char data_save=0;                   //data_save为 1时进行一次数据存储
char lack_water=0;
char water_mode=0;                        //water_mode为1是浸种模式下的浇水，为2是点击浇水
char data_save_over=0;                   //control water 函数结束标志，，，浇水完成需要记录的flow可能不为零，故需要手动清零。
char soak_mode=0;                      //用于判断后来的1500ml浇完 之后跳转主界面
int old_statue=-1;
int handle=1;                    //开启页面实时转跳
int minute_set;
int hour_set;
int Residue_Water_Statue=0;
int mn=1,nm=0;
//#define Residue_Water              1000     //过水位计剩余水量
//int32_t current_water=0;         //当前水量
//int today_is_over=1;             //每日浇水完成标志
//int Parameter_changed=0;          //参数改变标志位
long reserve_water=0;
//#define delay(x)         while(x--)
//*****************************************************************************
//
//! 任务初始化函数
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void task_init(void)
{
	   long lRetVal = -1;
	//    Semaphore_Params Print_par;
	//    Error_Block eb;
	//    Semaphore_Params TCP_Buffer_Par;											//TCP缓存保护信号量的参数申请
	//    Semaphore_Params TCP_Client_Par;											//TCP缓存保护信号量的参数申请
	 //   Semaphore_Params UART_SENT_Par;
	#ifdef  PROTO_BUFFER
	#else
	    Semaphore_Params TCP_UART_Compose_Par;
	#endif
	//    Semaphore_Params UART_Par;
	    Error_Block eb;
	    Error_init(&eb);
	#ifdef  PROTO_BUFFER
	#else
	     Semaphore_Params_init(&TCP_UART_Compose_Par);
	    TCP_UART_Compose_Par.mode = Semaphore_Mode_BINARY;
	    TCP_UART_Compose_Pro = Semaphore_create(1, &TCP_UART_Compose_Par, &eb);
	#endif
	//    Semaphore_Params_init(&TCP_Buffer_Par);
	//    TCP_Buffer_Par.mode = Semaphore_Mode_BINARY;
	//    TCP_Buffer_Pro = Semaphore_create(1, &TCP_Buffer_Par, &eb);					//创造TCP缓存保护信号量
	//
	//    Semaphore_Params_init(&TCP_Client_Par);
	//    TCP_Client_Par.mode = Semaphore_Mode_BINARY_PRIORITY;
	//    TCP_Client_Pro = Semaphore_create(1, &TCP_Client_Par, &eb);					//创造TCP缓存保护信号量
//
//	    Semaphore_Params_init(&UART_SENT_Par);
//	    UART_SENT_Par.mode = Semaphore_Mode_BINARY_PRIORITY;
//	    UART_SENT_Pro = Semaphore_create(1, &UART_SENT_Par, &eb);					//创造TCP缓存保护信号量

	//    Semaphore_Params_init(&UART_Par);
	//    UART_Par.mode = Semaphore_Mode_BINARY;
	//    UART_Pro = Semaphore_create(1, &UART_Par, &eb);

	//    MDNS_mail = Mailbox_create(sizeof(MDNS_getdata), 1, NULL, &eb);				//创建mdn信箱用于传递mdns消息
	    UART_mail = Mailbox_create(50,5,NULL,&eb);									//创建串口控制台信箱用于传递串口传来的消息
	    water_mail = Mailbox_create(10,5,NULL,&eb);
	    Control_water_Event=Event_create(NULL,&eb);										//创建SSID扫描事件
	    //Lack_water_Event=Event_create(NULL,&eb);
	//    MDNS_Event=Event_create(NULL,&eb);											//创建MDNS扫描事件
		//
		// Start the SimpleLink Host
		//
		lRetVal = VStartSimpleLinkSpawnTask(SPAWN_TASK_PRIORITY);										//创建simplink任务
		if (lRetVal < 0)
		{
			ERR_PRINT(lRetVal);
	//		LOOP_FOREVER();
		}
		//
		// Start the WlanStationMode task
		//
		lRetVal = osi_TaskCreate(WlanStationMode,(const signed char*) "Wlan Station Task",
								OSI_STACK_SIZE, NULL, 3, NULL);											//调试任务的创建
		if (lRetVal < 0)
		{
			ERR_PRINT(lRetVal);
		}
//		lRetVal = osi_TaskCreate(task_scan_ssid,(const signed char*) "task_scan_ssid",
//				2048, NULL, 2, NULL);															//创建SSID扫描任务
//		if (lRetVal < 0)
//		{
//			ERR_PRINT(lRetVal);
//		}
		lRetVal = osi_TaskCreate(task_Control_Tem_color, (const signed char*) "task_Control_Tem_color",
				1024, NULL,3, NULL);																	//创建温度控制任务
		if (lRetVal < 0)
		{
			ERR_PRINT(lRetVal);
	//		LOOP_FOREVER();
		}
		lRetVal = osi_TaskCreate(task_uart_server,(const signed char*) "task_uart_server",
				OSI_STACK_SIZE, NULL, 6, NULL);																	//创建串口控制台任务
		if (lRetVal < 0)
		{
			ERR_PRINT(lRetVal);
		}
		lRetVal = osi_TaskCreate(task_Control_Water,(const signed char*) "task_Control_Water",                 //浇水任务
				OSI_STACK_SIZE, NULL, 3, NULL);
		if (lRetVal < 0)
		{
			ERR_PRINT(lRetVal);
		}
		lRetVal = osi_TaskCreate(task_updata_time, (const signed char*) "task_updata_time",
				1024, NULL, 3, NULL);																	//更新系统时间
		if (lRetVal < 0)
		{
			ERR_PRINT(lRetVal);
	//		LOOP_FOREVER();
		}
		lRetVal = osi_TaskCreate(task_time_control, (const signed char*) "task_time_control",
				1024, NULL, 3, NULL);																	//时间控制台任务
		if (lRetVal < 0)
		{
			ERR_PRINT(lRetVal);
	//		LOOP_FOREVER();
		}
		lRetVal = osi_TaskCreate(task_timer_delay, (const signed char*) "task_timer_delay",
				1024, NULL, 8, NULL);																	//系统定时任务
		if (lRetVal < 0)
		{
			ERR_PRINT(lRetVal);
	//		LOOP_FOREVER();
		}
		lRetVal = osi_TaskCreate(task_real_time_skip, (const signed char*) "task_real_time_skip",
				1024, NULL, 6, NULL);																	//系统实时跳转
		if (lRetVal < 0)
		{
			ERR_PRINT(lRetVal);
	//		LOOP_FOREVER();
		}
//		lRetVal = osi_TaskCreate(task_Standby, (const signed char*) "task_Standby",
//				1024, NULL, 3, NULL);																	//系统待机任务
//		if (lRetVal < 0)
//		{
//			ERR_PRINT(lRetVal);
//	//		LOOP_FOREVER();
//		}
//		lRetVal = osi_TaskCreate(task_blink, (const signed char*) "task_blink",
//				1024, NULL, 2, NULL);																	//系统待机任务
//		if (lRetVal < 0)
//		{
//			ERR_PRINT(lRetVal);
//	//		LOOP_FOREVER();
//		}
		lRetVal = osi_TaskCreate(task_run_statue, (const signed char*) "task_run_statue",
				1024, NULL, 8, NULL);																	//系统待机任务
		if (lRetVal < 0)
		{
			ERR_PRINT(lRetVal);
	//		LOOP_FOREVER();
		}
		lRetVal = osi_TaskCreate(task_Parameters_print, (const signed char*) "task_Parameters_print",
				1024, NULL, 3, NULL);																	//系统待机任务
		if (lRetVal < 0)
		{
			ERR_PRINT(lRetVal);
	//		LOOP_FOREVER();
		}
//		lRetVal = osi_TaskCreate(task_file_save, (const signed char*) "task_file_save",
//				1024, NULL, 2, NULL);																	//系统待机任务
//		if (lRetVal < 0)
//		{
//			ERR_PRINT(lRetVal);
//	//		LOOP_FOREVER();
//		}
	//	lRetVal = osi_TaskCreate(task_start_link,(const signed char*) "task_start_link",
	//							OSI_STACK_SIZE, NULL, 2, (OsiTaskHandle )&task_start_linik_handle);											//创建设备刚启动时网络连接程序
	//	if (lRetVal < 0)
	//	{
	//		ERR_PRINT(lRetVal);
	////		LOOP_FOREVER();
	//	}												//时钟软中断设置暂时无用
}
//*****************************************************************************
//
//! Screen protect 触发任务
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
//void task_blink(void *pvParameters)
//{
//    //long lRetVal = -1;
//	//int i=0;
//	while(1)
//	{
		//UART_PRINT("blink....");
//		if(plant_statue==1)
//		{
//			if(GPIO_IF_Get(13)==0)             //等于0为缺水
//			{
//				UART_PRINT("short water \r\n");
//				UART_PRINT("Residue_Water is %d",Residue_Water);			   //将水位计以下的水排干净
//				Residue_Water_Statue=1;
//				controlrule.control_water_frequency=10;                                //传入水量参数
//				controlrule.control_water_yield=Residue_Water;
//				UART_PRINT("this time water_yeild is %d",Residue_Water);
//				osi_Sleep(1000);
//			}
//			if(lack_water==1&&GPIO_IF_Get(13)==1)
//			{
//				UART_PRINT("laishui le ");
//				time_begin_standby=1;       //开启屏保
//				lack_water=0;
//				send_commmand(skip,57);
//			}
//		}
		//send_commmand(skip,7);
//		if(GPIO_IF_Get(13)==0)
//		{
//			UART_PRINT("13 is 0");
//		}
//  if(GPIO_IF_Get(13)==1)
//  {
//	send_commmand(skip,67);  //开启闪烁
//	//lRetVal = osi_LockObjLock(*task_Control_Water,OSI_WAIT_FOREVER);																	//创建温度控制任务
//	if (lRetVal < 0)
//	{
//		ERR_PRINT(lRetVal);
////		LOOP_FOREVER();
//	}
//    //UART_PRINT("water shortage。。。\r\n");
//    }
//    else
//    send_commmand(skip,0x10);  //关闭闪烁
//	osi_Sleep(100);
//	}
//}
//*****************************************************************************
//
//! 文件保存操作
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void task_file_save(void *pvParameters)
{
	long lRetVal;
	//long reserve_water=0;
	while(1)
	{
		osi_Sleep(100);
		if(data_save==1||data_save_over==1)
		{
			filebuf[0]=(char)Parameter_set.lamp_on[0];
			filebuf[1]=(char)Parameter_set.lamp_on[1];
			filebuf[2]=(char)Parameter_set.lamp_close[0];
			filebuf[3]=(char)Parameter_set.lamp_close[1];
			filebuf[4]=(char)Parameter_set.water_yeild;                   //低8位
			filebuf[5]=(char)(Parameter_set.water_yeild>>8);     ///高8位
			filebuf[6]=(char)Parameter_set.water_interval;
			filebuf[7]=(char)Parameter_set.water_on[0];
			filebuf[8]=(char)Parameter_set.water_on[1];
			filebuf[9]=(char)Parameter_set.Temperature;
			filebuf[10]=(char)Parameter_set.humidity;
			filebuf[11]=(char)plan_date;
			filebuf[12]=(char)save_data_chang;
			filebuf[13]=(char)(flow_count>>8);
			filebuf[14]=(char)flow_count;
			if(data_save_over==1)
			{
				filebuf[13]=0;
				filebuf[14]=0;
			}
			filebuf[15]=(char)water_mode;
			//filebuf[16]=(char)today_is_over;
			//UART_PRINT("cunwenjian zhong flow_count is %d",flow_count);
//			UART_PRINT("储存：流出了%fml的水\r\n",flow_count/FLOW_CHANGE);
//			UART_PRINT("filebuf13 is %d,filebuf14 is %d",filebuf[13],filebuf[14]);
			//UART_PRINT("存入的毫升数是%d\r\n",(filebuf[13]*256+filebuf[14]));
			//UART_PRINT("save task begin\r\n");
			lRetVal=writekeepfile(RESTORE_FILE,filebuf);
			if (lRetVal < 0)
			{
				ERR_PRINT(lRetVal);
			}
			data_save=0;
			UART_PRINT("save complete\r\n");
			data_save_over=0;
//			UART_PRINT("read begin\r\n");
//			lRetVal=readkeepfile(RESTORE_FILE,test);
//			for(i=0;i<12;i++)
//				UART_PRINT("test[%d]=%d",i,test[i]);
//			UART_PRINT("Read complete\r\n");
/*			osi_Sleep(20*1000);*/
		}
	}
}
//*****************************************************************************
//
//! Screen protect 触发任务
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
//void task_Standby(void *pvParameters)
//{
//	while(1)
//	{
////		UART_PRINT("in the BGN");
////		lRetVal=tftp_getfile(FileRead,GEI_FILE_SIZE_MAX);
////		if(lRetVal < 0)
////		{
////			ERR_PRINT(lRetVal);
////		}
//		if(receive_data_statue==0)
//		{
//			//UART_PRINT("no data");
//			time_begin_standby=1;
//			//osi_Sleep(1000);
//			receive_data_statue=1;
//		}
//		osi_Sleep(100);
//	}
//}
//*****************************************************************************
//
//! System timer delay
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void task_timer_delay(void *pvParameters)
{
	long lRetVal=-1;
	long total_water=0;
	//char i=0;
	while(1)
	{
		osi_Sleep(100);
	    if(time_begin==1)       //5秒
	     {
	    	TIME_15min_lamp=min_15;
			 time_begin=0;
	     }
	    if(time_begin==2)
	    {
	    	unlock_key_delay=unlock_key_delay_set;
	    	time_begin=0;
	    }
	    if(unlock_key_delay==0)
	    {
	    	send_commmand(skip,93);       //按键延时时间到，跳转到解锁开始界面
	    	unlock_key_delay=-1;
	    }
	    if(time_begin==3)
	    {
	    	//UART_PRINT("TIME_15min_fan 已传入");
	    	TIME_15min_fan=min_15;
	    	time_begin=0;
	    }
	    if(time_begin_standby==1)
	    {
	    	//UART_PRINT("standby   已传入");
	    	standby=screen_protect_await_time;                //不操作进入待机界面时间
	    	time_begin_standby=0;
	    }
	    if(time_begin==6)                                //延时2秒钟
	    {
	    	//UART_PRINT("standby 已传入");
	    	watering_UI_delay=watering_UI_delay_set;
	    	time_begin=0;
	    }
	    if(adjust_time_delay==0)
	    {
	    //	UART_PRINT("shijian dao \r\n");
	    	//UART_PRINT("hour_set is %d",hour_set);
	    	send_commmand(skip,68);
	    	BGO_delay=BGO_delay_set;
	    	//UART_PRINT("BGO is %d",BGO_delay);
	    	adjust_time_delay=-1;
	    }
//	    UART_PRINT("BGO is %d",BGO_delay);
//	//    if(time_begin==7)               //设置时间延时
//	    {
//	    	UART_PRINT("time adjust delay\r\n");
//	    	adjust_time_delay=adjust_time_delay_set;
//	    	time_begin=0;
//	    	//send_commmand(skip,1);
//	    }
	    if(BGO_delay==0)     //后台操作等待时间
	    {
	    	//send_commmand(skip,1);
			//UART_PRINT("read begin\r\n");
			lRetVal=readkeepfile(RESTORE_FILE,test);
			if (lRetVal < 0)         //无文件
			{
				UART_PRINT("no file\r\n");
				send_commmand(skip,61);
			}
			else                    //有文件
			{
//				for(i=0;i<15;i++)
//					UART_PRINT("test[%d]=%d\r\n",i,test[i]);
//				UART_PRINT("Read complete\r\n");
				Parameter_set.lamp_on[0]=test[0];
				Parameter_set.lamp_on[1]=test[1];
				Parameter_set.lamp_close[0]=test[2];
				Parameter_set.lamp_close[1]=test[3];
				Parameter_set.water_yeild=test[4];                   //低8位
				Parameter_set.water_yeild|=test[5]<<8;     ///高8位
				Parameter_set.water_interval=test[6];
				Parameter_set.water_on[0]=test[7];
				Parameter_set.water_on[1]=test[8];
				Parameter_set.Temperature=test[9];
				Parameter_set.humidity=test[10];
				plan_date=test[11];
				save_data_chang=test[12];
				//UART_PRINT("test13 is %d,test14 is %d",test[13],test[14]);
				reserve_water=(test[13]*256+test[14]);
				water_mode=test[15];
				//today_is_over=test[16];
	    		send_commmand(lamp_on_h,Parameter_set.lamp_on[0]);
	    		send_commmand(lamp_on_min,Parameter_set.lamp_on[1]);
	    		send_commmand(lamp_close_h,Parameter_set.lamp_close[0]);
	    		send_commmand(lamp_clode_min,Parameter_set.lamp_close[1]);
	    		send_commmand(water_yeild_set,Parameter_set.water_yeild);     //水量
	    		send_commmand(water_interval_set,Parameter_set.water_interval);        //浇水间隔
	    		send_commmand(water_on_h,Parameter_set.water_on[0]);
	    		send_commmand(water_on_min,Parameter_set.water_on[1]);
	    		send_commmand(tem_set,Parameter_set.Temperature);
	    		send_commmand(humi_set,Parameter_set.humidity);
				UART_PRINT("读出的flow_count  is %d\r\n",reserve_water);
			//	UART_PRINT("上次浇水量is%f,本次还需浇水%f",reserve_water/FLOW_CHANGE,((2000*FLOW_CHANGE)-reserve_water)/FLOW_CHANGE);
				if(reserve_water!=0)
				{
					if(water_mode==1)
						total_water=soak_mode_yeild;
					else
						total_water=handle_water_yield;
					UART_PRINT("还需浇水 is %dml",(uint16_t)((total_water*FLOW_CHANGE)-reserve_water)/FLOW_CHANGE);
					controlrule.control_water_frequency=10;                                //传入水量参数
					controlrule.control_water_yield=(uint16_t)(((total_water*FLOW_CHANGE)-reserve_water)/FLOW_CHANGE);
					Event_post(Control_water_Event,Event_Id_01);
				}
				if(save_data_chang==1)
			    external_statue |= 0x08;
				plant_statue=1;             //开启时间控制
				handle=1;
				page_skip(external_statue);
			}
	    	time_get+=(unsigned int)hour_set*60*60+minute_set*60;
	    	//UART_PRINT("%d:%d",hour_set,minute_set);
	    	BGO_delay=-1;
	    }
	    if(watering_UI_delay==0)
	    {
	    	time_control_water_statue=1;
	    	page_skip(external_statue);
			watering_UI_delay=-1;
	    }
	    if(TIME_15min_lamp==0)
		 {
				time_control_lamp_statue=1;
				TIME_15min_lamp=-1;
		 }
	    if(TIME_15min_fan==0)
	     {
				//UART_PRINT("fan dingshi complete");
				time_control_fan_statue=1;
				TIME_15min_fan=-1;
	     }
	    if(standby==0)
	     {
				//UART_PRINT("standby dingshi complete");
	    	    send_commmand(read_id,0x02);
	    	    osi_Sleep(500);
				send_commmand(skip,80);        //跳转到解锁页面
#ifdef DEBUG
				get_real_time(time_get+3600*RTC_IF_GetValue().Secs, GMT_DIFF_TIME_HRS, GMT_DIFF_TIME_MINS);//获取真正的系统时间
#else
				get_real_time(time_get+RTC_IF_GetValue().Secs, GMT_DIFF_TIME_HRS, GMT_DIFF_TIME_MINS);//获取真正的系统时间
#endif
				send_commmand(time_h,time_axis.hour);
				send_commmand(time_m_shi,(time_axis.minute/10));
			    send_commmand(time_m_ge,(time_axis.minute%10));
				standby=-1;
				standby_statue=1;
	     }
	    if(time_begin==5)                   //浸泡时间
	    {
	    //	UART_PRINT("soak_time 已传入");
	    	fifty_minute=fifty_minute_set;
	    	one_hour=one_hour_time_set;
	    	time_begin=0;
	    }
	    if(one_hour==0)
	    {
		    plant_statue=1;                              //开时间控制台
		    handle=1;                                    //开启界面实时跳转
		    //flag_watering_stop=0;                        //取消删除水量任务
//			    osi_Sleep(500);                           //等待参数传递完成
        	Parameter_set.lamp_on[0]=save_Parameter[0];
        	Parameter_set.lamp_on[1]=save_Parameter[1];
        	Parameter_set.lamp_close[0]=save_Parameter[2];
        	Parameter_set.lamp_close[1]=save_Parameter[3];
        	Parameter_set.water_yeild=save_Parameter[4];
        	Parameter_set.water_interval=save_Parameter[5];
        	Parameter_set.water_on[0]=save_Parameter[6];
        	Parameter_set.water_on[1]=save_Parameter[7];
        	Parameter_set.Temperature=save_Parameter[8];
        	Parameter_set.humidity=save_Parameter[9];
    		send_commmand(lamp_on_h,Parameter_set.lamp_on[0]);
    		send_commmand(lamp_on_min,Parameter_set.lamp_on[1]);
    		send_commmand(lamp_close_h,Parameter_set.lamp_close[0]);
    		send_commmand(lamp_clode_min,Parameter_set.lamp_close[1]);
    		send_commmand(water_yeild_set,Parameter_set.water_yeild);     //水量
    		send_commmand(water_interval_set,Parameter_set.water_interval);        //浇水间隔
    		send_commmand(water_on_h,Parameter_set.water_on[0]);
    		send_commmand(water_on_min,Parameter_set.water_on[1]);
    		send_commmand(tem_set,Parameter_set.Temperature);
    		send_commmand(humi_set,Parameter_set.humidity);
    		//UART_PRINT("standby_statue is %d",standby_statue);
    		if(standby_statue==0)           //没有屏保才进行跳转
    		{
    			page_skip(external_statue);
    			time_begin_standby=1;          //重新计进入屏保的时间
    		}
    		else                           //有屏保时，不跳转，把解锁后跳转的界面改为主界面。
    		{
    			present_id=33;
    		}
			one_hour=-1;
	    }
	    if(fifty_minute==0)
	    {
			controlrule.control_water_frequency=10;                                //50分钟后进行又一次浇水
			controlrule.control_water_yield=soak_mode_yeild;
			Event_post(Control_water_Event,Event_Id_01);
			fifty_minute=-1;
	    }
	}
}
////*****************************************************************************
//
//! 主界面 real-time skip
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void task_real_time_skip(void *pvParameters)
{
  while(1)
  {
	  if((external_statue!=old_statue)&&(plant_statue==1)&&(handle==1)&&(standby_statue==0))
	  {
		  old_statue=external_statue;
		  page_skip(external_statue);
      }
	  osi_Sleep(100);                                          //等待按键页面跳转完成，然后再去读取界面
  }
}
////*****************************************************************************
//
//! Application startup display on UART
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
//void task_test2(void *pvParameters)
//{
////	char *a="123456";
//	long lRetVal=-1;
//	MDNS_getdata getmessage={0,0,0,""};
//	uint16_t sleep_time=0;
//#ifdef  PROTO_BUFFER
//	DATA IP_DATA=DATA_init_default;
//	com_ip_sent ip_sent={"","",0};
//	char mac[18]="";
//#endif
//	while(1)
//	{
////		Mailbox_post(Print_mail, a, BIOS_WAIT_FOREVER );
//		Mailbox_pend (MDNS_mail,&getmessage,BIOS_WAIT_FOREVER);
//
//		Report("\n\rPort: %d\n\r", getmessage.port);
//		Report("Addr: %d.%d.%d.%d\n\r", SL_IPV4_BYTE(getmessage.addr, 3),
//				SL_IPV4_BYTE(getmessage.addr, 2), SL_IPV4_BYTE(getmessage.addr, 1),
//				SL_IPV4_BYTE(getmessage.addr, 0));
//		Report("TLen1=%d\r\n", getmessage.TextLen);
//		Report("Text: %s\n\r", getmessage.Text);
//
//
//		unsigned char len = sizeof(SlNetCfgIpV4Args_t);
//		unsigned char ucDHCP;
//		SlNetCfgIpV4Args_t ipV4 = {0};
//
//		// get network configuration
//		sl_NetCfgGet(SL_IPV4_STA_P2P_CL_GET_INFO, &ucDHCP, &len,
//					(unsigned char *) &ipV4);	//获取client的ip地址，网管地址，是否为DHCP
//		UART_PRINT(" IP Acquired: IP=%d.%d.%d.%d , "
//		            "Gateway=%d.%d.%d.%d\n\r",
//		            SL_IPV4_BYTE(ipV4.ipV4,3),
//		            SL_IPV4_BYTE(ipV4.ipV4,2),
//		            SL_IPV4_BYTE(ipV4.ipV4,1),
//		            SL_IPV4_BYTE(ipV4.ipV4,0),
//		            SL_IPV4_BYTE(ipV4.ipV4Gateway,3),
//		            SL_IPV4_BYTE(ipV4.ipV4Gateway,2),
//		            SL_IPV4_BYTE(ipV4.ipV4Gateway,1),
//		            SL_IPV4_BYTE(ipV4.ipV4Gateway,0));		//串口打印ip地址和网关地址
//		g_ulDestinationIp=getmessage.addr;
//		g_ulDestinationPort=getmessage.port;
//		Semaphore_pend(TCP_Buffer_Pro,100);
//		g_ulPacketCount=1;
//		memset(g_cBsdBuf,'\0',BUF_SIZE);
//#ifdef  PROTO_BUFFER
////		ip_sent={"","",0};
//		sprintf(ip_sent.ip,"%d.%d.%d.%d",
//				 SL_IPV4_BYTE(ipV4.ipV4,3),
//				 SL_IPV4_BYTE(ipV4.ipV4,2),
//				 SL_IPV4_BYTE(ipV4.ipV4,1),
//				 SL_IPV4_BYTE(ipV4.ipV4,0));
//		sprintf(ip_sent.port,"%d",PORT_NUM);
//		sprintf(ip_sent.state,"%d",Current_Mode);
//		sprintf(mac,"%x:%x:%x:%x:%x:%x",macAddressVal[0],macAddressVal[1],macAddressVal[2],\
//											  macAddressVal[3],macAddressVal[4],macAddressVal[5]);
////		UART_PRINT("mac=%s\r\n",mac);
////		ip_sent.state=Current_Mode;
//		IP_DATA.DATA1=ip_sent.ip;
//		IP_DATA.DATA2=ip_sent.port;
////		IP_DATA.DATA3=(char *)(&(ip_sent.state));
//		IP_DATA.DATA3=ip_sent.state;
//		IP_DATA.DATA4=mac;
//		IP_DATA.DATA5=pcSsidName;
//		IP_DATA.DATA6=NULL;
//		sleep_time=(macAddressVal[4]*256+macAddressVal[5])/100;
//		UART_PRINT("*******sleep_time=%d*********\r\n",sleep_time);
//		osi_Sleep(sleep_time);
//		lRetVal= sent_tcp_command(g_cBsdBuf,sizeof(g_cBsdBuf),"SEND_IP",MOD_SEND_IP,1,IP_DATA);
//		if(lRetVal<=0)
//			ERR_PRINT(lRetVal);
////		IP_DATA=DATA_init_default;
//#else
//		sprintf(g_cBsdBuf,"CUST,TCP,2,1,0,0,0,0,%d.%d.%d.%d,%d,%d,0,~\r\n",
//						 SL_IPV4_BYTE(ipV4.ipV4,3),
//						 SL_IPV4_BYTE(ipV4.ipV4,2),
//						 SL_IPV4_BYTE(ipV4.ipV4,1),
//						 SL_IPV4_BYTE(ipV4.ipV4,0),
//						 PORT_NUM,Current_Mode);
//#endif
//		Semaphore_post(TCP_Buffer_Pro);
//		Report("\r\n%s\r\n",g_cBsdBuf);
//		BsdTcpClient(g_ulDestinationPort);
//	}
//}
//*****************************************************************************
//
//! mdns客户端搜寻服务任务
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
//void task_mdnsget(void *pvParameters)
//{
//	MDNS_getdata getmessage={0,0,0,""};
//	while(1)
//	{
//		MDNS_broadcast(getmessage,10);						//搜寻存在的mdns任务
//	}
//}
//*****************************************************************************
//
//! TCP服务器任务
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
//void task_tcp_server(void *pvParameters)
//{
//#ifdef PROTO_BUFFER
//	long lRetVal=-1;
//#else
//	Bool reval=0;
//	char receive_data[20][32]={'\0'};
//#endif
////	uint8_t i=0;
//	while(1)
//	{
//		if(IS_CONNECTED(g_ulStatus)&&IS_IP_ACQUIRED(g_ulStatus))					//判断当前是否连接，并且获取了IP地址
//		{
//			lRetVal=BsdTcpServer(PORT_NUM);										//开启服务器监听一次，建立短连接，接受完就关闭，否则一直等待接收数据
//#ifdef  PROTO_BUFFER
//			if(lRetVal==SUCCESS)
//			{
////				lRetVal=gettcpcommand_dispose(g_cBsdBuf,sizeof(g_cBsdBuf));				//反序列化获取到的指令，解析指令，发送命令
////				if(lRetVal<=0)
////					ERR_PRINT(lRetVal);
////				osi_Sleep(100);
////				BsdTcpClient(g_ulDestinationPort);								//TCP客户端发送数据
//			}
//#else
//			reval=Semaphore_pend(TCP_UART_Compose_Pro,100);
//			if(reval==FALSE )
//			{
//				UART_PRINT("TCP_UART_Compose_Pro LOCK in tcp server!!!\r\n");
//				Data_Dispose(g_cBsdBuf,receive_data);
//				task_commond_Send(receive_data);
//				Semaphore_post(TCP_UART_Compose_Pro);
//			}
//			else
//			{
//				Data_Dispose(g_cBsdBuf,receive_data);
//				task_commond_Send(receive_data);
//				Semaphore_post(TCP_UART_Compose_Pro);
//			}
//#endif
////			for(i=0;i<20;i++)
////				UART_PRINT("i=%d,%s\r\n",i,receive_data[i]);
//		}
//		osi_Sleep(100);
////		flag_mode_change=0;
//
//	}
//}
//*****************************************************************************
//
//! 串口控制台任务
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
/*************************************
 * external_statue       风          水           灯
 *       值                                                   0   0    0
 *                       默认值
 ***********************************/
void task_uart_server(void *pvParameters)
{
	uint8_t receive_data[50]={0};  uint8_t Temp_lamp_on_h=8; uint8_t Temp_lamp_on_m=0;  uint8_t Temp_lamp_close_h=20;
    uint8_t Temp_lamp_close_m=0;   uint8_t Temp_water_interval=3;
	uint8_t Temp_water_on_h=8; uint8_t Temp_water_on_m=0;
	uint8_t Temp_tem=25;  uint8_t Temp_humi=75;uint8_t water_select[10]={0};
	char right=0;
	int i=0;
	char m=0,n=0;
	long lRetVal=-1;
	unsigned long token1=0;
	error error_statue=statue_ok;
	while(1)
	{
		Mailbox_pend (UART_mail,receive_data,BIOS_WAIT_FOREVER);				//等待串口中断发送邮件，maibox最大的长度为50，最多可存储5条指令
		for(i=1;i<receive_data[0]+1;i++)
		{
			UART_PRINT("%x ",receive_data[i]);
		}
		UART_PRINT("\r\n");
		if(receive_data[4]==0x81)                           //发送读取当前页面指令后       读取当前页面ID
		{
			present_id=receive_data[8];                     //接收到屏幕ID时不进入计时   只有按下屏幕发下来的指令才计时
			if((present_id==0x5a)||(present_id==0x50)||present_id>92)      //此处是因为有时候读取页面ID值不正确，防止错误（读取ID值错误的原因可能是因为在屏幕往寄存器里写数据的时候你去读，导致读出错，原因尚不明确）
			{
				present_id=33;
				error_statue=read_id_error;
				printf("error_statue is %d",error_statue);
			}
			UART_PRINT("存入的页面是 %d",present_id);
		}
		else if(receive_data[4]==0x83)
		{
			time_begin_standby=1;                               //刷新接收状态
			switch(receive_data[6]){
            case 0x01:
            	                controlrule.control_water_frequency=10;                                //传入水量参数
								controlrule.control_water_yield=soak_mode_yeild;
								water_mode=1;
								UART_PRINT("flag_watering_stop is %d",flag_watering_stop);
								Event_post(Control_water_Event,Event_Id_01);
								time_begin=5;                                   //浸泡mode 操作在中断中
								break;
            case 0x41:                                                          //水量设置界面参数
            	        switch(receive_data[9]){
						case 0x00:
										send_commmand(water_yeild_set,0);
										send_commmand(water_display,0);
										Temp_water_yeild=0;
										break;
						case 0x01:
										send_commmand(water_yeild_set,600);
										send_commmand(water_display,600);
										Temp_water_yeild=600;
										break;
						case 0x02:
										send_commmand(water_yeild_set,800);
										send_commmand(water_display,800);
										Temp_water_yeild=800;
										break;
						case 0x03:
										send_commmand(water_yeild_set,1200);
										send_commmand(water_display,1200);
										Temp_water_yeild=1200;
										break;
						case 0x04:
										send_commmand(water_yeild_set,1600);
										send_commmand(water_display,1600);
										Temp_water_yeild=1600;
										break;
						case 0x05:
										send_commmand(water_yeild_set,2000);
										send_commmand(water_display,2000);
										Temp_water_yeild=2000;
										break;
		            	default:
		            		 error_statue=receive_command_error;
		            		 printf("error_statue is %d",error_statue);
		            		 break;
            	        }
            	        break;
            case 0x04:                                   //按键解锁控制
            	        switch(receive_data[9]){
            	        case 0x01:
            	        	time_begin=2;  break;
            	        case 0x02:
            	        	time_begin=2;  break;
            	        case 0x03:
            	            time_begin=2;  break;
            	        case 0x04:
							standby_statue=0;
							unlock_key_delay=-1;    //停止上一次的
							UART_PRINT("present_id is %d",present_id);
							osi_Sleep(500);
							if(present_id<=34)                   //跳转至主界面
								page_skip(external_statue);
							else
								send_commmand(skip,present_id);  break;
		            	default:
		            		 error_statue=receive_command_error;
		            		 printf("error_statue is %d",error_statue);
		            		 break;
            	        }
            case 0x03:
            	        switch(receive_data[9]){
            	        case 0x11:                                                 //照明界面返回选择界面
										if(save_data_chang==1)
										{
											handle=1;
											page_skip(external_statue);
										}
										else
											send_commmand(skip,35);
										break;
            	        case 0x06:                                                 //主界面动手达人选择界面
            	        	            handle=0;
										if(save_data_chang==1)
											send_commmand(skip,37);
										else
											send_commmand(skip,35);
										break;
            	        case 0x34:                                                 //开始界面，时间确定，保存修改时间
            	        	            adjust_time_delay=0;
            	        	            break;
            	        case 0x3a:                                                 //主界面调整时间按钮
								#ifdef DEBUG
												get_real_time(time_get+3600*RTC_IF_GetValue().Secs, GMT_DIFF_TIME_HRS, GMT_DIFF_TIME_MINS);//获取真正的系统时间
								#else
												get_real_time(time_get+RTC_IF_GetValue().Secs, GMT_DIFF_TIME_HRS, GMT_DIFF_TIME_MINS);//获取真正的系统时间
								#endif
									    handle=0;        //调整时间时关闭转跳
										hour_set=time_axis.hour;
										minute_set=time_axis.minute;
										send_commmand(time_h_set,time_axis.hour);
										send_commmand(time_m_shi_set,(time_axis.minute/10));
										send_commmand(time_m_ge_set,(time_axis.minute%10));
										send_commmand(skip,73);
										break;
            	        case 0x26:                                                 //抹除数据进入标准模式
										external_statue &= 0xf7;
										data_chang=0;       //延迟状态改变   等待下一次浇水的时候，再把data_save_chang变为0
										handle=1;
										plant_statue=1;
										mn=1,m=0;
										page_skip(external_statue);
										Parameter_set.lamp_on[0]=save_Parameter[0];
										Parameter_set.lamp_on[1]=save_Parameter[1];
										Parameter_set.lamp_close[0]=save_Parameter[2];
										Parameter_set.lamp_close[1]=save_Parameter[3];
										Parameter_set.water_yeild=save_Parameter[4];
										Parameter_set.water_interval=save_Parameter[5];
										Parameter_set.water_on[0]=save_Parameter[6];
										Parameter_set.water_on[1]=save_Parameter[7];
										Parameter_set.Temperature=save_Parameter[8];
										send_commmand(lamp_on_h,Parameter_set.lamp_on[0]);
										send_commmand(lamp_on_min,Parameter_set.lamp_on[1]);
										send_commmand(lamp_close_h,Parameter_set.lamp_close[0]);
										send_commmand(lamp_clode_min,Parameter_set.lamp_close[1]);
										send_commmand(water_yeild_set,Parameter_set.water_yeild);     //水量
										send_commmand(water_interval_set,Parameter_set.water_interval);        //浇水间隔
										send_commmand(water_on_h,Parameter_set.water_on[0]);
										send_commmand(water_on_min,Parameter_set.water_on[1]);
										send_commmand(tem_set,Parameter_set.Temperature);
										send_commmand(humi_set,Parameter_set.humidity);
										Parameter_set.humidity=save_Parameter[9];
										break;
            	        case 0x29:                                                 //不抹除数据进入标准模式
            	        	            page_skip(external_statue);
            	        	            break;
            	        case 0x25:                                                 //恢复到一次全新的种植
										lRetVal=sl_FsDel((unsigned char *)RESTORE_FILE,token1);
										if(lRetVal<=0)
										ERR_PRINT(lRetVal);
										else
											UART_PRINT("del ok");
										plan_date=1;
										next_water_date=6;
										plant_day=1;
										mn=1,nm=0;
										last_water_date=1;
										time_control_lamp_statue=1;
										time_control_fan_statue=1;
										time_control_water_statue=1;
					        			//water_select[0]=2;
					        			//Mailbox_post(water_mail,water_select,0);///传消息给浇水，浇水结束
										//today_is_over=0;
										handle=0;
//										if((external_statue&0x02)==0)
//											GPIO_IF_Set(5, 0);
//										else
											flag_watering_stop=1;              //删除任务
											osi_Sleep(500);
											flag_watering_stop=0;
										GPIO_IF_Set(11, 0);
										GPIO_IF_Set(10, 0);
										GPIO_IF_Set(12, 0);
										Timer_IF_SetDutyCycle(FAN,0);
										save_data_chang=0;
										Water_period=0;
										external_statue &= 0xf7;           //回到自动模式
										plant_statue=0;
										break;
            	        case 0x28:                                          //复位返回
										page_skip(external_statue);
										handle=1;
										break;
            	        case 0x30:                           //按下开水的时候关跳转
            	        	            handle=0;
            	        	            break;
            	        case 0x31:
										page_skip(external_statue);
            	        	            handle=1;
										break;
            	        case 0x33:
										page_skip(external_statue);
            	        	            handle=1;
										break;
            	        case 0x32:                                                          //待机后界面跳转到解锁界面
            	        	            send_commmand(skip,93);
										break;
            	        case 0x08:
										time_control_lamp_statue=0;
										GPIO_IF_Set(10, 1);
										GPIO_IF_Set(11, 1);
										GPIO_IF_Set(12, 1);                       //GPIO12 lamp1
										//GPIO_IF_Set(10, 1);                       //GPIO10 lamp3
										external_statue |= 0x01;
										page_skip(external_statue);
										time_begin=1;                 //屏蔽时间台灯光15分钟
										break;
            	        case 0x0d:
										UART_PRINT("您按了关灯");
										time_control_lamp_statue=0;
										GPIO_IF_Set(10, 0);
										GPIO_IF_Set(11, 0);
										GPIO_IF_Set(12, 0);                       //GPIO12 lamp1
										external_statue &=0x0e;
										page_skip(external_statue);
										time_begin=1;
										break;
            	        case 0x09:
            	        	            handle=0;
            	        	            break;
            	        case 0x35:
										UART_PRINT("您按了开水");
										external_statue |= 0x02;
										page_skip(external_statue);
										time_control_water_statue=0;
										handle=1;
										water_mode=2;
					//					if(today_is_over==1)                //      今天浇过水在浇水   浇水量
					//					{
											//UART_PRINT("浇水日期    is %d",plan_date);
											controlrule.control_water_frequency=10;
											controlrule.control_water_yield=handle_water_yield;
											//UART_PRINT("浇水量  is %dml",handle_water_yield);
											Event_post(Control_water_Event,Event_Id_01);
					//					}
					//					else                               //        今天未浇水  浇水量
					//					{
					//				    today_is_over=1;
					//				    //UART_PRINT("浇水日期    is %d",plan_date);
					//					controlrule.control_water_frequency=10;
					//					controlrule.control_water_yield=handle_non_water_yield;
					//					//UART_PRINT("浇水量  is %dml",handle_non_water_yield);
					//					Event_post(Control_water_Event,Event_Id_01);
					//					}
										while(water_complete==1)
										{
										   time_control_water_statue=1;
										   water_complete=0;
										}
										break;
            	        case 0x0c:
										UART_PRINT("您按了关水");
										time_control_water_statue=0;
										time_begin=6;
					//					UART_PRINT("time_begin is %d",time_begin);
					//					GPIO_IF_Set(5, 0);
					//					flag_watering_stop=1;              //删除任务
					//					external_statue &= 0x0d;
										break;
            	        case 0x0a:
										UART_PRINT("您按了开风扇");                 //开风扇
										time_control_fan_statue=0;
										Timer_IF_SetDutyCycle(FAN,100);
										external_statue |= 0x04;
										page_skip(external_statue);
										time_begin=3;                   //屏蔽时间台风扇15分钟
										break;
            	        case 0x0b:
										UART_PRINT("您按了关风扇");
										time_control_fan_statue=0;
										Timer_IF_SetDutyCycle(FAN,0);
										external_statue &= 0x0b;
										page_skip(external_statue);
										time_begin=3;
										break;
            	        case 0x21:                                                             //z最后数据保存操作
            	        	            if(save_data_chang==1)
										{
											page_skip(external_statue);
										}
            	        	            data_save=1;
										chang_time_number++;
									    data_chang_day=plan_date;       //记录更改数据为哪一天
										UART_PRINT(" data has chang data_chang_day is %d\r\n",data_chang_day);
										Parameter_set.lamp_on[0]=Temp_lamp_on_h;
										Parameter_set.lamp_on[1]=Temp_lamp_on_m;
										Parameter_set.lamp_close[0]=Temp_lamp_close_h;
										Parameter_set.lamp_close[1]=Temp_lamp_close_m;
										Parameter_set.water_yeild=Temp_water_yeild;
									//	UART_PRINT("Parameter_set.water_yeild is %d",Parameter_set.water_yeild);
										Parameter_set.water_interval=Temp_water_interval;
										Parameter_set.water_on[0]=Temp_water_on_h;
										Parameter_set.water_on[1]=Temp_water_on_m;
										Parameter_set.Temperature=Temp_tem;
				//			        	UART_PRINT("Temp_tem is %d",Temp_tem);
				//			        	UART_PRINT("Parameter_set.Temperature is %d",Parameter_set.Temperature);
										Parameter_set.humidity=Temp_humi;
					//					Timer_IF_SetDutyCycle(LAMP2,0);
					//					GPIO_IF_Set(12, 0);
					//					Timer_IF_SetDutyCycle(FAN,0);
										handle=1;
										external_statue |= 0x08;         //进入动手模式
										save_data_chang=1;
										break;
            	        case 0x16:
										page_skip(external_statue);
										handle=1;
										break;
            	        case 0x22:
										UART_PRINT("data not changed");
										Parameter_set.lamp_on[0]=save_Parameter[0];
										Parameter_set.lamp_on[1]=save_Parameter[1];
										Parameter_set.lamp_close[0]=save_Parameter[2];
										Parameter_set.lamp_close[1]=save_Parameter[3];
										Parameter_set.water_yeild=save_Parameter[4];
										Parameter_set.water_interval=save_Parameter[5];
										Parameter_set.water_on[0]=save_Parameter[6];
										Parameter_set.water_on[1]=save_Parameter[7];
										Parameter_set.Temperature=save_Parameter[8];
										send_commmand(lamp_on_h,Parameter_set.lamp_on[0]);
										send_commmand(lamp_on_min,Parameter_set.lamp_on[1]);
										send_commmand(lamp_close_h,Parameter_set.lamp_close[0]);
										send_commmand(lamp_clode_min,Parameter_set.lamp_close[1]);
										send_commmand(water_yeild_set,Parameter_set.water_yeild);     //水量
										send_commmand(water_interval_set,Parameter_set.water_interval);        //浇水间隔
										send_commmand(water_on_h,Parameter_set.water_on[0]);
										send_commmand(water_on_min,Parameter_set.water_on[1]);
										send_commmand(tem_set,Parameter_set.Temperature);
										send_commmand(humi_set,Parameter_set.humidity);
										page_skip(external_statue);
										handle=1;
										break;
            	        case 0x1a:                                                 //返回自动模式
										page_skip(external_statue);
										handle=1;
										break;
            	        case 0x19:
            	        	            handle=0;
            	        	            break;
                    	default:
                    		 error_statue=receive_command_error;
                    		 printf("error_statue is %d",error_statue);
                    		 break;
            	        }
            	        break;
            	case 0x31:                                      //小时修改
        			m=1;
        			send_commmand(time_h,receive_data[9]);
        			hour_set=receive_data[9];
        			//UART_PRINT("receive is %d",receive_data[9]);
        			break;
            	case 0x32:                                      //分钟修改
        			n=1;
        			minute_set=receive_data[9];
        			//UART_PRINT("receive is %d",receive_data[9]);
        			send_commmand(time_m_shi_set,receive_data[9]/10);
        			send_commmand(time_m_ge_set,receive_data[9]%10);
        			break;
            	case 0x30:                                     //修改确定按钮
        			if(m||n)
        			{
        		    time_get=1356998400u;
        			RTC_IF_SetInitValue(0,0);
        			time_get+=hour_set*60*60+minute_set*60;
        			}
        			m=n=0;
        			handle=1;
        			page_skip(external_statue);
        			break;
            	case 0x34:                                       //继续当前操作   yes
        			GPIO_IF_Set(5,1);
        			handle=1;
        			external_statue |= 0x02;
        			if(plant_statue==0)
        				send_commmand(skip,66);
        			else
        			page_skip(external_statue);
        			water_select[0]=1;
        			Mailbox_post(water_mail,water_select,0);
        			break;
            	case 0x35:                                        //继续当前操作 no
        			flag_watering_stop=1;
        			one_hour=-1;
        			handle=1;
        			fifty_minute=-1;
        			one_hour=-1;
        			osi_Sleep(200);
        			page_skip(external_statue);
        			external_statue &= 0x0d;
        			plant_statue=1;
        			water_select[0]=2;
        			flow_count=0;
        			data_save=1;
        			Mailbox_post(water_mail,water_select,0);
        			break;
            	case 0x36:                                        //跳过按钮
                	Parameter_set.lamp_on[0]=save_Parameter[0];
                	Parameter_set.lamp_on[1]=save_Parameter[1];
                	Parameter_set.lamp_close[0]=save_Parameter[2];
                	Parameter_set.lamp_close[1]=save_Parameter[3];
                	Parameter_set.water_yeild=save_Parameter[4];
                	Parameter_set.water_interval=save_Parameter[5];
                	Parameter_set.water_on[0]=save_Parameter[6];
                	Parameter_set.water_on[1]=save_Parameter[7];
                	Parameter_set.Temperature=save_Parameter[8];
                	Parameter_set.humidity=save_Parameter[9];
            		send_commmand(lamp_on_h,Parameter_set.lamp_on[0]);
            		send_commmand(lamp_on_min,Parameter_set.lamp_on[1]);
            		send_commmand(lamp_close_h,Parameter_set.lamp_close[0]);
            		send_commmand(lamp_clode_min,Parameter_set.lamp_close[1]);
            		send_commmand(water_yeild_set,Parameter_set.water_yeild);     //水量
            		send_commmand(water_interval_set,Parameter_set.water_interval);        //浇水间隔
            		send_commmand(water_on_h,Parameter_set.water_on[0]);
            		send_commmand(water_on_min,Parameter_set.water_on[1]);
            		send_commmand(tem_set,Parameter_set.Temperature);
            		send_commmand(humi_set,Parameter_set.humidity);
        			handle=1;
        		    flag_watering_stop=0;                        //取消删除水量任务
        			page_skip(external_statue);
        			plant_statue=1;
        			break;
            	case 0x06:                                      ///时间设置：时
        			hour_set=receive_data[9];
        			   UART_PRINT("receive is %d",hour_set);
        		    break;
            	case 0x08:                                     //时间设置：分
        			minute_set=receive_data[9];
        			UART_PRINT("receive is %d",minute_set);
        			send_commmand(time_m_shi,(minute_set/10));
        		    send_commmand(time_m_ge,(minute_set%10));
        		    break;
            	case 0x0d:                                             //开灯   时
        			Temp_lamp_on_h = receive_data[9];
        			UART_PRINT("receive is %d",Temp_lamp_on_h);
        		    break;
            	case 0x0f:                                               //开灯   分
        			Temp_lamp_on_m = receive_data[9];
        			UART_PRINT("receive is %d",Temp_lamp_on_m);
        			break;
            	case 0x11:                                             //照明关闭时间 时
        			Temp_lamp_close_h = receive_data[9];
        			UART_PRINT("receive is %d",Temp_lamp_close_h);
        			break;
            	case 0x13:                                             //照明结束时间     分
        			Temp_lamp_close_m = receive_data[9];
        			UART_PRINT("receive is %d",Temp_lamp_close_m);
        			break;
            	case 0x17:                                             //水量
        			Temp_water_yeild =(receive_data[8]<<8);
        			Temp_water_yeild |=receive_data[9];
        			UART_PRINT("receive is %d",Temp_water_yeild);
        			break;
            	case 0x19:                                           //浇水间隔
        			Temp_water_interval = receive_data[9];
        			UART_PRINT("receive is %d",Temp_water_interval);
        			break;
            	case 0x1a:                                          //开水时间  时
        			Temp_water_on_h = receive_data[9];
        			UART_PRINT("receive is %d",Temp_water_on_h);
        			break;
            	case 0x1c:                                        //开水时间    分
        			Temp_water_on_m = receive_data[9];
        			UART_PRINT("receive is %d",Temp_water_on_m);
        			break;
            	case 0x1e:                                       //温度
        			Temp_tem = receive_data[9];
        			UART_PRINT("receive is %d",Temp_tem);
        			break;
            	case 0x21:                                      //湿度
        			Temp_humi = receive_data[9];
        			UART_PRINT("receive is %d",Temp_humi);
        			break;
            	case 0x23:                                      //后台升级入口
        			if(receive_data[9]==0x01)
        					right=1;
        			if((receive_data[9]==0x02)&&(right==1))
        		            right=2;
        		    if((receive_data[9]==0x03)&&(right==2))
        		            right=3;
        		    if((receive_data[9]==0x04)&&(right==3))
        		    {
        		    	right=0;
        		    	//bootloader();
        		    	OGB_sign=1;
        		    	BGO_delay=-1;            //不进行正常启动
        		    	//readkeepfile(IMG_FACTORY_DEFAULT,filebuf);
        		    	//ImageLoader_reboot(FileRead ,IMG_FACTORY_DEFAULT);
        		    }
        		    break;
            	default:
            		 error_statue=receive_command_error;
            		 printf("error_statue is %d",error_statue);
            		 break;
			   }
		}
	}

}
//*****************************************************************************
//
//! 不同的wifi模式转换任务
//!
//! \param  none
//!
//! \return none
//!注：此任务有一个缺点，就是不能在STA模式再次转换到AP模式上
//*****************************************************************************
//void task_mode_change(void *pvParameters)
//{
//	;
//}
//*****************************************************************************
//
//!SSID 扫描任务
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
//void task_scan_ssid(void *pvParameters)
//{
//	uint8_t count=0,i=0;																//对扫描到的有效SSID进行计数
//	while(1)
//	{
//		Event_pend(Scan_SSID_Event, Event_Id_NONE,Event_Id_00, BIOS_WAIT_FOREVER);	//等待扫描SSID任务的发生，等待时间为一直等待
//		count = GetScanResult(&g_NetEntries[0]);								//扫描获取一次SSID
//		if (count > 0)															//如果扫描到的有效SSID的条数大于0打印信息
//			UART_PRINT("Scan Success  valile name count is %d\r\n", count);		//打印条目数
//		for(i=0;i<count;i++)
//		{
//			UART_PRINT("SSID[%d]=%s,SSID_LEN[%d]=%d,RSSI[%d]=%d\r\n",i,&g_NetEntries[i].ssid,i,g_NetEntries[i].ssid_len,i,g_NetEntries[i].rssi);
//		}
////		Sent_ssid();																//通过TCP发送扫描到的SSID
//	}
//}
//*****************************************************************************
//
//! wifi重启任务
//!
//! \param  none
//!
//! \return none
//!
//! 注：当处在AP连接到的设备掉了之后会自动停止wifi，或者在STA模式的时候连接的AP出现异常，连接失败的时候也会自动停止wifi所以需要重新启动
//*****************************************************************************
//void task_wifi_restart(void *pvParameters)
//{
//	;
//}
//*****************************************************************************
//
//!	设备上电wifi设置任务
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
//void task_start_link(void *pvParameters)
//{
//	Start_link();																						//设备上电wifi初始化任务
//}
//*****************************************************************************
//
//! 获取种植文件任务
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
//void task_Get_Controlfile(void *pvParameters)
//{
//	;
//}
//*****************************************************************************
//
//! 更新系统时间
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void task_updata_time(void *pvParameters)
{
	while(1)
	{
		if(plant_statue==1)
		{
#ifdef DEBUG
				get_real_time(time_get+3600*RTC_IF_GetValue().Secs, GMT_DIFF_TIME_HRS, GMT_DIFF_TIME_MINS);//获取真正的系统时间
#else
				get_real_time(time_get+RTC_IF_GetValue().Secs, GMT_DIFF_TIME_HRS, GMT_DIFF_TIME_MINS);//获取真正的系统时间
#endif
		send_commmand(time_h,time_axis.hour);
		send_commmand(time_m_shi,(time_axis.minute/10));
		send_commmand(time_m_ge,(time_axis.minute%10));
		//UART_PRINT("%d-%d-%d %d:%d:%d\r\n",time_axis.year,time_axis.month,time_axis.day,time_axis.hour,time_axis.minute,time_axis.second);//打印系统时间

//		Timer_IF_SetDutyCycle(FAN,99);
		}
		osi_Sleep(100);
	}
}
//*****************************************************************************
//
//! 时间控制台任务
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void task_time_control(void *pvParameters)
{
	static uint8_t old_day=0;
//	static uint16_t old_year=0;
	uint8_t new_day=0;
	uint8_t Water_period_day=0;
///	char *FileRead = "CommandMethod.txt";
	while(1)
	{
		osi_Sleep(100);												//任务休眠时间
#ifdef DEBUG
				get_real_time(time_get+3600*RTC_IF_GetValue().Secs, GMT_DIFF_TIME_HRS, GMT_DIFF_TIME_MINS);//获取真正的系统时间
#else
				get_real_time(time_get+RTC_IF_GetValue().Secs, GMT_DIFF_TIME_HRS, GMT_DIFF_TIME_MINS);//获取真正的系统时间
#endif
		new_day=time_axis.day;																	        //更新当前的day日期
        if(plant_statue==1)																	            //判断当前是否为种植状态
        {
					if(/*(new_year-old_year)<=1 && */new_day!=old_day)			                        //如果当前的日期不等于上一次记录的日期信息
					{
						plan_date++;
						plant_day++;
						//only_one=1;
						data_save=1;
						time_control_lamp_statue=1;
						old_day=new_day;//种植天数增加
						if(save_data_chang==0)                      //浇水后一天更改下一次浇水天数
						{
							if(plan_date==7)
								next_water_date=11;
							if(plan_date==12)
								next_water_date=16;
							if(plan_date==17)
								next_water_date=21;
							if(plan_date==22)
								next_water_date=25;
							if(plan_date==22+4*mn)
							{
							    mn++;
								next_water_date=21+4*mn;
							}
						 }
						if(Water_period==1&&save_data_chang==1)                     //等待本次周期结束，再进行
						{
							if(plan_date==Water_period_day+(Parameter_set.water_interval+1)*nm+1)           //浇水日期的后一天更新next_water_date
							{
								UART_PRINT("Water_period_day is %d",Water_period_day);
								UART_PRINT("Parameter_set.water_interval is %d",Parameter_set.water_interval);
								nm++;
							    next_water_date=Water_period_day+(Parameter_set.water_interval+1)*nm;
							}
						}
						//today_is_over=0;       //每天只浇一次标志
//						if(external_statue&0x08==1&&plan_date==(data_chang_day+Parameter_set.water_interval+2))
//						{
//							plan_date=data_chang_day;
//						}
						UART_PRINT("next_water_date is %d",next_water_date);
						UART_PRINT("today is =%d\r\n",plan_date);
						//updata_plan_day_rule(plant_name,plan_date,rulse_date);					    //更新种植规则
						//last_updata_plant_day=plan_date;															    //种植日期清零
					}
//					if(plan_date==(last_water_date+1))                                                  //确保在为浇水日的当天，上一次浇水日期不是今天，确保，在今天时显示  （距离下次浇水为0天）
//					{
//						last_water_day_record=last_water_date;
//						UART_PRINT("在第二天记录的last_water_day_record is %d",last_water_day_record);
//					}
					if(time_control_lamp_statue==1)
					{
						if(  ((time_axis.hour*60+time_axis.minute)>=(Parameter_set.lamp_on[0]*60+Parameter_set.lamp_on[1])) && ((time_axis.hour*60+time_axis.minute)<(Parameter_set.lamp_close[0]*60+Parameter_set.lamp_close[1]))/* && (only_one==1)*/ )
						{
							GPIO_IF_Set(10, 1);
							GPIO_IF_Set(11, 1);
							GPIO_IF_Set(12, 1);                       //GPIO12 lamp1
							external_statue |= 0x01;
							//only_one=0;
							//UART_PRINT("in the lamp control");
						}
						else
						{
							GPIO_IF_Set(10, 0);
							GPIO_IF_Set(11, 0);
							GPIO_IF_Set(12, 0);                       //GPIO12 lamp1
							external_statue &= 0x0e;
						}
						osi_Sleep(200);
					}
				   /*更新当前的水量控制*/
					if(time_control_water_statue==1)
					{
						if(save_data_chang==1&&Water_period==1)               //达人模式：第五天开始，每隔Parameter_set.water_interval浇一次水
						{                                                     //等待下次浇水完成
							for(i=Water_period_day;i<255;i+=(Parameter_set.water_interval+1))
							  {
								 if( (plan_date==i) && ((time_axis.hour*60+time_axis.minute)==(Parameter_set.water_on[0]*60+Parameter_set.water_on[1]))/*&& ( today_is_over== 0)*/)
									{
									    if(data_chang==0)
									    {
											if(plan_date<21)                    //跳回自动的时候根据假如处于自动模式的哪个周期，就从那天开始，重新经过多少天，再次进行浇水
											{
												plan_date=6;
												next_water_date=11;
											}
											else
											{
												plan_date=21;
											    next_water_date=25;
											}
									    	save_data_chang=0;
								            Water_period=0;
								            data_chang=1;
									    }
									    next_water_date=Water_period_day+(Parameter_set.water_interval+1)*(nm+1);
									    UART_PRINT("next_water_date is %d",next_water_date);
										controlrule.control_water_frequency=10;                //传入水量参数
										controlrule.control_water_yield=Parameter_set.water_yeild;
										Event_post(Control_water_Event,Event_Id_01);
									}
							 }
						}
						else                        //自动模式
						{
							//UART_PRINT("atuo mode");
							for(i=21;i<255;i+=(Parameter_set.water_interval+1))
							{
								if(((plan_date==6)||(plan_date==11)||(plan_date==16)||(plan_date==21)||(plan_date==i))&& ((time_axis.hour*60+time_axis.minute) == (Parameter_set.water_on[0]*60+Parameter_set.water_on[1])) /*&& (today_is_over==0)*/)
								{
									if(plan_date==6)
										next_water_date=11;
									if(plan_date==11)
										next_water_date=16;
									if(plan_date==16)
										next_water_date=21;
									if(plan_date==21)
										next_water_date=25;
									if(plan_date>22)
										next_water_date=21+4*mn;
									Parameter_set.water_yeild=automode_water_yeild;
									controlrule.control_water_frequency=10;                                //传入水量参数
									controlrule.control_water_yield=Parameter_set.water_yeild;
									Event_post(Control_water_Event,Event_Id_01);
									//external_statue |= 0x02;
									if(save_data_chang==1)
									{
										Parameter_set.water_yeild=Temp_water_yeild;
										Water_period=1;                   //下次浇水完成
										Water_period_day=plan_date;
										UART_PRINT("Water_period_day is %d",Water_period_day);
									}
									//today_is_over=1;
								}
							}
						}
					}
		//																					//写入日志文件																								//把当前的时间付给旧的时间，用于判断种植时间是否更新
		//		old_year=new_year;
		//		UART_PRINT("today is %d",plan_date);
				//UART_PRINT("%d-%d-%d %d:%d:%d\r\n",time_axis.year,time_axis.month,time_axis.day,time_axis.hour,time_axis.minute,time_axis.second);
		//		UART_PRINT("temp=%d,humi=%d\r\n",controlrule.control_temp,controlrule.control_humi);
		//		UART_PRINT("water_yield=%d,water_frequency=%d\r\n",controlrule.control_water_yield,controlrule.control_water_frequency);
		//		UART_PRINT("color1=%d,color2=%d,color3=%d\r\n",controlrule.control_lamp1_color,controlrule.control_lamp2_color,controlrule.control_lamp3_color);
	  }
	}
}
//*****************************************************************************
//
//!	水量控制任务
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void task_Control_Water(void *pvParameters)
{
	uint8_t i=0;
	uint8_t water_frequency=0,water_gap=0;
	uint16_t water_change=0,old_water=0;
	uint8_t data[10]={0};
	while(1)
	{

		//UART_PRINT("join the water contral...");
		Event_pend(Control_water_Event, Event_Id_NONE,Event_Id_00|Event_Id_01, BIOS_WAIT_FOREVER);			//等待水量控制事件的产生
		water_frequency=controlrule.control_water_frequency/10;												//获取浇水次数
		water_gap=controlrule.control_water_frequency%10;//获取浇水的时间间隔单位为分钟
		water_change=(uint16_t)(controlrule.control_water_yield*FLOW_CHANGE);
		UART_PRINT("本次在第%d天浇水\r\n",plan_date);
		UART_PRINT("本次浇水量=%dml\r\n",controlrule.control_water_yield);
		UART_PRINT("脉冲数是 %d",water_change);
		last_water_date=plant_day;
		external_statue |= 0x02;
		for(i=0;i<water_frequency;i++)
		{
			flow_count=0;																					//水量计数清零
			//long a,b=0;
			//UART_PRINT("water_cish=%d\r\n",i+1);
//			while(flow_count<controlrule.control_water_yield)
			GPIO_IF_Set(5,1);
			while(flow_count<water_change)//等待浇水完成
			{
				UART_PRINT("water yeild is =%d\r\n",flow_count);//打印当前浇水量
				osi_Sleep(30);
				if(flag_watering_stop==1)
					break;
				last_water_filed_record=(uint16_t)(flow_count/FLOW_CHANGE);
				if((flow_count%400)>0&&(flow_count%400)<5)
				{
				data_save=1;
				osi_Sleep(10);
				}
				if(old_water!=flow_count)
				{
			        old_water=flow_count;
				}
				else
				{
					osi_Sleep(2000);
					if(old_water==flow_count)
					{
						GPIO_IF_Set(5,0);
						external_statue &= 0x0d;
						osi_Sleep(500);                             //等待real skip完成
						while(1)
						{
							if(GPIO_IF_Get(13)==0)
							{							                                    //屏蔽其他跳转任务
								send_commmand(skip,67);
								handle=0;                                             //关闭实时跳转
								standby=-1;                                            //关闭屏保
								UART_PRINT("short water ...");
								data[0]=0;
							}
							else if(GPIO_IF_Get(13)==1)
							{
								UART_PRINT("laishui le");
								handle=0;                               //同样关闭跳转
								//standby=screen_protect_await_time;
								standby=-1;
								send_commmand(skip,57);
								//time_begin_standby=1;
								data[0]=0;
								Mailbox_pend(water_mail,data,BIOS_WAIT_FOREVER);     //mail_pend
								UART_PRINT("data is %d",data[0]);
								if(data[0]==1)                    //yes
								{
									UART_PRINT("yes");
									break;
								}
								else if(data[0]==2)              //no
								{
									flag_watering_stop=1;
									UART_PRINT("no");
									break;
								}
							}
							osi_Sleep(100);
						}
					}
					else
					{
						old_water=flow_count;
					}
				}
			}
			water_mode=0;
			UART_PRINT("flow_count gross=%d\r\n",flow_count);
			osi_Sleep(500);              //此处停电磁阀的时候，流量计还在计数，时间间隔尽量长点
			//flow_count=0;	//目前浇水量清零
			data_save_over=1;
			//UART_PRINT("time time FLOW_CHANGE is %f",(flow_count/controlrule.control_water_yield));
			GPIO_IF_Set(5,0);
			water_complete=1;
			if(flag_watering_stop==1)
					break;
			if(i!=(water_frequency-1))																		//如果不是最后一次浇水 就不休眠，否则休眠指定的时间
				osi_Sleep(1000*water_gap*60);
		}
		data[0]=0;
		external_statue &= 0x0d;
		flag_watering_stop=0;
		UART_PRINT("control_water flag_watering_stop is %d",flag_watering_stop);
		UART_PRINT("water complete\r\n");
		controlrule.control_water_frequency=0;																//浇水完成后，浇水的频率清零
		controlrule.control_water_yield=0;																	//浇水的水量清零
		water_change=0;
		osi_Sleep(100);
	}
//		UART_PRINT("Water complete\r\n");
}
//*****************************************************************************
//
//!	页面跳转任务
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
//void task_page_skip(void *pvParameters)
//{
//	while(1)
//	{
//		switch(external_statue)
//		{
//		case 0x00:                          //都关
//			send_commmand(skip,6);   break;
//		case 0x04:                          //开风
//			send_commmand(skip,8);   break;
//		case 0x02:                          //开水
//			send_commmand(skip,10);   break;
//		case 0x06:                          //开水  开风
//			send_commmand(skip,12);   break;
//		case 0x01:                          //开灯
//			send_commmand(skip,14);   break;
//		case 0x05:                          //开灯  开风
//			send_commmand(skip,16);   break;
//		case 0x03:                          //开灯 开水
//			send_commmand(skip,18);   break;
//		case 0x07:                          //全开
//			send_commmand(skip,20);   break;
//		default:
//			UART_PRINT("skip error\r\n");       break;
//		}
//	}
//}
//*****************************************************************************
//
//!	水量控制任务
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
//void task_Breath_led(void *pvParameters)
//{
//;
//}
//*****************************************************************************
//
//! 颜色，温度控制任务
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void task_Control_Tem_color(void *pvParameters)
{
	//TIME_TEST=1000;
	while(1)
	{
		uint64_t tem1=0,humi1=0/*,num=1*/;
//		MAP_UtilsDelay(100);
//		while((num++)<5000000);//2秒
//		GPIO_IF_Set(OUTSIDE_DHT11,1);
//		num=1;
//		while((num++)<5000000);
////		MAP_UtilsDelay(100);
//		GPIO_IF_Set(OUTSIDE_DHT11,0);
//		num=1;
		osi_Sleep(1000);																					//温度控制的周期
		DHT_GET DHT11_OUT={0,0,0,0,0};
		DHT11_OUT=dht_getdat(OUTSIDE_DHT11);																					//获取当前的温湿度
//		osi_Sleep(1000);
//		DHT11_IN=dht_getdat(INSIDE_DHT11);
//		if((DHT11_OUT.humi1+DHT11_OUT.humi2+DHT11_OUT.temp1+DHT11_OUT.temp2)==DHT11_OUT.check)									//判断温度是否校验成功，成功之后可以用，否则放弃此次控制
		    tem1=(DHT11_OUT.temp1*256+DHT11_OUT.temp2)/10;
		    humi1=(DHT11_OUT.humi1*256+DHT11_OUT.humi2)/10;
			//UART_PRINT("tem_out=%d,humi=%d,check_out=%d\r\n",tem1,humi1,DHT11_OUT.check);
//		if((DHT11_IN.humi1+DHT11_IN.humi2+DHT11_IN.temp1+DHT11_IN.temp2)==DHT11_IN.check)									//判断温度是否校验成功，成功之后可以用，否则放弃此次控制
			//UART_PRINT("tem_in=%d.%d,shidu_in=%d.%d,%d\r\n",DHT11_IN.temp1,DHT11_IN.temp2,DHT11_IN.humi1,DHT11_IN.humi2,DHT11_IN.check);
		    if(tem1&&humi1)
		    {
			send_commmand(tem,tem1);
			send_commmand(humi,humi1);
		    }
			if((time_control_fan_statue==1) && (plant_statue==1))
			{
				if((tem1>Parameter_set.Temperature)||humi1>Parameter_set.humidity)
				{
					Timer_IF_SetDutyCycle(FAN,100);
					external_statue |= 0x04;
					//UART_PRINT("%x",external_statue);
				   // UART_PRINT("in the wendu control");
				}
				else
				{
					Timer_IF_SetDutyCycle(FAN,0);
					external_statue &= 0x0b;
					//UART_PRINT("%x",external_statue);
				}
				//UART_PRINT("%d",Parameter_set.Temperature);
				//osi_Sleep(3*1000);
			}
//		if(TIME_TEST==0)
//		{
//			//TIME_TEST=1000;
//			UART_PRINT("I GET THE TIME\r\n");
////			GPIO_IF_Toggle(5);
//			TIME_TEST=-1;
//		}
	//	osi_Sleep(10);
	}
}
//*****************************************************************************
//
//! Application startup display on UART
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
//void task_send_rssi(void *pvParameters)
//{
//	int8_t rssi=0;
//	while(1)
//	{
//		osi_Sleep(6000);
//		if(Current_Mode==ROLE_STA)							//如果当前的模式为STA模式，搜寻当前连接的wifi名，然后返回当前的RSSI的值
//		{
//			rssi = Send_RSSI();								//搜寻连接的AP的rssi
//			UART_PRINT("rssi=%d\r\n", rssi);					//打印rssi信息
//			if (rssi != FALSE)									//如果返回的值不为FALSE
//				Report_User("CUST,UART,0,2,0,0,0,0,%d,%d,%d,0,~", Current_Mode,rssi,get_uart_sebsor.uart_water_leverl);	//串口发送当前的状态，和RSSI的值
//			else
//			{
//				Report_User("CUST,UART,0,2,0,0,0,0,%d,3,%d,0,~", Current_Mode,get_uart_sebsor.uart_water_leverl);
//				//否则打印错误信息
//				UART_PRINT("Isn't find rssi!\r\n");
//			}
//		}
//		else												//如果不是STA模式
//		{
//			rssi = 0;											//rssi清零
//			Report_User("CUST,UART,0,2,0,0,0,0,%d,%d,%d,0,~", Current_Mode, rssi,get_uart_sebsor.uart_water_leverl);	//串口发送当前的状态，和RSSI的值
//		}
//	}
//}
//*****************************************************************************
//
//! 重新注册TCPserver和task_mdns任务
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
//void task_restore(void)
//{
//	long lRetVal = -1;
//	if(task_tcp_server_handle==NULL)
//	{
//        lRetVal = osi_TaskCreate(task_tcp_server,(const signed char*) "task_tcp_server",
//                OSI_STACK_SIZE, NULL, 5, (OsiTaskHandle )&task_tcp_server_handle);							//创建TCP服务器任务
//
//        if (lRetVal < 0)
//        {
//            ERR_PRINT(lRetVal);
//        }
//	}
//	if(task_mdnsget_handle==NULL)
//	{
//        lRetVal = osi_TaskCreate(task_mdnsget,(const signed char*) "task_mdnsget",
//                1024, NULL, 1, (OsiTaskHandle )&task_mdnsget_handle);									//创建MDNS搜寻任务
//        if (lRetVal < 0)
//        {
//            ERR_PRINT(lRetVal);
//        }
//	}
//	if(task_test2_handle==NULL)
//	{
//        lRetVal = osi_TaskCreate(task_test2, (const signed char*) "task_test2",
//                                        OSI_STACK_SIZE, NULL, 2,  (OsiTaskHandle )&task_test2_handle);
//        if (lRetVal < 0)
//        {
//            ERR_PRINT(lRetVal);
//        //		LOOP_FOREVER();
//        }
//	}
//	UART_PRINT("task set complete\r\n");
//}

//*****************************************************************************
//
//!	 task_delete
//!	 删除当前网络未连接前与所有网络有关的任务
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void task_delete(void)
{
	if(task_mdnsget_handle!=NULL)
	{
		Task_delete(&task_mdnsget_handle);																		//删除mdns任务
		if(task_mdnsget_handle!=NULL)
			UART_PRINT("MDNS CLOSE TASK ERROR!!\r\n");
	}
	if(task_tcp_server_handle!=NULL)
	{
		Task_delete(&task_tcp_server_handle);																	//删除TCP服务器任务
		if(task_tcp_server_handle!=NULL)
			UART_PRINT("TCP_SERVER CLOSE TASK ERROR!!\r\n");
	}
	if(task_test2_handle!=NULL)
	{
		Task_delete(&task_test2_handle);																	//删除TCP服务器任务
		if(task_test2_handle!=NULL)
		UART_PRINT("TCP_SERVER CLOSE TASK ERROR!!\r\n");
	}
	UART_PRINT("task delete complete\r\n");
}
//*****************************************************************************
//
//! 控制台发送指令函数
//!
//! \param  backdata 传入的指令信息
//!
//! \return none
//!
//*****************************************************************************
void task_run_statue(void *pvParameters)
{
	long i;
	while(1)
	{
		for(i=0;i<100;i++)
		{
			GPIO_IF_Set(7,0);
			osi_Sleep(300);
			GPIO_IF_Set(7,1);
			osi_Sleep(300);
		}
	}
}
void task_Parameters_print(void *pvParameters)
{
//	int gap_day_mode=0;
	while(1)
	{
		osi_Sleep(100);
		if(plant_statue==1)
		{
					send_commmand(last_water_day,plant_day-last_water_date);
					send_commmand(last_water_filed,last_water_filed_record);
					send_commmand(next_water_day,next_water_date-plan_date);
					if(save_data_chang==1&&Water_period==1)
					{
					    send_commmand(next_water_yeild,Parameter_set.water_yeild);
					    UART_PRINT("Parameter_set.water_yeild is %d",Parameter_set.water_yeild);
					}
					else if(Water_period==0)
						send_commmand(next_water_yeild,automode_water_yeild);
	     }
		send_commmand(one_hour_minute,one_hour/1000/60);
	    send_commmand(one_hour_second,one_hour/1000%60);
	}
}

//*****************************************************************************
//
//! 时钟软中断注册
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
//void calculate_flow_task_set(void)
//{
//	Clock_Params clockParams;
//	Error_Block eb;
//
//	Error_init(&eb);
//	Clock_Params_init(&clockParams);									//初始化参量
//
//	clockParams.period = 1;
//	clockParams.startFlag = 1;											//注册完成后就开始执行
//	clockParams.arg =NULL;
//	flow_get = Clock_create((Clock_FuncPtr)task_flow_get, 1, &clockParams, &eb);//注册中断  ，中断函数为task_flow_get 第一次的时间为500
//
//	if (flow_get == NULL)														//判断是否创建成功
//	{
//		UART_PRINT("Clock create failed");
//	}
//}
//*****************************************************************************
//
//! tcp控制台，控制指令发送
//!
//! \param  control_message 解析到的命令
//!
//! \return none
//!
//*****************************************************************************
#ifdef  PROTO_BUFFER
void tcp_command_send(TCP_Message control_message)
{
	switch (control_message.mod)									//命令模式
	{
		case MOD_SSID_MOD:											//ssid 模式
			break;
		case MOD_CONNECT_MOD:										//连接模式
			break;
		case MOD_SEND_IP:											//发送IP指令
			break;
		case MOD_TEM_MOD:											//获取温度指令
			break;
		case MOD_LAMP_MOD:											//灯光控制指令
			break;
		case MOD_TANK_MOD:											//获取水箱状态指令
			break;
		case MOD_FAN_MOD:											//风扇状态指令
			break;
		case MOD_PLANT_MOD:											//种植日期指令
			break;
		case MOD_TYPE_MOD:											//种植类型指令
			break;
		case MOD_TIME_MOD:											//手机授时
			break;
		case MOD_RULE_MOD:
			break;
		case MOD_FIRMWARE_MOD:
			break;
		case MOD_LOG_MOD:
			break;
		case MOD_BIG_DATA_MOD:
			break;
		case MOD_NAME_MOD:
			break;
		default:
			UART_PRINT("Isn't right TCP commond\r\n");
	}
}
#endif
