/*
 * RTC.c
 *
 *  Created on: 2016年3月5日
 *      Author: Kang
 */

//*****************************************************************************
//
//! RTC 使能函数
//!
//! \param  None
//!
//! \return lRetVal 开启成功返回1，否则返回0
//
//*****************************************************************************
#include "RTC_if.h"

long
RTC_IF_Enable(void)
{
	long lRetVal = -1;
	PRCMRTCInUseSet();					//使能RTC
	lRetVal=PRCMRTCInUseGet();			//判断RTC是否被使能
	return lRetVal;
}

//*****************************************************************************
//
//! RTC 设置开始计数的值
//!
//! \param  sec 设置秒
//!	\param	msec 设置毫秒
//!
//! \return None
//
//*****************************************************************************
void
RTC_IF_SetInitValue(unsigned long sec,unsigned short msec)
{
	PRCMRTCSet(sec,msec);			//设置初始时间
}

//*****************************************************************************
//
//! RTC 获取时间函数
//!
//! \param  None
//!
//! \return rtc_time 返回得到的时间 secs 代表秒  Msec 代表毫秒
//
//*****************************************************************************
rtc_time
RTC_IF_GetValue(void)
{
	rtc_time gettime;
	PRCMRTCGet(&gettime.Secs,&gettime.Msec);		//获取时间
	return gettime;
}
