/*
 * RTC_if.h
 *
 *  Created on: 2016年3月5日
 *      Author: Kang
 */

#ifndef PLANT_CONTROL_PTOTOBUFFER_COMMON_RTC_IF_H_
#define PLANT_CONTROL_PTOTOBUFFER_COMMON_RTC_IF_H_
#include "systerm_init.h"
//RTC 读取时间结构体
typedef struct
{
	unsigned long Secs;
	unsigned short Msec;
}rtc_time;

//*****************************************************************************
//
// API Function prototypes
//
//*****************************************************************************
long RTC_IF_Enable(void);
void RTC_IF_SetInitValue(unsigned long sec,unsigned short msec);
rtc_time RTC_IF_GetValue(void);
#endif /* PLANT_CONTROL_PTOTOBUFFER_COMMON_RTC_IF_H_ */
