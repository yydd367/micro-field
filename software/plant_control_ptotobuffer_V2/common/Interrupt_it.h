/*
 * Interrupt_it.h   中断服务程序
 *
 *  Created on: 2016年3月5日
 *      Author: Kang
 */

#ifndef PLANT_CONTROL_PTOTOBUFFER_COMMON_INTERRUPT_IT_H_
#define PLANT_CONTROL_PTOTOBUFFER_COMMON_INTERRUPT_IT_H_
#include "systerm_init.h"
#define MAXUART_QUEUE	5
extern long flow_count;
extern uint8_t time_begin;
extern uint8_t recievebuffer[50];
extern char flag_uart_receive_complete;
extern uint8_t time_control_water_statue;
extern uint8_t time_control_lamp_statue;
extern uint8_t time_control_fan_statue;
//extern uint8_t receive_data_statue;
void TimerBaseIntHandler(void);
void TimerRefIntHandler(void);
void GpioA0Handle(void);
void UART1IntHandler(void);

#endif /* PLANT_CONTROL_PTOTOBUFFER_COMMON_INTERRUPT_IT_H_ */
