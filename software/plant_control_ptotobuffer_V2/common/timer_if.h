//*****************************************************************************
// timer_if.h
//
// timer interface header file: Prototypes and Macros for timer APIs
//
// Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

#ifndef __TIMER_IF_H__
#define __TIMER_IF_H__

#include "systerm_init.h"
//*****************************************************************************
//
// If building with a C++ compiler, make all of the definitions in this header
// have a C binding.
//
//*****************************************************************************
#ifdef __cplusplus
extern "C"
{
#endif

// The PWM works based on the following settings:
//     Timer reload interval -> determines the time period of one cycle
//     Timer match value -> determines the duty cycle
//                          range [0, timer reload interval]
// The computation of the timer reload interval and dutycycle granularity
// is as described below:
// Timer tick frequency = 80 Mhz = 80000000 cycles/sec
// For a time period of 0.5 ms,
//      Timer reload interval = 80000000/2000 = 40000 cycles
// To support steps of duty cycle update from [0, 255]
//      duty cycle granularity = ceil(40000/255) = 157
// Based on duty cycle granularity,
//      New Timer reload interval = 255*157 = 40035
//      New time period = 0.5004375 ms
//      Timer match value = (update[0, 255] * duty cycle granularity)
//
//#define TIMER_INTERVAL_RELOAD   40035 /* =(255*157) */
//#define DUTYCYCLE_GRANULARITY   157
//#define TIMER_INTERVAL_RELOAD   3200 /* =(32*100) 周期设置*/
//#define DUTYCYCLE_GRANULARITY   32	/* 占空比基准设置*/

//#define TIMER_INTERVAL_RELOAD   40000 /* =(32*100) 周期设置*/
//#define DUTYCYCLE_GRANULARITY   40	/* 占空比基准设置*/

typedef enum
{
	FAN=0,
	LAMP1,
	LAMP2,
}User_pwm_kind;
/****************************************************************************/
/*								MACROS										*/
/****************************************************************************/
#define SYS_CLK				    80000000							//系统时钟
#define MILLISECONDS_TO_TICKS(ms)   ((SYS_CLK/1000) * (ms))			//定时周期最小1ms
#define TIME_lamp_day				SoftTime[0]
#define TIME_15min_lamp				SoftTime[1]
#define TIME_water              SoftTime[2]
#define TIME_15min_fan          SoftTime[3]
#define standby                 SoftTime[4]
#define one_hour                SoftTime[5]
#define watering_UI_delay       SoftTime[6]
#define adjust_time_delay       SoftTime[7]
#define BGO_delay               SoftTime[8]
#define unlock_key_delay        SoftTime[9]
#define fifty_minute            SoftTime[10]
//#define PERIODIC_TEST_LOOPS     5
extern int32_t SoftTime[15];
extern void Timer_IF_Init( unsigned long ePeripheralc, unsigned long ulBase,
    unsigned long ulConfig, unsigned long ulTimer, unsigned long ulValue);
extern void Timer_IF_IntSetup(unsigned long ulBase, unsigned long ulTimer, 
                   void (*TimerBaseIntHandler)(void));
extern void Timer_IF_InterruptClear(unsigned long ulBase);
extern void Timer_IF_Start(unsigned long ulBase, unsigned long ulTimer, 
                unsigned long ulValue);
extern void Timer_IF_Stop(unsigned long ulBase, unsigned long ulTimer);
extern void Timer_IF_ReLoad(unsigned long ulBase, unsigned long ulTimer, 
                unsigned long ulValue);
extern unsigned int Timer_IF_GetCount(unsigned long ulBase, unsigned long ulTimer);
void Timer_IF_DeInit(unsigned long ulBase,unsigned long ulTimer);
//*****************************************************************************
//
// PWM mode function
//
//*****************************************************************************
//void Timer_IF_SetPWMMode(unsigned long ulBase, unsigned long ulTimer,
//                       unsigned long ulConfig, unsigned char ucInvert);
void Timer_IF_InitPWMModules(void);
void Timer_IF_DeInitPWMModules(void);
//void Timer_IF_SetDutyCycle(unsigned long ulBase, unsigned long ulTimer,
//                     unsigned short ucLevel);
void Timer_IF_SetPWMMode(User_pwm_kind kind,unsigned long ulConfig, unsigned char ucInvert,unsigned long  frequency);
void Timer_IF_SetDutyCycle(User_pwm_kind kind,unsigned short ucLevel);
void pwm_init(void);
//*****************************************************************************
//
// user time function
//
//*****************************************************************************
void time_init(void);
//*****************************************************************************
//
// Mark the end of the C bindings section for C++ compilers.
//
//*****************************************************************************
#ifdef __cplusplus
}
#endif
#endif //  __TIMER_IF_H__
