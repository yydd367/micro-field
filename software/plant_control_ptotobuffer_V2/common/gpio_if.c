//*****************************************************************************
// gpio_if.c
//
// GPIO interface APIs, this common interface file helps to configure,
// set/toggle only 3 GPIO pins which are connected to 3 LEDs of CC32xx Launchpad
//
// Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

// Standard includes
#include <stdio.h>

// Driverlib includes
#include "hw_types.h"
#include "hw_ints.h"
#include "hw_memmap.h"
#include "hw_apps_rcm.h"
#include "interrupt.h"
#include "pin.h"
#include "gpio.h"
#include "prcm.h"
#include "rom.h"
#include "rom_map.h"
//#include "uart_if.h"
#include "common.h"
// OS includes
#if defined(USE_TIRTOS) || defined(USE_FREERTOS) || defined(SL_PLATFORM_MULTI_THREADED)
#include <stdlib.h>
#include "osi.h"
#endif

// Common interface include
#include "gpio_if.h"

//****************************************************************************
//                      GLOBAL VARIABLES       GPIO地址数组
//****************************************************************************
static unsigned long ulReg[]=
{
    GPIOA0_BASE,
    GPIOA1_BASE,
    GPIOA2_BASE,
    GPIOA3_BASE,
    GPIOA4_BASE
};

//*****************************************************************************
// Variables to store TIMER Port,Pin values
//*****************************************************************************
//unsigned int g_uiLED1Port = 0,g_uiLED2Port = 0,g_uiLED3Port = 0;
//unsigned char g_ucLED1Pin,g_ucLED2Pin,g_ucLED3Pin;

#define GPIO_LED1 9
#define GPIO_LED2 10
#define GPIO_LED3 11


//****************************************************************************
//                      LOCAL FUNCTION DEFINITIONS
//****************************************************************************
/*
 * 获取GPIO的中断向量
 * */
static unsigned char
GetPeripheralIntNum(unsigned int uiGPIOPort)
{
    switch(uiGPIOPort)
    {
       case GPIOA0_BASE:
          return INT_GPIOA0;
       case GPIOA1_BASE:
          return INT_GPIOA1;
       case GPIOA2_BASE:
          return INT_GPIOA2;
       case GPIOA3_BASE:
          return INT_GPIOA3;
       default:
          return INT_GPIOA0;
    }

}


//*****************************************************************************
//
//!  读取GPIO的数值，输入输出都能用
//!
//!  \param   ucPin is the pin to be set-up as a GPIO (0:39) GPIO号
//!
//!
//!  \return 获取的数值
//
//*****************************************************************************
unsigned char
GPIO_IF_Status(unsigned char ucPin)
{
  unsigned char ucLEDStatus=0;
  ucLEDStatus=GPIO_IF_Get(ucPin);		//读取相应引脚的数值
  return ucLEDStatus;					//返回读取的状态
}


//*****************************************************************************
//
//! 翻转电平
//!
//! \param  ucPin is the pin to be set-up as a GPIO (0:39) GPIO号
//!
//! \return none
//!
//! \brief  翻转IO的电平
//
//*****************************************************************************
void GPIO_IF_Toggle(unsigned char ucPin)
{

    unsigned char ucLEDStatus = GPIO_IF_Status(ucPin);			//读取当前引脚的状态
    if(ucLEDStatus == 1)										//状态为高电平
    {
    	GPIO_IF_Set(ucPin,0);									//设置低电平
    }
    else
    {
    	GPIO_IF_Set(ucPin,1);									//否则设置为高电平
    }
}

//****************************************************************************
//
//! 通过输入的引脚号 获取GPIO的基础地址，和组号
//!
//! \param ucPin is the pin to be set-up as a GPIO (0:39) GPIO号
//! This function
//!
//!
//! \return None.
//
//****************************************************************************
g_ucgpio
GPIO_IF_GetPortNPin(unsigned char ucPin)
{
	g_ucgpio gpio;
    //
    // Get the GPIO pin from the external Pin number
    //
    gpio.g_ucpin = 1 << (ucPin % 8);				//获取GPIO的 引脚号

    //
    // Get the GPIO port from the external Pin number
    //
    gpio.g_ulport = (ucPin / 8);					//获取GPIO的组号
    gpio.g_ulport = ulReg[gpio.g_ulport];			//得到对应组号的基础地址
    return gpio;									//返回得到的数据
}

//****************************************************************************
//
//! Configures the GPIO selected as input to generate interrupt on activity
//!
//! \param ucPin is the pin to be set-up as a GPIO (0:39) GPIO号
//! \param uiIntType is the type of the interrupt (refer gpio.h)
//! \param pfnIntHandler is the interrupt handler to register
//! 
//! This function  
//!    1. Sets GPIO interrupt type
//!    2. Registers Interrupt handler
//!    3. Enables Interrupt
//!
//! \return None
//
//****************************************************************************
void
GPIO_IF_ConfigureNIntEnable(unsigned char ucPin,
                                  unsigned int uiIntType,
                                  void (*pfnIntHandler)(void))
{
	g_ucgpio gpio;
//	int c=0;
	gpio = GPIO_IF_GetPortNPin(ucPin);							//得到相应GPIO的组号和引脚号
    //
    // Set GPIO interrupt type
    //
    MAP_GPIOIntTypeSet(gpio.g_ulport,gpio.g_ucpin,uiIntType);	//设置引脚类型

    //
    // Register Interrupt handler
    //
#if defined(USE_TIRTOS) || defined(USE_FREERTOS) || defined(SL_PLATFORM_MULTI_THREADED)
    // USE_TIRTOS: if app uses TI-RTOS (either networking/non-networking)
    // USE_FREERTOS: if app uses Free-RTOS (either networking/non-networking)
    // SL_PLATFORM_MULTI_THREADED: if app uses any OS + networking(simplelink)
    osi_InterruptRegister(GetPeripheralIntNum(gpio.g_ulport),
                                        pfnIntHandler, INT_PRIORITY_LVL_1);			//操作系统中 注册中断向量
//    UART_PRINT("OsiReturnVal_e=%d\r\n",c);
#else
	MAP_IntPrioritySet(GetPeripheralIntNum(gpio.g_ulport), INT_PRIORITY_LVL_1);		//非操作系统设置 优先级
    MAP_GPIOIntRegister(gpio.g_ulport,pfnIntHandler);								//非操作系统设置中断向量
#endif

    //
    // Enable Interrupt
    //
    MAP_GPIOIntClear(gpio.g_ulport,gpio.g_ucpin);									//清除中断标志位
    MAP_GPIOIntEnable(gpio.g_ulport,gpio.g_ucpin);									//使能中断
}

//****************************************************************************
//
//! Set a value to the specified GPIO pin
//!
//! \param ucPin is the pin to be set-up as a GPIO (0:39) GPIO号

//! \param ucGPIOValue is the value to be set
//! 
//! This function  
//!    1. Sets a value to the specified GPIO pin
//!
//! \return None.
//
//****************************************************************************
void 
GPIO_IF_Set(unsigned char ucPin,unsigned char ucGPIOValue)
{
	g_ucgpio gpio;
    //
    // Set the corresponding bit in the bitmask
    //
	gpio = GPIO_IF_GetPortNPin(ucPin);			//获取输入引脚的
    ucGPIOValue = ucGPIOValue << (ucPin % 8);	//得到设置的值

    //
    // Invoke the API to set the value
    //
    MAP_GPIOPinWrite(gpio.g_ulport,gpio.g_ucpin,ucGPIOValue);//设置相应的引脚值
}


//****************************************************************************
//
//! Get a value to the specified GPIO pin
//!
//! \param ucPin is the GPIO pin to be set (0:39) GPIO号
//!
//! This function
//!    1. Gets a value of the specified GPIO pin
//!
//! \return value of the GPIO pin
//
//****************************************************************************
unsigned char
GPIO_IF_Get(unsigned char ucPin)
{
    unsigned char ucGPIOValue;
    long lGPIOStatus;
    g_ucgpio gpio;
    gpio = GPIO_IF_GetPortNPin(ucPin);								//获取输入引脚的
    //
    // Invoke the API to Get the value
    //
    lGPIOStatus =  MAP_GPIOPinRead(gpio.g_ulport,gpio.g_ucpin);		//读取相应值的数值

    //
    // Set the corresponding bit in the bitmask
    //
    ucGPIOValue = lGPIOStatus >> (ucPin % 8);						//把得到的值进行处理
    return ucGPIOValue;												//返回得到的值
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************
