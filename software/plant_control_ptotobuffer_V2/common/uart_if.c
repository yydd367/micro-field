//*****************************************************************************
// uart_if.c
//
// uart interface file: Prototypes and Macros for UARTLogger
//
// Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

// Standard includes
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Driverlib includes
#include "hw_types.h"
#include "hw_memmap.h"
#include "hw_ints.h"
#include "prcm.h"
#include "pin.h"
#include "uart.h"
#include "rom.h"
#include "rom_map.h"
#include "interrupt.h"

#if defined(USE_FREERTOS) || defined(USE_TI_RTOS)
#include "osi.h"
#endif
#include "osi.h"
#include "uart_if.h"
#include "common.h"
#include "task.h"

#define IS_SPACE(x)       (x == 32 ? 1 : 0)

//*****************************************************************************
// Global variable indicating command is present
//*****************************************************************************
static unsigned long __Errorlog;

//*****************************************************************************
// Global variable indicating input length
//*****************************************************************************
unsigned int ilen=1;


//*****************************************************************************
//
//! Initialization
//!
//! This function
//!        1. Configures the UART to be used.
//!
//! \return none
//
//*****************************************************************************
void 
InitTerm()
{
#ifndef NOTERM
  /*配置串口0
   * 波特率 	115200
   * 数据长度	8
   * 停止位	1
   * 校验位	无
   * */
  MAP_UARTConfigSetExpClk(CONSOLE,MAP_PRCMPeripheralClockGet(CONSOLE_PERIPH), 
                  UART_BAUD_RATE, (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                		  UART_CONFIG_PAR_ODD));


#endif
  __Errorlog = 0;
}
void
InitTerm1()
{
	  /*配置串口1
	    * 波特率 	115200
	    * 数据长度	8
	    * 停止位	1
	    * 校验位	无
	    * */
	  MAP_UARTConfigSetExpClk(CONSOLE1,MAP_PRCMPeripheralClockGet(CONSOLE1_PERIPH),
	                    UART_BAUD_RATE, (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
	                    		UART_CONFIG_PAR_ODD));
	  UARTFIFODisable  (CONSOLE1 ) ;
}
//*****************************************************************************
//
//!    Outputs a character string to the console
//!
//! \param str is the pointer to the string to be printed
//!
//! This function
//!        1. prints the input string character by character on to the console.
//!
//! \return none
//
//*****************************************************************************

//串口0的发送字符串函数
void 
Message(const char *str)
{
#ifndef NOTERM
    if(str != NULL)
    {
        while(*str!='\0')
        {
            MAP_UARTCharPut(CONSOLE,*str++);		//发送字符
        }
    }
#endif
}

//串口1的发送字符串韩硕
void
Message_User(const char *str)
{
#ifndef NOTERM
    if(str != NULL)
    {
        while(*str!='\0')
        {
            MAP_UARTCharPut(CONSOLE1,*str++);	//发送字符
        }
    }
#endif
}
void Send_LCD_Commond(LCD_Control Commond)
{
	uint8_t data_len=0,i=0;
	 MAP_UARTCharPut(CONSOLE1,0x5a);
	 MAP_UARTCharPut(CONSOLE1,0xA5);
	 MAP_UARTCharPut(CONSOLE1,Commond.data_len);
	 MAP_UARTCharPut(CONSOLE1,Commond.commond);
	 if((Commond.commond==WriteRegisterCommond)||(Commond.commond==ReadRegisterCommond))
	 {
		 MAP_UARTCharPut(CONSOLE1,Commond.addr[0]);
		 data_len=Commond.data_len-2;
	 }
	 else
	 {
		 MAP_UARTCharPut(CONSOLE1,Commond.addr[0]);
		 MAP_UARTCharPut(CONSOLE1,Commond.addr[1]);
		 data_len=Commond.data_len-3;
	 }
	 for(i=0;i<data_len;i++)
	 {
		 MAP_UARTCharPut(CONSOLE1,Commond.data[i]);
	 }
}
//*****************************************************************************
//
//!    Clear the console window
//!
//! This function
//!        1. clears the console window.
//!
//! \return none
//
//*****************************************************************************
//void
//ClearTerm()
//{
//    Message("\33[2J\r");
//}
//*****************************************************************************
//
//!    Clear the console window
//!
//! This function
//!        1. clears the console window.
//!
//! \return none
//
//*****************************************************************************
void
UART1_IF_ConfigureNIntEnable(void (*pfnIntHandler)(void))
{
    //
    // Register Interrupt handler
    //
#if defined(USE_TIRTOS) || defined(USE_FREERTOS) || defined(SL_PLATFORM_MULTI_THREADED)
    // USE_TIRTOS: if app uses TI-RTOS (either networking/non-networking)
    // USE_FREERTOS: if app uses Free-RTOS (either networking/non-networking)
    // SL_PLATFORM_MULTI_THREADED: if app uses any OS + networking(simplelink)
    osi_InterruptRegister(INT_UARTA1,pfnIntHandler, INT_PRIORITY_LVL_2);			//操作系统中 注册中断向量
//    UART_PRINT("OsiReturnVal_e=%d\r\n",c);
#else
	MAP_IntPrioritySet(INT_UARTA1, INT_PRIORITY_LVL_2);		//非操作系统设置 优先级
	MAP_UARTIntRegister(UARTA1_BASE,pfnIntHandler);								//非操作系统设置中断向量
#endif

    //
    // Enable Interrupt
    //
	 MAP_UARTIntEnable(UARTA1_BASE,UART_INT_RX);
}
//*****************************************************************************
//
//! Error Function
//!
//! \param 
//!
//! \return none
//! 
//*****************************************************************************
void 
Error(char *pcFormat, ...)
{
#ifndef NOTERM
    char  cBuf[256];
    va_list list;
    va_start(list,pcFormat);
    vsnprintf(cBuf,256,pcFormat,list);
    Message(cBuf);
#endif
    __Errorlog++;
}

//*****************************************************************************
//
//! Get the Command string from UART
//!
//! \param  pucBuffer is the command store to which command will be populated
//! \param  ucBufLen is the length of buffer store available
//!
//! \return Length of the bytes received. -1 if buffer length exceeded.
//! 
//*****************************************************************************
int
GetCmd(char *pcBuffer, unsigned int uiBufLen)
{
    char cChar;
    int iLen = 0;
    
    //
    // Wait to receive a character over UART
    //
    while(MAP_UARTCharsAvail(CONSOLE) == false)		 //判断fifo中是否有数据，没有的话等待1ms
    {
#if defined(USE_FREERTOS) || defined(USE_TI_RTOS)
    	osi_Sleep(1);
#endif
    }
    cChar = MAP_UARTCharGetNonBlocking(CONSOLE);	//获取fifo中的字符
    
    //
    // Echo the received character
    //
    MAP_UARTCharPut(CONSOLE, cChar);				//打印字符
    iLen = 0;										//长度为0
    
    //
    // Checking the end of Command
    //
    while((cChar != '\r') && (cChar !='\n') )		//判断是否结束输入
    {
        //
        // Handling overflow of buffer
        //
        if(iLen >= uiBufLen)					//长度大于设定额长度返回-1
        {
            return -1;
        }
        
        //
        // Copying Data from UART into a buffer
        //
        if(cChar != '\b')						//判断是否等于退格键
        { 
            *(pcBuffer + iLen) = cChar;			//把接收到的数据拷贝到缓存中去
            iLen++;								//相应的长度加1
        }
        else
        {
            //
            // Deleting last character when you hit backspace 
            //
            if(iLen)							//判断长度是否为0
            {
                iLen--;							//长度－1
            }
        }
        //
        // Wait to receive a character over UART
        //
        while(MAP_UARTCharsAvail(CONSOLE) == false) //判断fifo中是否有数据，没有的话等待1ms
        {
#if defined(USE_FREERTOS) || defined(USE_TI_RTOS)
        	osi_Sleep(1);
#endif
        }
        cChar = MAP_UARTCharGetNonBlocking(CONSOLE);//接收数据
        //
        // Echo the received character
        //
        MAP_UARTCharPut(CONSOLE, cChar);			//发送接收到的数据
    }

    *(pcBuffer + iLen) = '\0';						//补上结束符

    Report("\n\r");									//打印换行符

    return iLen;									//返回接收到的数据长度
}

//*****************************************************************************
//
//!    Trim the spaces from left and right end of given string
//!
//! \param  Input string on which trimming happens
//!
//! \return length of trimmed string
//
//*****************************************************************************
int TrimSpace(char * pcInput)
{
    size_t size;
    char *endStr, *strData = pcInput;
    char index = 0;
    size = strlen(strData);

    if (!size)
        return 0;

    endStr = strData + size - 1;
    while (endStr >= strData && IS_SPACE(*endStr))
        endStr--;
    *(endStr + 1) = '\0';

    while (*strData && IS_SPACE(*strData))
    {
        strData++;
        index++;
    }
    memmove(pcInput,strData,strlen(strData)+1);

    return strlen(pcInput);
}

//*****************************************************************************
//
//!    prints the formatted string on to the console
//!
//! \param format is a pointer to the character string specifying the format in
//!           the following arguments need to be interpreted.
//! \param [variable number of] arguments according to the format in the first
//!         parameters
//! This function
//!        1. prints the formatted error statement.
//!
//! \return count of characters printed
//
//*****************************************************************************
int Report(const char *pcFormat, ...)
{
 int iRet = 0;
#ifndef NOTERM

  char *pcBuff=NULL, *pcTemp;
  int iSize = 256;
 
  va_list list;
  pcBuff = (char*)malloc(iSize);
  if(pcBuff == NULL)
  {
      return -1;
  }
  while(1)
  {
      va_start(list,pcFormat);
      iRet = vsnprintf(pcBuff,iSize,pcFormat,list);
      va_end(list);
      if(iRet > -1 && iRet < iSize)
      {
          break;
      }
      else
      {
          iSize*=2;
          if((pcTemp=realloc(pcBuff,iSize))==NULL)
          { 
              Message("Could not reallocate memory\n\r");
              iRet = -1;
              break;
          }
          else
          {
              pcBuff=pcTemp;
          }
          
      }
  }
  Message(pcBuff);
  free(pcBuff);
#endif
  return iRet;
}
//*****************************************************************************
//
//!    prints the formatted string on to the console
//!
//! \param format is a pointer to the character string specifying the format in
//!           the following arguments need to be interpreted.
//! \param [variable number of] arguments according to the format in the first
//!         parameters
//! This function
//!        1. prints the formatted error statement.
//!
//! \return count of characters printed
//
//*****************************************************************************
int Report_User(const char *pcFormat, ...)
{
	Semaphore_pend(UART_SENT_Pro,100);
 int iRet = 0;
#ifndef NOTERM

  char *pcBuff=NULL, *pcTemp;
  int iSize = 256;

  va_list list;
  pcBuff = (char*)malloc(iSize);
  if(pcBuff == NULL)
  {
      return -1;
  }
  while(1)
  {
      va_start(list,pcFormat);
      iRet = vsnprintf(pcBuff,iSize,pcFormat,list);
      va_end(list);
      if(iRet > -1 && iRet < iSize)
      {
          break;
      }
      else
      {
          iSize*=2;
          if((pcTemp=realloc(pcBuff,iSize))==NULL)
          {
        	  Message_User("Could not reallocate memory\n\r");
              iRet = -1;
              break;
          }
          else
          {
              pcBuff=pcTemp;
          }

      }
  }
  Message_User(pcBuff);
  free(pcBuff);
  Semaphore_post(UART_SENT_Pro);
#endif
  return iRet;
}
