//*****************************************************************************
// timer_if.c
//
// timer interface file: contains different interface functions for timer APIs
//
// Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

// Driverlib includes
#include "hw_types.h"
#include "hw_memmap.h"
#include "hw_ints.h"
#include "debug.h"
#include "interrupt.h"
#include "timer.h"
#include "rom.h"
#include "rom_map.h"
#include "prcm.h"

// TI-RTOS includes
#if defined(USE_TIRTOS) || defined(USE_FREERTOS) || defined(SL_PLATFORM_MULTI_THREADED)
#include <stdlib.h>
#include "osi.h"
#endif

#include "timer_if.h"

int32_t SoftTime[15]={-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
static unsigned char
GetPeripheralIntNum(unsigned long ulBase, unsigned long ulTimer)   //根据定时器地址，和定时器种类，返回中断向量号
{
    if(ulTimer == TIMER_A)
    {
       switch(ulBase)
       {
           case TIMERA0_BASE:
                 return INT_TIMERA0A;
           case TIMERA1_BASE:
                 return INT_TIMERA1A;
           case TIMERA2_BASE:
                 return INT_TIMERA2A;
           case TIMERA3_BASE:
                 return INT_TIMERA3A;
           default:
                 return INT_TIMERA0A;
           }
    }
    else if(ulTimer == TIMER_B)
    {
       switch(ulBase)
       {
           case TIMERA0_BASE:
                 return INT_TIMERA0B;
           case TIMERA1_BASE:
                 return INT_TIMERA1B;
           case TIMERA2_BASE:
                 return INT_TIMERA2B;
           case TIMERA3_BASE:
                 return INT_TIMERA3B;
           default:
                 return INT_TIMERA0B;
           }
    }
    else
    {
        return INT_TIMERA0A;
    }

}

//*****************************************************************************
//
//!    Initializing the Timer           中断初始化程序
//!
//! \param ePeripheral is the peripheral which need to be initialized.     需要被初始化的外设
//! \param ulBase is the base address for the timer.
//! \param ulConfig is the configuration for the timer.
//! \param ulTimer selects amoung the TIMER_A or TIMER_B or TIMER_BOTH.
//! \param ulValue is the timer prescale value which must be between 0 and
//! 255 (inclusive) for 16/32-bit timers and between 0 and 65535 (inclusive)
//! for 32/64-bit timers.
//! This function
//!     1. Enables and reset the peripheral for the timer.
//!     2. Configures and set the prescale value for the timer.
//!
//! \return none
//
//*****************************************************************************
void Timer_IF_Init( unsigned long ePeripheral, unsigned long ulBase, unsigned
               long ulConfig, unsigned long ulTimer, unsigned long ulValue)
{
    //
    // Initialize GPT A0 (in 32 bit mode) as periodic down counter.
    //
    MAP_PRCMPeripheralClkEnable(ePeripheral, PRCM_RUN_MODE_CLK);				//打开定时器时钟
    MAP_PRCMPeripheralReset(ePeripheral);										//定时器软件复位
    MAP_TimerConfigure(ulBase,ulConfig);										//配置定时器
    MAP_TimerPrescaleSet(ulBase,ulTimer,ulValue);								//设置定时器分频
}

//*****************************************************************************
//
//!    setting up the timer
//!
//! \param ulBase is the base address for the timer.
//! \param ulTimer selects between the TIMER_A or TIMER_B or TIMER_BOTH.
//! \param TimerBaseIntHandler is the pointer to the function that handles the
//!    interrupt for the Timer
//!
//! This function
//!     1. Register the function handler for the timer interrupt.
//!     2. enables the timer interrupt.
//!
//! \return none
//
//*****************************************************************************
void Timer_IF_IntSetup(unsigned long ulBase, unsigned long ulTimer, 
                   void (*TimerBaseIntHandler)(void))
{
  //
  // Setup the interrupts for the timer timeouts.
  //
#if defined(USE_TIRTOS) || defined(USE_FREERTOS) || defined(SL_PLATFORM_MULTI_THREADED) 
    // USE_TIRTOS: if app uses TI-RTOS (either networking/non-networking)
    // USE_FREERTOS: if app uses Free-RTOS (either networking/non-networking)
    // SL_PLATFORM_MULTI_THREADED: if app uses any OS + networking(simplelink)
      if(ulTimer == TIMER_BOTH)
      {
          osi_InterruptRegister(GetPeripheralIntNum(ulBase, TIMER_A),				//操作系统中注册中断向量，初始化定时器中断
                                   TimerBaseIntHandler, INT_PRIORITY_LVL_3);
          osi_InterruptRegister(GetPeripheralIntNum(ulBase, TIMER_B),
                                  TimerBaseIntHandler, INT_PRIORITY_LVL_3);
      }
      else
      {
          osi_InterruptRegister(GetPeripheralIntNum(ulBase, ulTimer),
                                   TimerBaseIntHandler, INT_PRIORITY_LVL_3);
      }
        
#else
	  MAP_IntPrioritySet(GetPeripheralIntNum(ulBase, ulTimer), INT_PRIORITY_LVL_3);
      MAP_TimerIntRegister(ulBase, ulTimer, TimerBaseIntHandler);
#endif
 

  if(ulTimer == TIMER_BOTH)
  {
    MAP_TimerIntEnable(ulBase, TIMER_TIMA_TIMEOUT|TIMER_TIMB_TIMEOUT);					//使能定时器
  }
  else
  {
    MAP_TimerIntEnable(ulBase, ((ulTimer == TIMER_A) ? TIMER_TIMA_TIMEOUT : 			//使能定时器
                                   TIMER_TIMB_TIMEOUT));
  }
}

//*****************************************************************************
//
//!    clears the timer interrupt
//!
//! \param ulBase is the base address for the timer.
//!
//! This function
//!     1. clears the interrupt with given base.
//!
//! \return none
//
//*****************************************************************************
void Timer_IF_InterruptClear(unsigned long ulBase)
{
    unsigned long ulInts;
    ulInts = MAP_TimerIntStatus(ulBase, true);								//获取定时器的中断状态
    //
    // Clear the timer interrupt.
    //
    MAP_TimerIntClear(ulBase, ulInts);										//清除定时器标志位
}

//*****************************************************************************
//
//!    starts the timer
//!
//! \param ulBase is the base address for the timer.
//! \param ulTimer selects amoung the TIMER_A or TIMER_B or TIMER_BOTH.
//! \param ulValue is the time delay in mSec after that run out, 
//!                 timer gives an interrupt.
//!
//! This function
//!     1. Load the Timer with the specified value.
//!     2. enables the timer.
//!
//! \return none
//!
//! \Note- HW Timer runs on 80MHz clock 
//
//*****************************************************************************
void Timer_IF_Start(unsigned long ulBase, unsigned long ulTimer, 
                unsigned long ulValue)
{
//    MAP_TimerLoadSet(ulBase,ulTimer,MILLISECONDS_TO_TICKS(ulValue));		//设置定时器的周期
	 MAP_TimerLoadSet(ulBase,ulTimer,(SYS_CLK/20000) * (ulValue));

    //
    // Enable the GPT 
    //
    MAP_TimerEnable(ulBase,ulTimer);										//定时器使能
}

//*****************************************************************************
//
//!    disable the timer
//!
//! \param ulBase is the base address for the timer.
//! \param ulTimer selects amoung the TIMER_A or TIMER_B or TIMER_BOTH.
//!
//! This function
//!     1. disables the interupt.
//!
//! \return none
//
//*****************************************************************************
void Timer_IF_Stop(unsigned long ulBase, unsigned long ulTimer)
{
    //
    // Disable the GPT 
    //
    MAP_TimerDisable(ulBase,ulTimer);					//关闭指定的定时器
}

//*****************************************************************************
//
//!    De-Initialize the timer
//!
//! \param uiGPTBaseAddr
//! \param ulTimer
//!
//! This function 
//!        1. disable the timer interrupts
//!        2. unregister the timer interrupt
//!
//!    \return None.
//
//*****************************************************************************
void Timer_IF_DeInit(unsigned long ulBase,unsigned long ulTimer)
{
    //
    // Disable the timer interrupt
    //
    MAP_TimerIntDisable(ulBase,TIMER_TIMA_TIMEOUT|TIMER_TIMB_TIMEOUT);		//关闭定时器
    //
    // Unregister the timer interrupt
    //
    MAP_TimerIntUnregister(ulBase,ulTimer);									//关闭定时器中断
}

//*****************************************************************************
//
//!    starts the timer
//!
//! \param ulBase is the base address for the timer.
//! \param ulTimer selects between the TIMER A and TIMER B.
//! \param ulValue is timer reload value (mSec) after which the timer will run out and gives an interrupt.
//! \param MILLISECONDS_TO_TICKS的作用是限定ms级
//! This function
//!     1. Reload the Timer with the specified value.
//!
//! \return none
//
//*****************************************************************************
void Timer_IF_ReLoad(unsigned long ulBase, unsigned long ulTimer, 
                unsigned long ulValue)
{
    MAP_TimerLoadSet(ulBase,ulTimer,MILLISECONDS_TO_TICKS(ulValue));		//设置定时器的装载值，也就是设置定时周期
}

//*****************************************************************************
//
//!    starts the timer
//!
//! \param ulBase is the base address for the timer.
//! \param ulTimer selects amoung the TIMER_A or TIMER_B or TIMER_BOTH.
//!
//! This function
//!     1. returns the timer value.
//!
//! \return Timer Value.
//
//*****************************************************************************
unsigned int Timer_IF_GetCount(unsigned long ulBase, unsigned long ulTimer)
{
    unsigned long ulCounter;
    ulCounter = MAP_TimerValueGet(ulBase, ulTimer);			//读取指定定时器的当前值
    return 0xFFFFFFFF - ulCounter;
}


//****************************************************************************
//
//! 设置pwm模式
//!
//! \param ulBase	定时器的基础地址
//! \param ulTimer	定时器的时钟类型，TIMEA 或者TIMEB
//! \param ulConfig 定时器的工作类型
//! \param ucInvert	电平设置
//!
//! This function sets up the folowing
//!    1. TIMERA2 (TIMER B) as RED of RGB light
//!    2. TIMERA3 (TIMER B) as YELLOW of RGB light
//!    3. TIMERA3 (TIMER A) as GREEN of RGB light
//!
//! \return None.
//
//****************************************************************************

//void Timer_IF_SetPWMMode(unsigned long ulBase, unsigned long ulTimer,
//                       unsigned long ulConfig, unsigned char ucInvert)
//{
//    //
//    // Set GPT - Configured Timer in PWM mode.
//    //
//    MAP_TimerConfigure(ulBase,ulConfig);						//配置定时器，设置定时器的工作类型
//    MAP_TimerPrescaleSet(ulBase,ulTimer,0);						//设置分频系数为0
//
//    //
//    // Inverting the timer output if required
//    //
//    MAP_TimerControlLevel(ulBase,ulTimer,ucInvert);				//设施输出电平
//
//    //
//    // Load value set to ~0.5 ms time period
//    //
//    MAP_TimerLoadSet(ulBase,ulTimer,TIMER_INTERVAL_RELOAD);		//设置定时器的装载值 也就是设置pwm的周期
//
//    //
//    // Match value set so as to output level 0
//    //
//    MAP_TimerMatchSet(ulBase,ulTimer,TIMER_INTERVAL_RELOAD);	//设置定时器的装载数，设到最高，也就是设置占空比
//
//}
void Timer_IF_SetPWMMode(User_pwm_kind kind,unsigned long ulConfig, unsigned char ucInvert,unsigned long  frequency)
{
	unsigned long ulBase,ulTimer;
	unsigned long fre=0;
	switch (kind)
	{
		case FAN:
		ulBase=TIMERA2_BASE;
		ulTimer=TIMER_B;
		break;
		case LAMP1:
		ulBase=TIMERA3_BASE;
		ulTimer=TIMER_A;
		break;
		case LAMP2:
		ulBase=TIMERA3_BASE;
		ulTimer=TIMER_B;
		break;
		default:
			return ;
//		break;

	}
	fre=80000000/frequency;
	if(fre>65535||fre<=0)
	{
		UART_PRINT("frequency set error,please check!\r\n");
		return;
	}
	//
	// Set GPT - Configured Timer in PWM mode.
	//
	MAP_TimerConfigure(ulBase, ulConfig);				//配置定时器，设置定时器的工作类型
	MAP_TimerPrescaleSet(ulBase, ulTimer, 0);					//设置分频系数为0

	//
	// Inverting the timer output if required
	//
	MAP_TimerControlLevel(ulBase, ulTimer, ucInvert);				//设施输出电平

	//
	// Load value set to ~0.5 ms time period
	//
	MAP_TimerLoadSet(ulBase, ulTimer, fre);//设置定时器的装载值 也就是设置pwm的周期

	//
	// Match value set so as to output level 0
	//
	MAP_TimerMatchSet(ulBase, ulTimer, fre);//设置定时器的装载数，设到最高，也就是设置占空比
}
//****************************************************************************
//
//! Sets up the identified timers as PWM to drive the peripherals
//!
//! \param none
//!
//! This function sets up the folowing
//!    1. TIMERA2 (TIMER B) as RED of RGB light
//!    2. TIMERA3 (TIMER B) as YELLOW of RGB light
//!    3. TIMERA3 (TIMER A) as GREEN of RGB light
//!
//! \return None.
//
//****************************************************************************
void Timer_IF_InitPWMModules(void)
{
    //
    // Initialization of timers to generate PWM output
    //
    MAP_PRCMPeripheralClkEnable(PRCM_TIMERA2, PRCM_RUN_MODE_CLK);				//使能定时器2的时钟
    MAP_PRCMPeripheralClkEnable(PRCM_TIMERA3, PRCM_RUN_MODE_CLK);

    //
    // TIMERA2 (TIMER B) as RED of RGB light. GPIO 9 --> PWM_5					FAN1
    //
    Timer_IF_SetPWMMode(FAN,(TIMER_CFG_SPLIT_PAIR | TIMER_CFG_B_PWM),0,25000);//25000
//    Timer_IF_SetPWMMode(TIMERA2_BASE, TIMER_B,
//            (TIMER_CFG_SPLIT_PAIR | TIMER_CFG_B_PWM), 1);						//设置定时器2 TIMEB模式为双16位定时器，pwm模式 指定输出电平为高
    //
    // TIMERA3 (TIMER B) as YELLOW of RGB light. GPIO 10 --> PWM_6				LAMP1
    //
    Timer_IF_SetPWMMode(LAMP1,(TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PWM | TIMER_CFG_B_PWM),1,2000);
//    Timer_IF_SetPWMMode(TIMERA3_BASE, TIMER_A,
//            (TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PWM | TIMER_CFG_B_PWM), 1);		//设置定时器3 TIMEA 模式为双16位定时器，pwm模式 指定输出电平为高
    //
    // TIMERA3 (TIMER A) as GREEN of RGB light. GPIO 11 --> PWM_7				LAMP2
    //
    Timer_IF_SetPWMMode(LAMP2,(TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PWM | TIMER_CFG_B_PWM),1,2000);
//    Timer_IF_SetPWMMode(TIMERA3_BASE, TIMER_B,
//            (TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PWM | TIMER_CFG_B_PWM), 1);		//设置定时器3 TIMEB 模式为双16位定时器，pwm模式 指定输出电平为高

    MAP_TimerEnable(TIMERA2_BASE,TIMER_B);										//使能定时器
    MAP_TimerEnable(TIMERA3_BASE,TIMER_A);
    MAP_TimerEnable(TIMERA3_BASE,TIMER_B);
}
//****************************************************************************
//
//! Disables the timer PWMs
//!
//! \param none
//!
//! This function disables the timers used
//!
//! \return None.
//
//****************************************************************************
void Timer_IF_DeInitPWMModules(void)
{
    //
    // Disable the peripherals
    //
    MAP_TimerDisable(TIMERA2_BASE, TIMER_B);						//关闭定时器2 的timeB
    MAP_TimerDisable(TIMERA3_BASE, TIMER_A);						//关闭定时器1的timeA
    MAP_TimerDisable(TIMERA3_BASE, TIMER_B);						//关闭定时器3的timeB
    MAP_PRCMPeripheralClkDisable(PRCM_TIMERA2, PRCM_RUN_MODE_CLK);	//关闭时钟
    MAP_PRCMPeripheralClkDisable(PRCM_TIMERA3, PRCM_RUN_MODE_CLK);	//关闭时钟
}

//****************************************************************************
//
//! Update the dutycycle of the PWM timer
//!
//! \param ulBase is the base address of the timer to be configured
//! \param ulTimer is the timer to be setup (TIMER_A or  TIMER_B)
//! \param ucLevel translates to duty cycle settings (0:255)
//!
//! This function
//!    1. The specified timer is setup to operate as PWM
//!
//! \return None.
//
//****************************************************************************
//void Timer_IF_SetDutyCycle(unsigned long ulBase, unsigned long ulTimer,
//                     unsigned short ucLevel)
//{
//    //
//    // Match value is updated to reflect the new dutycycle settings
//    //
//    MAP_TimerMatchSet(ulBase,ulTimer,(ucLevel*DUTYCYCLE_GRANULARITY));				//设置定时器的装载数 （设置pwm的占空比）
//}
void Timer_IF_SetDutyCycle(User_pwm_kind kind,unsigned short ucLevel)
{
	unsigned int DUTYCYCLE;
	unsigned long ulBase,ulTimer;
//	uint16_t duty=0;
	switch (kind) {
	case FAN:
		ulBase = TIMERA2_BASE;
		ulTimer = TIMER_B;
		DUTYCYCLE=32*ucLevel;						//32
		if(DUTYCYCLE>=3200)
			DUTYCYCLE=3199;
		break;
	case LAMP1:
		ulBase = TIMERA3_BASE;
		ulTimer = TIMER_A;
//		DUTYCYCLE=40;
		DUTYCYCLE=400*ucLevel;
		if(DUTYCYCLE>=40000)
			DUTYCYCLE=39999;
		break;
	case LAMP2:
		ulBase = TIMERA3_BASE;
		ulTimer = TIMER_B;
//		DUTYCYCLE=40;
		DUTYCYCLE=400*ucLevel;
		if(DUTYCYCLE>=40000)
			DUTYCYCLE=39999;
		break;
	default:
		return;

	}
    //
    // Match value is updated to reflect the new dutycycle settings
    //
    MAP_TimerMatchSet(ulBase,ulTimer,DUTYCYCLE);				//设置定时器的装载数 （设置pwm的占空比）(ucLevel*DUTYCYCLE)
}
//****************************************************************************
//
//! 初始化定时器A0 timeA   A1 timeA
//!
//! \param none
//!
//! 这个函数用来初始化用户定时器
//!
//! \return None.
//
//****************************************************************************
void time_init(void)
{
	Timer_IF_Init(PRCM_TIMERA0, TIMERA0_BASE, TIMER_CFG_PERIODIC, TIMER_A, 0);		//设置定时器A0 TIMER_A 为周期定时  不分频
//	Timer_IF_Init(PRCM_TIMERA1, TIMERA1_BASE, TIMER_CFG_PERIODIC, TIMER_A, 0);		//设置定时器A1 TIMER_A 为周期定时  不分频
	Timer_IF_IntSetup(TIMERA0_BASE, TIMER_A, TimerBaseIntHandler);					//定时器A0 TIMER_A 中断设置
//	Timer_IF_IntSetup(TIMERA1_BASE, TIMER_A, TimerRefIntHandler);					//定时器A1 TIMER_A 中断设置
	Timer_IF_Start(TIMERA0_BASE, TIMER_A, 20);										//设置定时器A0的定时周期为500ms
//	Timer_IF_Start(TIMERA1_BASE, TIMER_A, 1000);									//设置定时器A1的定时周期为1000ms
}
//****************************************************************************
//
//! 初始化pwm
//!
//! \param none
//!
//! 这个函数用来初始化用户定时器
//!
//! \return None.
//
//****************************************************************************
void pwm_init(void)
{
	GPIO_IF_Set(6, 0);       //默认风扇上电位停转状态
	Timer_IF_InitPWMModules();
}
//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************
