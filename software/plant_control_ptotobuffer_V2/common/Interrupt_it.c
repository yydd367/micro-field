/*
 * Interrupt_it.c
 *
 *  Created on: 2016年3月5日
 *      Author: Kang
 */

#include "Interrupt_it.h"
#include "task.h"
#include "updata.h"
#include "Collection.h"
#include "timer_if.h"
long flow_count=0;
uint8_t time_begin=0;
uint8_t recievebuffer[50]={0};
uint8_t time_control_water_statue=1;
uint8_t time_control_lamp_statue=1;
uint8_t time_control_fan_statue=1;
//uint8_t receive_data_statue=0;
//char receive_uart_data[12][5]={'\0'};
//*****************************************************************************
//
//! The interrupt handler for the first timer interrupt.
//!
//! \param  None
//!
//! \return none
//
//*****************************************************************************
void
TimerBaseIntHandler(void)
{
    //
    // Clear the timer interrupt.
    //
	uint8_t i=0;
	Timer_IF_InterruptClear(TIMERA0_BASE);	//清除定时器0中断标志
//	GPIO_IF_Toggle(5);
	for(i=0;i<15;i++)                       //10ms定时器
	{
		if(SoftTime[i]>0)
			SoftTime[i]--;
	}

}

//*****************************************************************************
//
//! The interrupt handler for the second timer interrupt.
//!
//! \param  None
//!
//! \return none
//
//*****************************************************************************
void
TimerRefIntHandler(void)
{
    //
    // Clear the timer interrupt.
    //
    Timer_IF_InterruptClear(TIMERA1_BASE);	//清除定时器1中断标志

//    g_ulRefTimerInts ++;
//    GPIO_IF_Toggle(10);
}
//*****************************************************************************
//
//! The interrupt handler for the GPIO A0 interrupt.
//!
//! \param  None
//!
//! \return none
//
//*****************************************************************************
void GpioA0Handle(void)
{

	if (GPIOIntStatus(GPIOA0_BASE, true) == GPIO_INT_PIN_4)	//读取中断
	{
		flow_count++;
	}
	GPIOIntClear(GPIOA0_BASE, GPIO_INT_PIN_4);				//清除GPIO13 中断标志

}
//*****************************************************************************
//
//! The interrupt handler for the GPIO A0 interrupt.
//!
//! \param  None
//!
//! \return none
//
//*****************************************************************************
void
UART1IntHandler(void)
{
    //
    // Clear the timer interrupt.
    //
	static int iLen = 0,j=0;
		if(UARTIntStatus(UARTA1_BASE,true)==UART_INT_RX)
		{
			if(iLen==0)
				memset(recievebuffer,0, 50);                 //清空缓存
			recievebuffer[++iLen] = UARTCharGet(UARTA1_BASE);
			//MAP_UARTCharPut(CONSOLE,recievebuffer[iLen]);
			if(iLen==2)
			{
				if(recievebuffer[1]!=0x5a||recievebuffer[2]!=0xa5)
				iLen=0;
	//			memset(recievebuffer,0, 50);
				return;
			}
			if(iLen==4)
			{
				if(recievebuffer[4]!=0x81&&recievebuffer[4]!=0x83&&recievebuffer[4]!=0x82&&recievebuffer[4]!=0x80)
				{
					iLen=0;
				}
			}
			j=recievebuffer[3];
		    if(iLen>2+j)
			{
				recievebuffer[0]=iLen;
				if(recievebuffer[iLen]==0x5a)
					UARTIntClear (UARTA1_BASE,UART_INT_RX);
				iLen=0;
				Mailbox_post(UART_mail,recievebuffer,0);
			}
/*			if(iLen==4)
			{
				if(recievebuffer[4]==0x81)
				{
					j=8;
					receive8_over=1;
//					if(iLen==6)
//						j=recievebuffer[5]+5;
				}
				else if(recievebuffer[4]==0x83)
				{
					j=9;
//					if(iLen==7)
//						j=recievebuffer[7]*2+6;
				}
			}
			if((iLen==8 && receive8_over==1) || iLen==9)                       //等待所有数据接收完毕再发到邮箱
			{
				recievebuffer[0]=j;
				iLen=0;
				receive8_over=0;
				receive_data_statue=1;
				Mailbox_post(UART_mail,recievebuffer,0);
			}*/
//			if(iLen>j)
//			{
//				recievebuffer[0]=iLen;
//				iLen=0;
//				j=5;
//				Mailbox_post(UART_mail,recievebuffer,0);
//			}
//			if(recievebuffer[4]==0x83)
//			{
//				if(iLen>6+j*2)
//				{
//					recievebuffer[0]=iLen;
//					iLen=0;
//					Mailbox_post(UART_mail,recievebuffer,0);
//				}
//			}
//			else if(recievebuffer[4]==0x81)
//			{
//				if(iLen>5+j)
//				{
//					recievebuffer[0]=iLen;
//					iLen=0;
//					Mailbox_post(UART_mail,recievebuffer,0);
//				}
//			}
			UARTIntClear (UARTA1_BASE,UART_INT_RX);
		}
//		else(UARTIntStatus(UARTA1_BASE,ture)==UART_INT_RX)
//		{
//			UART_PRINT("no data");
//		}
}
